<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\FileSystem;
use Illuminate\Support\Str;
use App\Libraries\General;

class Vehicles extends AppModel
{
    protected $table = 'vehicles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**** ONLY USE FOR MAIN TALBLES NO NEED TO USE FOR RELATION TABLES OR DROPDOWNS OR SMALL SECTIONS ***/
    use SoftDeletes;
    
    /**
    * Get resize images
    *
    * @return array
    */
    public function getResizeImagesAttribute()
    {
        return $this->image ? FileSystem::getAllSizeImages($this->image) : null;
    }

    /**
    * Vehicles -> Admins belongsTO relation
    * 
    * @return Admins
    */
    public function owner()
    {
        return $this->belongsTo(Admins::class, 'created_by', 'id');
    }

    /**
    * Vehicles -> Admins belongsTO relation
    * 
    * @return Admins
    */
    public function driver()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }

    /**
    * Vehicle -> VehicleBrands belongsTO relation
    * 
    * @return VehicleBrands
    */
    public function vehicle_brands()
    {
        return $this->belongsTo(VehicleBrands::class, 'brand_id', 'id');
    }

    /**
    * Vehicle -> VehicleModels belongsTO relation
    * 
    * @return VehicleModels
    */
    public function vehicle_models()
    {
        return $this->belongsTo(VehicleModels::class, 'model_id', 'id');
    }

    /**
    * Vehicle -> VehicleColors belongsTO relation
    * 
    * @return VehicleColors
    */
    public function vehicle_colors()
    {
        return $this->belongsTo(VehicleColors::class, 'color_id', 'id');
    }

    /**
    * Vehicle -> Services hasMany relation
    * 
    * @return Services
    */
    public function services()
    {
        return $this->belongsToMany(Services::class, 'vehicle_services', 'vehicle_id', 'service_id');
    }

    /**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */

    public static function getListing(Request $request, $where = [])
    {
    	$orderBy = $request->get('sort') ? $request->get('sort') : 'vehicles.id';
    	$direction = $request->get('direction') ? $request->get('direction') : 'desc';
    	$page = $request->get('page') ? $request->get('page') : 1;
    	$limit = self::$paginationLimit;
    	$offset = ($page - 1) * $limit;
    	
    	$listing = Vehicles::select([
	    		'vehicles.*',
                'owner.first_name as owner_first_name',
                'owner.last_name as owner_last_name',
                'vehicle_brands.title as brand_name',
                'vehicle_models.title as model_name',
                'users.first_name as driver_first_name',
                'users.last_name as driver_last_name',
                'vehicle_colors.title as color',
	    	])
            ->leftJoin('admins as owner', 'owner.id', '=', 'vehicles.created_by')
            ->leftJoin('users', 'users.id', '=', 'vehicles.user_id')
            ->leftJoin('vehicle_brands', 'vehicle_brands.id', '=', 'vehicles.brand_id')
            ->leftJoin('vehicle_colors', 'vehicle_colors.id', '=', 'vehicles.color_id')
            ->leftJoin('vehicle_models', 'vehicle_models.id', '=', 'vehicles.model_id')
            ->orderBy($orderBy, $direction);

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }

	    // Put offset and limit in case of pagination
	    if($page !== null && $page !== "" && $limit !== null && $limit !== "")
	    {
	    	$listing->offset($offset);
	    	$listing->limit($limit);
	    }
        
	    $listing = $listing->paginate($limit);

	    return $listing;
    }

    /**
    * To get all records
    * @param $where
    * @param $orderBy
    * @param $limit
    */
    public static function getAll($select = [], $where = [], $orderBy = 'vehicles.id desc', $limit = null)
    {
    	$listing = Vehicles::orderByRaw($orderBy);

    	if(!empty($select))
    	{
    		$listing->select($select);
    	}
    	else
    	{
    		$listing->select([
    			'vehicles.*'
    		]);	
    	}

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }
	    
	    if($limit !== null && $limit !== "")
	    {
	    	$listing->limit($limit);
	    }

	    $listing = $listing->get();

	    return $listing;
    }

    /**
    * To get single record by id
    * @param $id
    */
    public static function get($id)
    {
    	$record = Vehicles::where('id', $id)
            ->with([
                'vehicle_brands' => function($query) {
                    $query->select(['vehicle_brands.id', 'vehicle_brands.title']);
                },
                'vehicle_colors' => function($query) {
                    $query->select(['vehicle_colors.id', 'vehicle_colors.title']);
                },
                'vehicle_models' => function($query) {
                    $query->select(['vehicle_models.id', 'vehicle_models.title']);
                },
                'owner' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                },
            ])
            ->first();
            
	    return $record;
    }

    /**
    * To get single row by conditions
    * @param $where
    * @param $orderBy
    */
    public static function getRow($where = [], $orderBy = 'vehicles.id desc')
    {
    	$record = Vehicles::orderByRaw($orderBy);

	    foreach($where as $query => $values)
	    {
	    	if(is_array($values))
                $record->whereRaw($query, $values);
            elseif(!is_numeric($query))
                $record->where($query, $values);
            else
                $record->whereRaw($values);
	    }
	    
	    $record = $record->limit(1)->first();

	    return $record;
    }

    /**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$vehicle = new Vehicles();

    	foreach($data as $k => $v)
    	{
    		$vehicle->{$k} = $v;
    	}

        $vehicle->created_by = AdminAuth::getLoginId();
    	$vehicle->created = date('Y-m-d H:i:s');
    	$vehicle->modified = date('Y-m-d H:i:s');
	    if($vehicle->save())
	    {
	    	return $vehicle;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$vehicle = Vehicles::find($id);
    	foreach($data as $k => $v)
    	{
    		$vehicle->{$k} = $v;
    	}

    	$vehicle->modified = date('Y-m-d H:i:s');
	    if($vehicle->save())
	    {
	    	return $vehicle;
	    }
	    else
	    {
	    	return null;
	    }
    }

    
    /**
    * To update all
    * @param $id
    * @param $where
    */
    public static function modifyAll($ids, $data)
    {
    	if(!empty($ids))
    	{
    		return Vehicles::whereIn('vehicles.id', $ids)
		    		->update($data);
	    }
	    else
	    {
	    	return null;
	    }

    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$blog = Vehicles::find($id);
    	return $blog->delete();
    }

    /**
    * To delete all
    * @param $id
    * @param $where
    */
    public static function removeAll($ids)
    {
    	if(!empty($ids))
    	{
    		return Vehicles::whereIn('vehicles.id', $ids)
		    		->delete();
	    }
	    else
	    {
	    	return null;
	    }
    }

    public static function handleServices($vehicleId, $services)
    {
        VehicleServices::where('vehicle_id', $vehicleId)->delete();
        foreach($services as $k => $p)
        {
            $service = new VehicleServices();
            $service->vehicle_id = $vehicleId;
            $service->service_id = $p;
            $service->save();
        }
    }
}