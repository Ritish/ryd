<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class ServicePrices extends AppModel
{
	protected $table = 'service_prices';
    protected $primaryKey = 'id';
 	public $timestamps = false;

}