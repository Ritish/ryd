<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripsRating extends AppModel
{
	protected $table = 'trips_rating';
    protected $primaryKey = 'id';
 	public $timestamps = false;
    
 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$rating = new TripsRating();

    	foreach($data as $k => $v)
    	{
    		$rating->{$k} = $v;
    	}

    	$rating->created = date('Y-m-d H:i:s');
    	$rating->modified = date('Y-m-d H:i:s');
	    if($rating->save())
	    {
	    	return $rating;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$rating = TripsRating::find($id);
    	foreach($data as $k => $v)
    	{
    		$rating->{$k} = $v;
    	}

    	$rating->modified = date('Y-m-d H:i:s');
	    if($rating->save())
	    {
	    	return $rating;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$rating = TripsRating::find($id);
    	return $rating->delete();
    }

    public static function get($id)
    {
        $rating = TripsRating::find($id);
        return $rating;
    }
}