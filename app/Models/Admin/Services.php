<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class Services extends AppModel
{
	protected $table = 'services';
    protected $primaryKey = 'id';
 	public $timestamps = false;

 	public static function getPriceValue($id, $key)
 	{
 		return ServicePrices::where('service_id', $id)->where('name', $key)->limit(1)->pluck('value')->first();
 	}

 	/**
     * Update option value
     *
     * @param  Settings $key
     * @return Response
     */
    public static function putPriceValue($id, $key, $value)
    {
        $option = ServicePrices::where('service_id', $id)->where('name', $key)->limit(1)->first();
        if($option)
        {
            $option->value = $value ? $value : "";
            return $option->save();
        }
        else
        {
            $option = new Settings();
            $option->name = $key;
            $option->value = $value;
            return $option->save();
        }
        return false;
    }
}