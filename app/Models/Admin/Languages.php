<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class Languages extends AppModel
{
	protected $table = 'languages';
    protected $primaryKey = 'id';
 	public $timestamps = false;
}