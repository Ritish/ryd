<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class States extends AppModel
{
	protected $table = 'states';
    protected $primaryKey = 'id';
 	public $timestamps = false;
}