<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class UserServices extends AppModel
{
	protected $table = 'user_services';
    protected $primaryKey = 'id';
 	public $timestamps = false;
}