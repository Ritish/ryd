<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;

class VehiclesModels extends AppModel
{
	protected $table = 'vehicle_models';
    protected $primaryKey = 'id';
 	public $timestamps = false;

 	/**
    * VehiclesModels -> Admins belongsTO relation
    * 
    * @return Admins
    */
    public function owner()
    {
        return $this->belongsTo(Admins::class, 'created_by', 'id');
    }

    /**
    * VehiclesModels -> VehiclesBrands belongsTO relation
    * 
    * @return Admins
    */
    public function brand()
    {
        return $this->belongsTo(VehiclesBrands::class, 'created_by', 'id');
    }
    
 	/**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */

    public static function getListing(Request $request, $where = [])
    {
    	$orderBy = $request->get('sort') ? $request->get('sort') : 'vehicle_models.id';
    	$direction = $request->get('direction') ? $request->get('direction') : 'desc';
    	$page = $request->get('page') ? $request->get('page') : 1;
    	$limit = self::$paginationLimit;
    	$offset = ($page - 1) * $limit;
    	
    	$listing = VehiclesModels::select([
	    		'vehicle_models.*',
                'brand.title as brand_title',
                'owner.first_name as owner_first_name',
                'owner.last_name as owner_last_name'
	    	])
            ->leftJoin('admins as owner', 'owner.id', '=', 'vehicle_models.created_by')
            ->leftJoin('vehicle_brands as brand', 'brand.id', '=', 'vehicle_models.brand_id')
            ->orderBy($orderBy, $direction);

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }

	    // Put offset and limit in case of pagination
	    if($page !== null && $page !== "" && $limit !== null && $limit !== "")
	    {
	    	$listing->offset($offset);
	    	$listing->limit($limit);
	    }
        
	    $listing = $listing->paginate($limit);

	    return $listing;
    }

    /**
    * To get all records
    * @param $where
    * @param $orderBy
    * @param $limit
    */
    public static function getAll($select = [], $where = [], $orderBy = 'vehicle_models.id desc', $limit = null)
    {
    	$listing = VehiclesModels::orderByRaw($orderBy);

    	if(!empty($select))
    	{
    		$listing->select($select);
    	}
    	else
    	{
    		$listing->select([
    			'vehicle_models.*'
    		]);	
    	}

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }
	    
	    if($limit !== null && $limit !== "")
	    {
	    	$listing->limit($limit);
	    }

	    $listing = $listing->get();

	    return $listing;
    }

    /**
    * To get single record by id
    * @param $id
    */
    public static function get($id)
    {
    	$record = VehiclesModels::where('id', $id)
            ->with([
                'owner' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                },
                'brand' => function($query) {
                    $query->select(['id', 'title']);
                },
            ])
            ->first();
            
	    return $record;
    }

    /**
    * To get single row by conditions
    * @param $where
    * @param $orderBy
    */
    public static function getRow($where = [], $orderBy = 'vehicle_models.id desc')
    {
    	$record = VehiclesModels::orderByRaw($orderBy);

	    foreach($where as $query => $values)
	    {
	    	if(is_array($values))
                $record->whereRaw($query, $values);
            elseif(!is_numeric($query))
                $record->where($query, $values);
            else
                $record->whereRaw($values);
	    }
	    
	    $record = $record->limit(1)->first();

	    return $record;
    }

    /**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$brand = new VehiclesModels();

    	foreach($data as $k => $v)
    	{
    		$brand->{$k} = $v;
    	}

        $brand->created_by = AdminAuth::getLoginId();
    	$brand->created = date('Y-m-d H:i:s');
    	$brand->modified = date('Y-m-d H:i:s');
	    if($brand->save())
	    {
	    	return $brand;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$brand = VehiclesModels::find($id);
    	foreach($data as $k => $v)
    	{
    		$brand->{$k} = $v;
    	}

    	$brand->modified = date('Y-m-d H:i:s');
	    if($brand->save())
	    {
	    	return $brand;
	    }
	    else
	    {
	    	return null;
	    }
    }

    
    /**
    * To update all
    * @param $id
    * @param $where
    */
    public static function modifyAll($ids, $data)
    {
    	if(!empty($ids))
    	{
    		return VehiclesModels::whereIn('vehicle_models.id', $ids)
		    		->update($data);
	    }
	    else
	    {
	    	return null;
	    }

    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$brand = VehiclesModels::find($id);
    	return $brand->delete();
    }

    /**
    * To delete all
    * @param $id
    * @param $where
    */
    public static function removeAll($ids)
    {
    	if(!empty($ids))
    	{
    		return VehiclesModels::whereIn('vehicle_models.id', $ids)
		    		->delete();
	    }
	    else
	    {
	    	return null;
	    }

    }

    public static function getTitle($id)
    {
        return VehiclesModels::where('id', $id)->pluck('title')->first();
    }
}