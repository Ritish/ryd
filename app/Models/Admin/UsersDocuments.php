<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersDocuments extends AppModel
{
	protected $table = 'users_documents';
    protected $primaryKey = 'id';
 	public $timestamps = false;

    public static function getDocumentName($type = null)
    {
        $names = [
            "driving-license-front" => "Driver's License - Front",
            "driving-license-back" => "Driver's License - Back",
            "insurance-proof" => "Proof of Insurance",
            "vehicle-registration" => "Vehicle Registration"
        ];

        return $type ? $names[$type] : $names;
    } 
 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$document = new UsersDocuments();

    	foreach($data as $k => $v)
    	{
    		$document->{$k} = $v;
    	}

    	$document->created = date('Y-m-d H:i:s');
    	$document->modified = date('Y-m-d H:i:s');
	    if($document->save())
	    {
	    	return $document;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$document = UsersDocuments::find($id);
    	foreach($data as $k => $v)
    	{
    		$document->{$k} = $v;
    	}

    	$document->modified = date('Y-m-d H:i:s');
	    if($document->save())
	    {
	    	return $document;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update all
    * @param $id
    * @param $where
    */
    public static function modifyAll($ids, $data)
    {
        if(!empty($ids))
        {
            return UsersDocuments::whereIn('id', $ids)
                    ->update($data);
        }
        else
        {
            return null;
        }

    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$document = UsersDocuments::find($id);
    	return $document->delete();
    }
}