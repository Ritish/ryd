<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\FileSystem;

class Users extends AppModel
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**** ONLY USE FOR MAIN TALBLES NO NEED TO USE FOR RELATION TABLES OR DROPDOWNS OR SMALL SECTIONS ***/
    use SoftDeletes;

    /**
    * Users -> UsersDocuments hasMany relation
    * 
    * @return UsersDocuments
    */
    public function documents()
    {
        return $this->hasMany(UsersDocuments::class, 'user_id', 'id');
    }

    /**
    * Users -> Addresses hasMany relation
    * 
    * @return Addresses
    */
    public function addreses()
    {
        return $this->hasMany(Addresses::class, 'user_id', 'id');
    }

    /**
    * Users -> UsersCards hasMany relation
    * 
    * @return Addresses
    */
    public function cards()
    {
        return $this->hasMany(UsersCards::class, 'user_id', 'id');
    }

    /**
    * Users -> Languages hasMany relation
    * 
    * @return Addresses
    */
    public function languages()
    {
        return $this->belongsToMany(Languages::class, 'user_languages', 'user_id', 'language_id');
    }

    

    /**
    * Get resize images
    *
    * @return array
    */
    public function getResizeImagesAttribute()
    {
        return $this->image ? FileSystem::getAllSizeImages($this->image) : null;
    }
    
    /**
    * Name getter
    */
    function getNameAttribute()
    {
    	return $this->first_name . ' ' . $this->last_name;
    }

    /**
    * Verfied getter
    */
    function getIsVerifiedAttribute()
    {
        $docs = UsersDocuments::where('user_id', $this->id)->get();
        $count = 0;
        foreach ($docs as $k => $doc) {
            if(in_array($doc->slug, ['background-check', 'vehicle-registration', 'insurance-proof', 'driving-license-back', 'driving-license-front']) && $doc->verified && strtotime($doc->expiry_date . '23:59:59') >= strtotime(date('Y-m-d 00:00:01')))
            {
                $count++;
            }
        }

        return $count >= 5 ? 1 : 0;
    }

    
    /**
    * Password setter
    * @param $value
    */
    function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }
    
    /**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */

    public static function getListing(Request $request, $where = [], $verified = null)
    {
    	$orderBy = $request->get('order') ? $request->get('order') : 'users.id';
    	$direction = $request->get('direction') ? $request->get('direction') : 'desc';
    	$page = $request->get('page') ? $request->get('page') : 1;
    	$limit = self::$paginationLimit;
    	$offset = ($page - 1) * $limit;

    	$listing = Users::select([
	    		'users.*',
                DB::raw('( select count(id) from users_documents where (users_documents.slug = "background-check" or users_documents.slug = "vehicle-registration" or users_documents.slug = "insurance-proof" or users_documents.slug =  "driving-license-back" or users_documents.slug = "driving-license-front") and verified = 1 and expiry_date >= current_date() and users_documents.user_id = users.id ) as all_verified')
	    	])
	    	->orderBy($orderBy, $direction);


	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }

        if($verified !== null)
        {
            if($verified)
                $listing->having('all_verified', '>=', 5);
            else
                $listing->having('all_verified', '<', 5);
        }

	    // Put offset and limit in case of pagination
	    if($page !== null && $page !== "" && $limit !== null && $limit !== "")
	    {
	    	$listing->offset($offset);
	    	$listing->limit($limit);
	    }

	    $listing = $listing->paginate($limit);

	    return $listing;
    }

    /**
    * To get all records
    * @param $where
    * @param $orderBy
    * @param $limit
    */
    public static function getAll($select = [], $where = [], $orderBy = 'users.id desc', $limit = null)
    {
    	$listing = Users::orderByRaw($orderBy);

    	if(!empty($select))
    	{
    		$listing->select($select);
    	}
    	else
    	{
    		$listing->select([
    			'users.*'
    		]);	
    	}

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }
	    
	    if($limit !== null && $limit !== "")
	    {
	    	$listing->limit($limit);
	    }

	    $listing = $listing->get();

	    return $listing;
    }

    /**
    * To get single record by id
    * @param $id
    */
    public static function get($id)
    {
    	$record = Users::find($id);
        
	    return $record;
    }

    /**
    * To get single row by conditions
    * @param $where
    * @param $orderBy
    */
    public static function getRow($where = [], $orderBy = 'users.id desc')
    {
    	$record = Users::orderByRaw($orderBy);

	    foreach($where as $query => $values)
	    {
	    	if(is_array($values))
                $record->whereRaw($query, $values);
            elseif(!is_numeric($query))
                $record->where($query, $values);
            else
                $record->whereRaw($values);
	    }
	    
	    $record = $record->limit(1)->first();

	    return $record;
    }

    /**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$user = new Users();

    	foreach($data as $k => $v)
    	{
    		$user->{$k} = $v;
    	}

        $user->status = 1;
        $user->created_by = AdminAuth::getLoginId();
    	$user->created = date('Y-m-d H:i:s');
    	$user->modified = date('Y-m-d H:i:s');
	    if($user->save())
	    {
	    	return $user;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$user = Users::find($id);
    	foreach($data as $k => $v)
    	{
    		$user->{$k} = $v;
    	}

    	$user->modified = date('Y-m-d H:i:s');
	    if($user->save())
	    {
	    	return $user;
	    }
	    else
	    {
	    	return null;
	    }
    }

    
    /**
    * To update all
    * @param $id
    * @param $where
    */
    public static function modifyAll($ids, $data)
    {
    	if(!empty($ids))
    	{
    		return Users::whereIn('users.id', $ids)
		    		->update($data);
	    }
	    else
	    {
	    	return null;
	    }

    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$user = Users::find($id);
    	return $user->delete();
    }

    /**
    * To delete all
    * @param $id
    * @param $where
    */
    public static function removeAll($ids)
    {
    	if(!empty($ids))
    	{
    		return Users::whereIn('users.id', $ids)
		    		->delete();
	    }
	    else
	    {
	    	return null;
	    }

    }

    public static function handleServices($userId, $services)
    {
        UserServices::where('user_id', $userId)->delete();
        foreach($services as $k => $p)
        {
            $service = new UserServices();
            $service->user_id = $userId;
            $service->service_id = $p;
            $service->save();
        }
    }

    public static function handleLanguages($userId, $languages)
    {
        UserLanguages::where('user_id', $userId)->delete();
        foreach($languages as $k => $p)
        {
            $language = new UserLanguages();
            $language->user_id = $userId;
            $language->language_id = $p;
            $language->save();
        }
    }
}