<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersCards extends AppModel
{
	protected $table = 'users_cards';
    protected $primaryKey = 'id';
 	public $timestamps = false;

 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$card = new UsersCards();

    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->created = date('Y-m-d H:i:s');
    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$card = UsersCards::find($id);
    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$card = UsersCards::find($id);
    	return $card->delete();
    }
}