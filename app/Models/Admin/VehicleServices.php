<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use App\Libraries\General;

class VehicleServices extends AppModel
{
	protected $table = 'vehicle_services';
    protected $primaryKey = 'id';
 	public $timestamps = false;

 	public static function getAmount($driver, $pickup, $destinations, $serviceId)
 	{
 		$multiplier = Services::getPriceValue($serviceId, 'distance_time_multiplier');
 		$pickupDistance = General::distance($driver['lat'], $driver['lng'], $pickup['lat'], $pickup['lng']);
		$pickupMinutes = $pickupDistance * $multiplier;

		$lastLAT = $pickup['lat'];
		$lastLNG = $pickup['lng'];
		$totalDistance = 0;
		$totalDuration = 0;
		foreach($destinations as $d)
		{
			$distance = General::distance($lastLAT, $lastLNG, $d['lat'], $d['lng']);
			$minutes = $distance * $multiplier;
			$totalDistance += $distance;
			$totalDuration += $minutes;

			$lastLAT = $d['lat'];
			$lastLNG = $d['lng'];
		}

		$timeBeforeTripStarts = $pickupMinutes * Services::getPriceValue($serviceId, 'pickup_duration_fee');
		$distanceBeforeTripStarts = $pickupDistance * Services::getPriceValue($serviceId, 'distance_fee');
		$bookingFee = Services::getPriceValue($serviceId, 'booking_fee');
		$pickupFee = Services::getPriceValue($serviceId, 'pickup_fee');
		$perMile = $totalDistance * Services::getPriceValue($serviceId, 'per_mile_fee');
		$perMinutes = $totalDuration * Services::getPriceValue($serviceId, 'per_minute_fee');
		$waitTime = 0;
		$surgeCharge = Services::getPriceValue($serviceId, 'surge_fee');
		$atlantaFee = Services::getPriceValue($serviceId, 'atlanta_fee');
		$emFee = Services::getPriceValue($serviceId, 'em_fee');
		$etFee = Services::getPriceValue($serviceId, 'et_fee');
		$gaTax = Services::getPriceValue($serviceId, 'ga_tax');
		
		$totalAmount = $timeBeforeTripStarts + $distanceBeforeTripStarts + $bookingFee + $pickupFee + $perMile + $perMinutes + $waitTime + $surgeCharge + $atlantaFee + $emFee + $etFee + $gaTax;
		$totalAmount = round($totalAmount, 2);
		
		return $totalAmount;
 	}
}