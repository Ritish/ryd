<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripWaitings extends AppModel
{
	protected $table = 'trip_waitings';
    protected $primaryKey = 'id';
 	public $timestamps = false;
    
 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$card = new TripWaitings();

    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->created = date('Y-m-d H:i:s');
    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$card = TripWaitings::find($id);
    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$card = TripWaitings::find($id);
    	return $card->delete();
    }

    public static function get($id)
    {
        $waiting = TripWaitings::find($id);
        if($waiting)
        {
            $waiting->wait_seconds = ($waiting->end_time ? $waiting->end_time : time()) - $waiting->start_time;
        }

        return $waiting;
    }
}