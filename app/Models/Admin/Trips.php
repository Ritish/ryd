<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\DateTime;
class Trips extends AppModel
{
	protected $table = 'trips';
    protected $primaryKey = 'id';
 	public $timestamps = false;

    /**** ONLY USE FOR MAIN TALBLES NO NEED TO USE FOR RELATION TABLES OR DROPDOWNS OR SMALL SECTIONS ***/
    use SoftDeletes;
    

    /**
    * Trips -> Users belongsTO relation
    * 
    * @return Users
    */
    public function driver()
    {
        return $this->belongsTo(Users::class, 'driver_id', 'id');
    }

    /**
    * Trips -> Users belongsTO relation
    * 
    * @return Users
    */
    public function customer()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }

    /**
    * Trips -> Services belongsTO relation
    * 
    * @return Services
    */
    public function service()
    {
        return $this->belongsTo(Services::class, 'service_id', 'id');
    }

    /**
    * Trips -> Vehicles belongsTO relation
    * 
    * @return Vehicles
    */
    public function vehicle()
    {
        return $this->belongsTo(Vehicles::class, 'vehicle_id', 'id');
    }

    /**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */

    public static function getListing(Request $request, $where = [])
    {
        $orderBy = $request->get('sort') ? $request->get('sort') : 'trips.id';
        $direction = $request->get('direction') ? $request->get('direction') : 'desc';
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = self::$paginationLimit;
        $offset = ($page - 1) * $limit;
        
        $listing = Trips::select([
                'trips.id',
                'trips.user_id',
                'trips.service_id',
                'trips.vehicle_id',
                'trips.driver_id',
                'trips.pickup_address',
                'trips.to_address',
                DB::raw('(trips.distance + before_pickup_distance) as distance'),
                DB::raw('ADDTIME(waiting_time, duration_time) as duration'),
                'trips.customer_total',
                'trips.driver_total',
                'trips.status',
                'trips.created',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.phonenumber as user_phonenumber',
                'driver.first_name as driver_first_name',
                'driver.last_name as driver_last_name',
                'driver.phonenumber as driver_phonenumber',
                'services.title as service_title',
                'vehicles.number as vehicle_number',
                'vehicle_brands.title as vehicle_brand',
                'vehicle_models.title as vehicle_model',

            ])
            ->leftJoin('users', 'users.id', '=', 'trips.user_id')
            ->leftJoin('users as driver', 'driver.id', '=', 'trips.driver_id')
            ->leftJoin('services', 'services.id', '=', 'trips.service_id')
            ->leftJoin('vehicles', 'vehicles.id', '=', 'trips.vehicle_id')
            ->leftJoin('vehicle_brands', 'vehicle_brands.id', '=', 'vehicles.brand_id')
            ->leftJoin('vehicle_models', 'vehicle_models.id', '=', 'vehicles.model_id')
            ->orderBy($orderBy, $direction);

        if(!empty($where))
        {
            foreach($where as $query => $values)
            {
                if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
            }
        }

        // Put offset and limit in case of pagination
        if($page !== null && $page !== "" && $limit !== null && $limit !== "")
        {
            $listing->offset($offset);
            $listing->limit($limit);
        }
        
        $listing = $listing->paginate($limit);

        return $listing;
    }

 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$card = new Trips();

    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->created = date('Y-m-d H:i:s');
    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$card = Trips::find($id);
    	foreach($data as $k => $v)
    	{
    		$card->{$k} = $v;
    	}

    	$card->modified = date('Y-m-d H:i:s');
	    if($card->save())
	    {
	    	return $card;
	    }
	    else
	    {
	    	return null;
	    }
    }
    public static function get($id)
    {
        $trip = Trips::find($id);
        if($trip)
        {
            $trip->destinations = TripDestinations::where('trip_id', $id)->orderBy('id', 'asc')->get();
            $trip->waitings =  TripWaitings::where('trip_id', $id)->orderBy('id', 'asc')->get();
        }

        return $trip;
    }
    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
        $trip = Trips::find($id);
        return $trip->delete();
    }

    /**
    * To delete all
    * @param $id
    * @param $where
    */
    public static function removeAll($ids)
    {
        if(!empty($ids))
        {
            return Trips::whereIn('trips.id', $ids)
                    ->delete();
        }
        else
        {
            return null;
        }
    }

    public static function getStatus($key = null)
    {
        $status = [
            'Pending/Failed',
            'Active',
            'At Pickup Location',
            'Customer Riding',
            'Waiting',
            'Completed',
            'Cancel by Driver',
            'Cancel by Customer',
            'Decline by Customer',
        ];

        return $key !== null ? $status[$key] : $status;
    }

    public static function getSatusLabel($key)
    {
        switch ($key) {
            case '1':
                return '<span class="badge badge-yellow">' . Trips::getStatus($key) . '</span>';
            break;
            case '2':
                return '<span class="badge badge-yellow">' . Trips::getStatus($key) . '</span>';
            break;
            case '3':
                return '<span class="badge badge-yellow">' . Trips::getStatus($key) . '</span>';
            break;
            case '4':
                return '<span class="badge badge-cyan"><i class="fas fa-hourglass-half"></i> ' . Trips::getStatus($key) . '</span>';
            break;
            case '5':
                return '<span class="badge badge-success"><i class="fas fa-check"></i> ' . Trips::getStatus($key) . '</span>';
            break;

            default:
                return '<span class="badge badge-danger">' . Trips::getStatus($key) . '</span>';
            break;
        }
    }

    public static function getCustomerTripCharges($trip)
    {
        /** Customer Trip Calculations **/
        $pickupMinutes = DateTime::hoursToSecondsFormat($trip->before_pickup_duration) / 60;
        $pickupDistance = $trip->before_pickup_distance;
        $waitingMinutes = $trip->waiting_time !== null && $trip->free_wait_time !== null ? ((DateTime::hoursToSecondsFormat($trip->waiting_time) - ($trip->free_wait_time ? DateTime::hoursToSecondsFormat($trip->free_wait_time) : 0)) / 60) : 0;
        $totalDistance = $trip->distance;
        $totalDuration = $trip->duration_time !== null ? DateTime::hoursToSecondsFormat($trip->duration_time) / 60 : 0;
        $timeBeforeTripStarts = round($pickupMinutes * $trip->pickup_duration_fee, 2);
        $distanceBeforeTripStarts = round($pickupDistance * $trip->pickup_distance_fee, 2);
        $bookingFee = $trip->booking_fee;
        $pickupFee = $trip->pickup_fee;
        $perMile = round($totalDistance * $trip->per_mile_fee, 2);
        $perMinutes = round($totalDuration * $trip->per_minute_fee, 2);
        $waitTime = $waitingMinutes * $trip->wait_fee;
        $surgeCharge = $trip->surge_fee;
        $atlantaFee = $trip->atlanta_fee;
        $emFee = $trip->em_fee;
        $etFee = $trip->et_fee;
        $gaTax = $trip->ga_tax;
        $tip = $trip->tip;
        
        $charges = [
            'totalDistance' => $totalDistance,
            'pickupDistance' => $pickupDistance,
            'waitingTime' => $trip->waiting_time,
            'driverFreeWaitTime' => $trip->driver_free_wait_time,
            'freeWaitTime' => $trip->free_wait_time,
            'timeBeforeTripStarts' => $timeBeforeTripStarts,
            'distanceBeforeTripStarts' => $distanceBeforeTripStarts,
            'bookingFee' => $bookingFee,
            'pickupFee' => $pickupFee,
            'perMile' => $perMile,
            'perMinutes' => $perMinutes,
            'waitTime' => $waitTime,
            'surgeCharge' => $surgeCharge,
            'atlantaFee' => $atlantaFee,
            'emFee' => $emFee,
            'etFee' => $etFee,
            'gaTax' => $gaTax,
            'tip' => $tip
        ];
        
        $totalAmount = $timeBeforeTripStarts + $distanceBeforeTripStarts + $bookingFee + $pickupFee + $perMile + $perMinutes + $waitTime + $surgeCharge + $atlantaFee + $emFee + $etFee + $gaTax + $tip;
        $charges['totalAmount'] = round($totalAmount, 2);

        return $charges;

        /** Customer Trip Calculations **/
    }

    public static function getDriverTripAmount($trip)
    {
        $pickupMinutes = $trip->before_pickup_duration ? DateTime::hoursToSecondsFormat($trip->before_pickup_duration) / 60 : 0;
        $pickupDistance = $trip->before_pickup_distance;
        $waitingMinutes = $trip->waiting_time ? (( DateTime::hoursToSecondsFormat($trip->waiting_time) - ($trip->driver_free_wait_time ? DateTime::hoursToSecondsFormat($trip->driver_free_wait_time) : 0) ) / 60) : 0;
        $totalDistance = $trip->distance;
        $totalDuration = $trip->duration_time ? DateTime::hoursToSecondsFormat($trip->duration_time) / 60 : 0;
        $timeBeforeTripStarts = round($pickupMinutes * $trip->dbefore_pickup_time_fee, 2);
        $distanceBeforeTripStarts = round($pickupDistance * $trip->dbefore_pickup_mile_fee, 2);
        $pickupFee = $trip->dpickup_fee;
        $perMile = round($totalDistance * $trip->dper_mile_fee, 2);
        $perMinutes = round($totalDuration * $trip->dper_minute_fee, 2);
        $waitTime = $waitingMinutes * $trip->dwait_fee;
        $surgeCharge = $trip->dsurge_fee;
        $atlantaFee = $trip->datlanta_fee;
        $emFee = 0;
        $etFee = 0;
        $gaTax = 0;
        $tip = $trip->tip;

        $charges = [
            'totalDistance' => $totalDistance,
            'pickupDistance' => $pickupDistance,
            'timeBeforeTripStarts' => $timeBeforeTripStarts,
            'distanceBeforeTripStarts' => $distanceBeforeTripStarts,
            'waitingTime' => $trip->waiting_time,
            'driverFreeWaitTime' => $trip->driver_free_wait_time,
            'freeWaitTime' => $trip->free_wait_time,
            'pickupFee' => $pickupFee,
            'perMile' => $perMile,
            'perMinutes' => $perMinutes,
            'waitTime' => $waitTime,
            'surgeCharge' => $surgeCharge,
            'atlantaFee' => $atlantaFee,
            'emFee' => $emFee,
            'etFee' => $etFee,
            'gaTax' => $gaTax,
            'tip' => $tip
        ];
        $totalAmount = $timeBeforeTripStarts + $distanceBeforeTripStarts + $pickupFee + $perMile + $perMinutes + $waitTime + $surgeCharge + $atlantaFee + $emFee + $etFee + $gaTax + $tip;
        $charges['totalAmount'] = round($totalAmount, 2);

        return $charges;
    }
}