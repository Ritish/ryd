<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends AppModel
{
	protected $table = 'addresses';
    protected $primaryKey = 'id';
 	public $timestamps = false;
    
 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$address = new Addresses();

    	foreach($data as $k => $v)
    	{
    		$address->{$k} = $v;
    	}

    	$address->created = date('Y-m-d H:i:s');
    	$address->modified = date('Y-m-d H:i:s');
	    if($address->save())
	    {
	    	return $address;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$address = Addresses::find($id);
    	foreach($data as $k => $v)
    	{
    		$address->{$k} = $v;
    	}

    	$address->modified = date('Y-m-d H:i:s');
	    if($address->save())
	    {
	    	return $address;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$address = Addresses::find($id);
    	return $address->delete();
    }
}