<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Messages extends AppModel
{
	protected $table = 'messages';
    protected $primaryKey = 'id';
 	public $timestamps = false;
    
    /**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */
    public static function getListing(Request $request, $where = [])
    {
        $orderBy = $request->get('sort') ? $request->get('sort') : 'messages.id';
        $direction = $request->get('direction') ? $request->get('direction') : 'desc';
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = self::$paginationLimit;
        $offset = ($page - 1) * $limit;
        
        $listing = Messages::select([
                'messages.*'
            ])
            ->orderBy($orderBy, $direction);

        if(!empty($where))
        {
            foreach($where as $query => $values)
            {
                if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
            }
        }

        // Put offset and limit in case of pagination
        if($page !== null && $page !== "" && $limit !== null && $limit !== "")
        {
            $listing->offset($offset);
            $listing->limit($limit);
        }
        
        $listing = $listing->paginate($limit);

        return $listing;
    }

 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$message = new Messages();

    	foreach($data as $k => $v)
    	{
    		$message->{$k} = $v;
    	}

    	$message->created = date('Y-m-d H:i:s');
    	$message->modified = date('Y-m-d H:i:s');
	    if($message->save())
	    {
	    	return $message;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$message = Messages::find($id);
    	foreach($data as $k => $v)
    	{
    		$message->{$k} = $v;
    	}

    	$message->modified = date('Y-m-d H:i:s');
	    if($message->save())
	    {
	    	return $message;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$message = Messages::find($id);
    	return $message->delete();
    }
}