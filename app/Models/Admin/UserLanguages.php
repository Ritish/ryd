<?php

namespace App\Models\Admin;

use App\Models\AppModel;

class UserLanguages extends AppModel
{
	protected $table = 'user_languages';
    protected $primaryKey = 'id';
 	public $timestamps = false;
}