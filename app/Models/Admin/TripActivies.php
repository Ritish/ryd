<?php

namespace App\Models\Admin;

use App\Models\AppModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripActivies extends AppModel
{
	protected $table = 'trip_activities';
    protected $primaryKey = 'id';
 	public $timestamps = false;
    
 	/**
    * To insert
    * @param $where
    * @param $orderBy
    */
    public static function create($data)
    {
    	$activity = new TripActivies();

    	foreach($data as $k => $v)
    	{
    		$activity->{$k} = $v;
    	}

    	$activity->created = date('Y-m-d H:i:s');
    	$activity->modified = date('Y-m-d H:i:s');
	    if($activity->save())
	    {
	    	return $activity;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To update
    * @param $id
    * @param $where
    */
    public static function modify($id, $data)
    {
    	$activity = TripActivies::find($id);
    	foreach($data as $k => $v)
    	{
    		$activity->{$k} = $v;
    	}

    	$activity->modified = date('Y-m-d H:i:s');
	    if($activity->save())
	    {
	    	return $activity;
	    }
	    else
	    {
	    	return null;
	    }
    }

    /**
    * To delete
    * @param $id
    */
    public static function remove($id)
    {
    	$activity = TripActivies::find($id);
    	return $activity->delete();
    }

    public static function get($id)
    {
        $activity = TripActivies::find($id);
        return $activity;
    }
}