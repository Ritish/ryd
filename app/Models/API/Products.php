<?php

namespace App\Models\API;

use App\Models\Admin\Products as AdminProducts;
use App\Models\Admin\ProductCategoryRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\FileSystem;

class Products extends AdminProducts
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    public $timestamps = false;
    /**
    * Get resize images
    *
    * @return array
    */
    public function getImageAttribute($value)
    {
        return $value ? FileSystem::getAllSizeImages($value) : null;
    }

    /**
    * To search and get pagination listing
    * @param Request $request
    * @param $limit
    */

    public static function getListing(Request $request, $where = [])
    {
        $userId = ApiAuth::getLoginId();
    	$orderBy = $request->get('sort') ? $request->get('sort') : 'products.id';
    	$direction = $request->get('direction') ? $request->get('direction') : 'desc';
    	$page = $request->get('page') ? $request->get('page') : 1;
    	$limit = 12;
    	$offset = ($page - 1) * $limit;
    	   
        $select = [
            'products.id',
            'products.title',
            'products.slug',
            'products.price',
            'products.image',
            'products.sale_price',
            DB::raw('concat(shop_owner.first_name, " ", shop_owner.last_name) as shop_name'),
            'shop_owner.id as shop_owner_id',
            DB::raw('concat(shop_owner.first_name, " ",shop_owner.last_name) as shop_owner_name'),
            DB::raw('(CASE WHEN products.sale_price is null or products.sale_price = 0 THEN products.price ELSE products.sale_price END) as price_order'),
        ];


        /*if($request->get('latitude') && $request->get('longitude'))
        {
            $select[] = DB::raw("ROUND( SQRT( POW((69.1 * ((products.lat) - '".$request->get('latitude')."')), 2) + POW((53 * ((products.lng) - '".$request->get('longitude')."')), 2)), 1) AS distance");
        }*/

        if($userId)
        {
            $select[] = 'users_wishlist.id as wishlist_id';
        }

    	$listing = Products::select($select)
            ->leftJoin('shops', 'shops.id', '=', 'products.shop_id')
            ->leftJoin('users as shop_owner', 'shop_owner.id', '=', 'shops.user_id')
            ->where('products.status', 1);
        
        if($userId)
        {
            $listing->leftJoin('users_wishlist', function($join) use ($userId) {
                $join->on('users_wishlist.product_id', '=', 'products.id');
                $join->where('users_wishlist.user_id', '=', $userId);
            });
        }

	    if(!empty($where))
	    {
	    	foreach($where as $query => $values)
	    	{
	    		if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
	    	}
	    }

        if($request->get('categories'))
        {
            $ids = ProductCategoryRelation::distinct()->whereIn('category_id', $request->get('categories'))->pluck('product_id')->toArray();
            $listing->whereIn('products.id', $ids);
        }

        if($request->get('title'))
        {
            $listing->whereRaw('(products.title LIKE ? or shops.name LIKE ?)', [$request->get('title'), $request->get('title')]);
        }

        if($request->get('address'))
        {
            $listing->whereRaw('(products.address LIKE ? or products.postcode LIKE ?)', [$request->get('address'), $request->get('address')]);
        }

        switch ($orderBy) {
            case 'nearest':
                /*if($request->get('latitude') && $request->get('longitude') && $request->get('radius') > 0)
                {
                    $listing->having('distance', '<=', $request->get('radius'));
                    $listing->orderBy('distance', 'asc');
                }*/
            break;

            case 'higest_price':
                $listing->orderByRaw('(price_order) desc');
            break;

            case 'lowest_price':
                $listing->orderByRaw('(price_order) asc');
            break;

            case 'recent':
                $listing->orderByRaw('products.id desc');
            break;
            
            default:
                $listing->orderBy($orderBy, $direction);
            break;
        }
        

	    // Put offset and limit in case of pagination
	    if($page !== null && $page !== "" && $limit !== null && $limit !== "")
	    {
	    	$listing->offset($offset);
	    	$listing->limit($limit);
	    }

	    $listing = $listing->paginate($limit);

	    return $listing;
    }
}