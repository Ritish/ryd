<?php

namespace App\Models\API;

use App\Models\Admin\Trips as AdminTrips;
use App\Models\API\TripDestinations;
use App\Models\Admin\TripWaitings;
use App\Models\Admin\Settings;
use App\Models\Admin\Services;
use Illuminate\Http\Request;
use App\Libraries\General;
use App\Libraries\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trips extends AdminTrips
{
    public static function getListing(Request $request, $where = [])
    {
        $orderBy = $request->get('sort') ? $request->get('sort') : 'trips.id';
        $direction = $request->get('direction') ? $request->get('direction') : 'desc';
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = self::$paginationLimit;
        $offset = ($page - 1) * $limit;
        
        $listing = Trips::select([
                'trips.id',
                'trips.user_id',
                'trips.service_id',
                'trips.vehicle_id',
                'trips.driver_id',
                'trips.pickup_address',
                'trips.to_address',
                DB::raw('(trips.distance + before_pickup_distance) as distance'),
                DB::raw('ADDTIME(waiting_time, duration_time) as duration'),
                'trips.customer_total',
                'trips.driver_total',
                'trips.status',
                'trips.created',
                'users.first_name as user_first_name',
                'users.last_name as user_last_name',
                'users.phonenumber as user_phonenumber',
                'driver.first_name as driver_first_name',
                'driver.last_name as driver_last_name',
                'driver.phonenumber as driver_phonenumber',
                'services.title as service_title',
                'vehicles.number as vehicle_number',
                'vehicle_brands.title as vehicle_brand',
                'vehicle_models.title as vehicle_model',

            ])
            ->leftJoin('users', 'users.id', '=', 'trips.user_id')
            ->leftJoin('users as driver', 'driver.id', '=', 'trips.driver_id')
            ->leftJoin('services', 'services.id', '=', 'trips.service_id')
            ->leftJoin('vehicles', 'vehicles.id', '=', 'trips.vehicle_id')
            ->leftJoin('vehicle_brands', 'vehicle_brands.id', '=', 'vehicles.brand_id')
            ->leftJoin('vehicle_models', 'vehicle_models.id', '=', 'vehicles.model_id')
            ->orderBy($orderBy, $direction);

        if(!empty($where))
        {
            foreach($where as $query => $values)
            {
                if(is_array($values))
                    $listing->whereRaw($query, $values);
                elseif(!is_numeric($query))
                    $listing->where($query, $values);
                else
                    $listing->whereRaw($values);
            }
        }

        // Put offset and limit in case of pagination
        if($page !== null && $page !== "" && $limit !== null && $limit !== "")
        {
            $listing->offset($offset);
            $listing->limit($limit);
        }
        
        $listing = $listing->paginate($limit);

        return $listing;
    }

	public static function createTrip($data)
	{
		$destinations = $data['destinations'];
		$data['destinations'] = count($destinations);

		$trip = Trips::create($data);
		if($trip)
		{
			foreach($destinations as $d)
			{
				TripDestinations::create([
					'trip_id' => $trip->id,
					'address' => $d['address'],
					'lat' => $d['lat'],
					'lng' => $d['lng'],
					'completed' => 0
				]);
			}
		}

		return $trip ? $trip->id : null;
	}

	public static function updatePrices($tripId)
    {
    	$trip = Trips::find($tripId);
    	$destinations = TripDestinations::where(['trip_id' => $tripId, 'completed' => 1])
    		->orderBy('id', 'asc')
    		->get();
    	$waitings = TripWaitings::where(['trip_id' => $tripId])
            ->whereNotNull('start_time')
            ->whereNotNull('end_time')
    		->orderBy('id', 'asc')
    		->get();

        $multiplier = Services::getPriceValue($trip->service_id, 'distance_time_multiplier');
        $pickupDistance = General::distance($trip->from_address_lat, $trip->from_address_lng, $trip->pickup_address_lat, $trip->pickup_address_lng);
        $pickupMinutes = $pickupDistance * $multiplier;

        /** Destination Calculation **/
        $lastLAT = $trip->pickup_address_lat;
        $lastLNG = $trip->pickup_address_lng;
        $totalDistance = 0;
        $totalDuration = 0;
        foreach($destinations as $d)
        {
            $distance = General::distance($trip->pickup_address_lat, $trip->pickup_address_lng, $d->lat, $d->lng);
            $minutes = $distance * $multiplier;
            $totalDistance += $distance;
            $totalDuration += $minutes;

            $lastLAT = $d->lat;
            $lastLNG = $d->lng;
        }
        /** Destination Calculation **/

        /** Waiting Calculation **/
        $actualWatingMinutes = 0;
        $freeWaitTime = Services::getPriceValue($trip->service_id, 'free_wait_time');

        foreach($waitings as $w)
        {
            $seconds = $w->end_time - $w->start_time;
            $minutes = $seconds / 60;
            $actualWatingMinutes += $minutes;
        }

        $waitingMinutes = $actualWatingMinutes - Services::getPriceValue($trip->service_id, 'free_wait_time');
        $waitingMinutes = $waitingMinutes > 0 ? $waitingMinutes : 0;
        /** Waiting Calculation **/

        /** Customer Trip Save Pricing **/
        $trip->before_pickup_duration = DateTime::secondsToTime($pickupMinutes*60);
        $trip->before_pickup_distance = $pickupDistance;
        $trip->waiting_time = DateTime::secondsToTime($waitingMinutes*60);
        $trip->distance = $totalDistance;
        $trip->duration_time = DateTime::secondsToTime($totalDuration*60);
        $trip->booking_fee = Services::getPriceValue($trip->service_id, 'booking_fee');
        $trip->pickup_fee = Services::getPriceValue($trip->service_id, 'pickup_fee');
        $trip->per_mile_fee = Services::getPriceValue($trip->service_id, 'per_mile_fee');
        $trip->per_minute_fee = Services::getPriceValue($trip->service_id, 'per_minute_fee');
        $trip->wait_fee = Services::getPriceValue($trip->service_id, 'wait_fee');
        $trip->surge_fee = Services::getPriceValue($trip->service_id, 'surge_fee');
        $trip->atlanta_fee = Services::getPriceValue($trip->service_id, 'atlanta_fee');
        $trip->em_fee = Services::getPriceValue($trip->service_id, 'em_fee');
        $trip->et_fee = Services::getPriceValue($trip->service_id, 'et_fee');
        $trip->ga_tax = Services::getPriceValue($trip->service_id, 'ga_tax');
        $trip->pickup_duration_fee = Services::getPriceValue($trip->service_id, 'pickup_duration_fee');
        $trip->pickup_distance_fee = Services::getPriceValue($trip->service_id, 'distance_fee');
        /** Customer Trip Save Pricing **/

        /** Customer Trip Calculations **/
        $timeBeforeTripStarts = round($pickupMinutes * $trip->pickup_duration_fee, 2);
        $distanceBeforeTripStarts = round($pickupDistance * $trip->pickup_distance_fee, 2);
        $bookingFee = $trip->booking_fee;
        $pickupFee = $trip->pickup_fee;
        $perMile = round($totalDistance * $trip->per_mile_fee, 2);
        $perMinutes = round($totalDuration * $trip->per_minute_fee, 2);
        $waitTime = $waitingMinutes * $trip->wait_fee;
        $surgeCharge = $trip->surge_fee;
        $atlantaFee = $trip->atlanta_fee;
        $emFee = $trip->em_fee;
        $etFee = $trip->et_fee;
        $gaTax = $trip->ga_tax;
        
        $totalAmount = $timeBeforeTripStarts + $distanceBeforeTripStarts + $bookingFee + $pickupFee + $perMile + $perMinutes + $waitTime + $surgeCharge + $atlantaFee + $emFee + $etFee + $gaTax;
        $totalAmount = round($totalAmount, 2);

        $trip->customer_total = $totalAmount;
        /** Customer Trip Calculations **/

        /** Driver Trip Save Pricing **/
        $waitingMinutes = $actualWatingMinutes - Services::getPriceValue($trip->service_id, 'driver_free_wait_time');
        $waitingMinutes = $waitingMinutes > 0 ? $waitingMinutes : 0;
        $trip->driver_waiting_time = DateTime::secondsToTime($waitingMinutes*60);
        $trip->dpickup_fee = Services::getPriceValue($trip->service_id, 'driver_pickup_fee');
        $trip->dper_mile_fee = Services::getPriceValue($trip->service_id, 'driver_per_mile_fee');
        $trip->dper_minute_fee = Services::getPriceValue($trip->service_id, 'driver_per_minute_fee');
        $trip->dwait_fee = Services::getPriceValue($trip->service_id, 'driver_wait_fee');
        $trip->dsurge_fee = Services::getPriceValue($trip->service_id, 'driver_surge_fee');
        $trip->datlanta_fee = Services::getPriceValue($trip->service_id, 'driver_atlanta_fee');
        $trip->dem_fee = Services::getPriceValue($trip->service_id, 'driver_em_fee');
        $trip->det_fee = Services::getPriceValue($trip->service_id, 'driver_et_fee');
        $trip->dbefore_pickup_time_fee = Services::getPriceValue($trip->service_id, 'driver_pickup_duration_fee');
        $trip->dbefore_pickup_mile_fee = Services::getPriceValue($trip->service_id, 'driver_distance_fee');
        /** Driver Trip Save Pricing **/

        /** Driver Trip Calculations **/
        $timeBeforeTripStarts = round($pickupMinutes * $trip->dbefore_pickup_time_fee, 2);
        $distanceBeforeTripStarts = round($pickupDistance * $trip->dbefore_pickup_mile_fee, 2);
        $pickupFee = $trip->dpickup_fee;
        $perMile = round($totalDistance * $trip->dper_mile_fee, 2);
        $perMinutes = round($totalDuration * $trip->dper_minute_fee, 2);
        $waitTime = $waitingMinutes * $trip->dwait_fee;
        $surgeCharge = $trip->dsurge_fee;
        $atlantaFee = $trip->datlanta_fee;
        $emFee = 0;
        $etFee = 0;
        $gaTax = 0;
        
        $totalAmount = $timeBeforeTripStarts + $distanceBeforeTripStarts + $pickupFee + $perMile + $perMinutes + $waitTime + $surgeCharge + $atlantaFee + $emFee + $etFee + $gaTax;
        $totalAmount = round($totalAmount, 2);

        $trip->driver_total = $totalAmount;
        /** Customer Trip Calculations **/

        $trip->save();
        return $trip;
    }
}