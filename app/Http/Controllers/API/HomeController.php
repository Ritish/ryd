<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Libraries\PositionStack;
use App\Models\API\SearchSugessions;
use App\Models\API\Products;
use Illuminate\Support\Facades\DB;

class HomeController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

	function information(Request $request)
	{
		$sugessions = [];
		if(Settings::get('home_page_search_sugession'))
		{
			$sugessions = SearchSugessions::select([
					'search_sugessions.id',
					'search_sugessions.title',
					'search_sugessions.slug',
					'search_sugessions.image'
				])
				->where('status', 1)
				->orderBy('search_sugessions.id', 'desc')
				->get();
		}

		return Response()->json([
		    	'status' => true,
	    		'layout' => Settings::get('home_page_layout'),
	    		'banner' => Settings::get('home_page_banner'),
	    		'text' => Settings::get('home_page_text'),
	    		'search_text' => Settings::get('home_page_search_text'),
	    		'sugessions' => $sugessions
		    ]);
	}

	function products(Request $request)
	{
		$products = [];
		if($request->get('search'))
		{
			$search = $request->get('search');
			$products = Products::getAll([
					'id',
					'title'
				],
				[
					'products.title LIKE ?' => [ "%{$search}%" ]
				],
				'products.title asc'
			);
		}
		
		return Response()->json([
		    	'status' => true,
	    		'products' => $products
		    ]);
	}

	function address(Request $request)
	{
		$address = [];
		if($request->get('search'))
		{
			$search = $request->get('search');
			$address = Products::select([
					'postcode as label'
				])
				->where('postcode', 'LIKE', $search . '%')
				->groupBy('postcode')
				->limit(40)
				->get();
			
			if($address->count() < 1)
			{
				$address = Products::select([
						'address as label'
					])
					->where('address', 'LIKE', '%' . $search . '%')
					->groupBy('address')
					->limit(40)
					->get();
			}
		}
		
		return Response()->json([
		    	'status' => true,
	    		'address' => $address
		    ]);
	}
}