<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Models\Admin\Pages;
use App\Models\Admin\Messages;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;
use App\Models\Admin\States;
use App\Models\Admin\Languages;
use App\Models\API\ApiAuth;

class PagesController extends AppController
{

	function get(Request $request, $slug)
	{
		$page = Pages::select(['id', 'title', 'description'])
			->where('slug', 'LIKE', $slug)
			->where('status', 1)
			->first();
		if($page)
		{
			return Response()->json([
		    	'status' => true,
		    	'page' => $page
	    	]);
		}
		else
		{
			return Response()->json([
		    	'status' => false,
		    	'message' => 'Page not exist.'
	    	], 400);
		}
	}

	function states()
	{
		$states = States::select(['id', 'name', 'state_code'])->orderBy('state_code', 'asc')->get();
		return Response()->json([
			'status' => true,
			'states' => $states
		]);
	}

	function languages()
	{
		$languages = languages::all();
		return Response()->json([
			'status' => true,
			'languages' => $languages
		]);
	}

	function messages(Request $request)
	{
		$userId = ApiAuth::getLoginId();
		if($userId)
		{
			$messages = Messages::getListing(
				$request, 
				[
					"(messages.from_id = ? or messages.to_id = ?)" => [$userId, $userId],
					"messages.trip_id" => $request->get('trip_id')
				]
			);

			return Response()->json([
				'status' => true,
				'messags' => $messages->items()
			], 200);
		}
		else
		{
			return Response()->json([
				'status' => false
			], 400);
		}
	}
}