<?php


namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Models\API\Users;
use App\Models\API\ApiAuth;
use App\Models\Admin\UsersDocuments;
use App\Libraries\General;
use App\Libraries\Twillio;
use App\Libraries\Stripe;


class AuthController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

	function authState(Request $request)
	{
		if(!ApiAuth::isLogin())
		{
			return Response()->json([
			    	'status' => false
			    ]);
		}
		else
		{
			return Response()->json([
			    	'status' => true
			    ]);	
		}
	}

	function signup(Request $request)
	{
		$allowed = ['country_code', 'phonenumber', 'password', 'first_name', 'last_name', 'device_id', 'device_type', 'device_name', 'fcm_token', 'user_type'];
    	if($request->has($allowed))
    	{
    		$validateData = $request->toArray();
    		$validateData['country_code'] = str_replace('+', '', $request->get('country_code'));
    		$validateData['phonenumber'] = str_replace(['+', '(', ')', '-'], ['', '', '', ''], $request->get('phonenumber'));
    		$validator = Validator::make(
	            $validateData,
	            [
	                'device_type' => 'required',
	                'user_type' => 'required',
	                'device_id' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
				    'device_name' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
				    'fcm_token' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
				    'country_code' => 'required',
	            	'phonenumber' => 'required|max:11',
	                'password' => [
	                	'required',
					    'min:8',
	                ],
	            ]
	        );

	        if(!$validator->fails())
	        {

	        	$countryCode = str_replace('+', '', $request->get('country_code'));
	        	$phonenumber = str_replace(['+', '(', ')', '-'], ['', '', '', ''], $request->get('phonenumber'));
	        	if(!Twillio::validateNumber($countryCode, $phonenumber))
	        	{
	        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Please enter a valid phone number.'
					    ], 400);
	        	}

	        	$exist = Users::select(['id', 'last_login'])
	        		->where('phonenumber', 'LIKE', $phonenumber)
	        		->where('country_code', $countryCode)
	        		->first();
	        	if(!$exist || !$exist->last_login)
	        	{
		        	$password = $request->get('password');
		        	$user = [
		        		'first_name' => $request->get('first_name'),
		        		'last_name' => $request->get('last_name'),
		        		'country_code' => $countryCode,
		        		'phonenumber' => $phonenumber,
		        		'password' => $request->get('password'),
		        		'token' => General::hash(64),
		        		'driver' => $request->get('user_type') == 'driver' ? 1 : 0
		        	];

		        	if($exist && !$exist->last_login)
		        		$user = Users::modify($exist->id, $user);
		        	else
		        		$user = Users::create($user);

		        	if($user)
		        	{
		        		Users::handleLanguages($user->id, [1]);

		        		if($user->driver)
		        		{
		        			$document = UsersDocuments::where('user_id',$user->id)->where('slug', 'background-check')->first();
		        			if(empty($document))
		        			{
		        				$document = new UsersDocuments();
		        			}

				        	$document->user_id = $user->id;
				        	$document->title = 'Background Check';
				        	$document->slug = 'background-check';
				        	$document->file = null;
				        	$document->verified = 0;
				        	$document->created = date('Y-m-d H:i:s');
				        	$document->modified = date('Y-m-d H:i:s');
				        	$document->save();
		        		}

		        		$otp = General::randomNumber(5);
		        		if(General::sendOtp($user->country_code, $user->phonenumber, $otp))
		        		{
		        			$user->otp = $otp;
		        			$user->save();
		        			
		        			return Response()->json([
						    	'status' => true,
						    	'message' => 'Please enter the otp, sent your phone number',
						    	'token' => $user->token,
						    	'otp' => $otp
						    ]);	
		        		}
		        		else
		        		{
		        			return Response()->json([
						    	'status' => false,
						    	'message' => 'Could not able to send OTP on your phonenumber. Please contact us.'
						    ], 400);		
		        		}
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Unable to register new user. Please try again.'
					    ], 400);
		        	}
		        }
		        else
		        {
		        	return Response()->json([
					    	'status' => false,
					    	'message' => 'Phone number is already registered with us. Please login to proceed.'
					    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

    function login(Request $request)
    {
    	$allowed = ['country_code', 'phonenumber', 'password', 'device_type', 'device_id', 'device_name', 'fcm_token', 'user_type'];

    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'country_code' => 'required',
	                'user_type' => 'required',
	                'phonenumber' => 'required',
	                'password' => 'required',
	                'device_type' => 'required',
	                'device_id' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
				    'device_name' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
				    'fcm_token' => Rule::requiredIf(function () use ($request) {
				        return $request->get('device_type') != 'web';
				    }),
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	//Validate recaptcha and throw error.
	        	if(Settings::get('client_recaptcha'))
	        	{	
	        		if(!$request->get('g-recaptcha-response') || !General::validateReCaptcha( $request->get('g-recaptcha-response') ))
	        		{
	        			$request->session()->flash('error', 'Captcha does not match. Please try again.');
		        		return redirect()->back()->withInput();
	        		}
	        	}


		    	// Make user login
		    	$user = ApiAuth::attemptLogin($request);
		    	if ($user) 
		        {
		        	if($request->get('user_type') == 'driver' && !$user->driver)
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not authorized.',
					    ], 403);
		        	}
		        	elseif($request->get('user_type') != 'driver' && $user->driver)
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not authorized.',
					    ], 403);
		        	}
		        	else
		        	{
			        	if(Settings::get('client_second_auth_factor'))
			        	{
			        		$otp = General::randomNumber(5);
			        		$user->token = General::hash(64);
			        		$user->otp = $otp;
			        		if($user->save())
			        		{
			        			$codes = [
			        				'{first_name}' => $user->first_name,
			        				'{last_name}' => $user->last_name,
			        				'{one_time_password}' => $otp
			        			];
			        			
			        			if(General::sendOtp($user->country_code, $user->phonenumber, $otp))
			        			{
				        			return Response()->json([
								    	'status' => true,
								    	'message' => 'We have sent an OTP on your provided phonenumber.',
								    	'token' => $user->token,
								    	'otp' =>  $otp
								    ]);
				        		}
				        		else
				        		{
				        			return Response()->json([
								    	'status' => false,
								    	'message' => 'Could not able to send OTP on your phonenumber. Please contact us.'
								    ], 400);	
				        		}
			        		}
			        		else
			        		{
			        			return Response()->json([
							    	'status' => false,
							    	'message' => 'Session could not be establised. Please try again.',
							    ], 400);
			        		}
			        	}
			        	else
			        	{
				        	$user = ApiAuth::makeLoginSession($request, $user);
				        	if($user)
				        	{
				        		return Response()->json([
							    	'status' => true,
							    	'message' => 'Login successfully',
							    	'user' => $user
							    ]);
				        	}
				        	else
				        	{
				        		return Response()->json([
							    	'status' => false,
							    	'message' => 'Session could not be establised. Please try again.',
							    ], 400);
				        	}
				        }
				    }
		        }
		        else
		        {
		        	return Response()->json([
				    	'status' => false,
				    	'message' => 'Phone number or password does not match, please try again.',
				    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
    }

    function secondAuth(Request $request)
    {
    	$allowed = ['token', 'otp'];
    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'token' => 'required',
	                'otp' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
		    	$user = ApiAuth::getRow([
					'token LIKE ?' => [
						$request->get('token')
					],
					'status' => 1
				]);

				if(!$user)
				{
					return Response()->json([
					    	'status' => false,
					    	'message' => 'User not registered with us with this phone number.',
					    ], 400);
				}

		    	if($request->get('otp'))
		    	{
		    		$otp = $request->get('otp');
					if($user->otp == $otp)
					{
						$user->otp = null;
						$user->token = null;
						if($user->save())
						{
							$loginUser = ApiAuth::makeLoginSession($request, $user);
				        	if($loginUser)
				        	{
				        		if(!$user->driver && !$user->stripe_customer_id)
				        		{
				        			$response = Stripe::createCustomer($user);
				        			if($response)
				        			{
				        				return Response()->json([
									    	'status' => true,
									    	'message' => 'Login successfully',
									    	'user' => $loginUser
									    ]);
				        			}
				        			else
				        			{
				        				return Response()->json([
									    	'status' => false,
									    	'message' => 'Stripe could not be linked with your account. Please contact us.',
									    ], 400);
				        			}
				        		}
				        		else
				        		{
					        		return Response()->json([
								    	'status' => true,
								    	'message' => 'Login successfully',
								    	'user' => $loginUser
								    ]);
					        	}
				        	}
				        	else
				        	{
				        		return Response()->json([
							    	'status' => false,
							    	'message' => 'Session could not be establised. Please try again.',
							    ], 400);
				        	}
						}
						else
						{
							return Response()->json([
						    	'status' => false,
						    	'message' => 'User could not be saved. Please try again.',
						    ], 400);
						}
					}
					else
					{
						return Response()->json([
						    	'status' => false,
						    	'message' => 'The one time password is incorrect.',
						    ], 400);
					}
		    	}
		    	else
		    	{
		    		return Response()->json([
					    	'status' => false,
					    	'message' => 'Please enter your one time password.',
					    ], 400);
		    	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
    }

    function forgotPassword(Request $request)
    {
    	$allowed = ['country_code', 'phonenumber'];
    	if($request->has($allowed))
	    {
	    	$phonenumber = str_replace(['+', '(', ')', '-'], '', $request->get('phonenumber'));
        	$countryCode = str_replace(['+', '(', ')', '-'], '', $request->get('country_code'));
	    	if($request->get('country_code') && $request->get('phonenumber'))
	    	{
	    		$user = Users::getRow([
	    			'(country_code LIKE ? and phonenumber LIKE ?)' => [$countryCode, $phonenumber],
	    			'status' => 1
	    		]);

	    		if($user)
	    		{
	    			$user->token = General::hash(64);
	    			$otp = General::randomNumber(5);
	    			$user->otp = $otp;
	    			if($user->save())
	    			{
		        		if(General::sendOtp($user->country_code, $user->phonenumber, $otp))
		        		{	
		        			return Response()->json([
						    	'status' => true,
						    	'message' => 'We have sent you an OTP on your registered phone number. Enter OTP to change password.',
						    	'token' => $user->token,
						    	'otp' => $otp
						    ]);	
		        		}
		        		else
		        		{
		        			return Response()->json([
						    	'status' => false,
						    	'message' => 'Could not able to send OTP on your phonenumber. Please contact us.'
						    ], 400);		
		        		}
	    			}
	    			else
	    			{
	    				return Response()->json([
						    	'status' => false,
						    	'message' => 'Something went wrong. Please try again.',
						    ], 400);
	    			}
	    		}
	    		else
	    		{
	    			return Response()->json([
					    	'status' => false,
					    	'message' => 'The phone number entered cannot be found, please try again.',
					    ], 400);
	    		}
	    	}
	    	else
	    	{
	    		return Response()->json([
				    	'status' => false,
				    	'message' => 'Please enter your registered phone number to recover password.',
				    ], 400);
	    	}
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
    }

    function verifyForgotPassword(Request $request)
    {
    	$allowed = ['token', 'code'];
    	if($request->has($allowed))
	    {
	    	if($request->get('token') && $request->get('code'))
	    	{
	    		$user = Users::getRow([
	    			'token' => $request->get('token'),
	    			'status' => 1
	    		]);

	    		if($user)
	    		{
	    			if($user->otp == $request->get('code'))
	    			{
		    			$user->token = General::hash(64);
		    			$user->otp = null;
		    			if($user->save())
		    			{
			        		return Response()->json([
						    	'status' => true,
						    	'message' => 'OTP verified.',
						    	'token' => $user->token
						    ]);
		    			}
		    			else
		    			{
		    				return Response()->json([
							    	'status' => false,
							    	'message' => 'Something went wrong. Please try again.',
							    ], 400);
		    			}
		    		}
		    		else
		    		{
		    			return Response()->json([
						    	'status' => false,
						    	'message' => 'Entered OTP is invalid.',
						    ], 400);
		    		}
	    		}
	    		else
	    		{
	    			return Response()->json([
					    	'status' => false,
					    	'message' => 'Invaid token or user.',
					    ], 400);
	    		}
	    	}
	    	else
	    	{
	    		return Response()->json([
				    	'status' => false,
				    	'message' => 'Please enter your registered phone number to recover password.',
				    ], 400);
	    	}
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
    }

    function recoverPassword(Request $request)
    {
    	$allowed = ['new_password', 'confirm_password', 'token'];
    	if($request->has($allowed))
	    {
    		$data = $request->toArray();

            $validator = Validator::make(
	            $request->toArray(),
	            [
	            	'token' => 'required',
	                'new_password' => [
	                	'required',
					    'min:8'
	                ],
	                'confirm_password' => [
	                	'required',
					    'min:8'
	                ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$user = Users::getRow([
	    			'token like ?' => [$data['token']],
	    			'(otp is null)'
	    		]);

	    		if($user)
				{
	        		if($data['new_password'] && $data['confirm_password'] && $data['new_password'] == $data['confirm_password'])
	        		{
	        			$user->password = $data['new_password'];
	        			$user->token = null;
	        			if($user->save())
	        			{
		    				return Response()->json([
						    	'status' => true,
						    	'message' => 'Password updated successfully. Login with new credentials to proceed.'
						    ], 200);
	        			}
	        			else
	        			{
	        				return Response()->json([
						    	'status' => false,
						    	'message' => 'New password could be updated.'
						    ], 400);
	        			}
	        		}
	        		else
	        		{
	        			return Response()->json([
						    	'status' => false,
						    	'message' => 'Password did not match.'
						    ], 400);
	        		}
	        	}
	        	else
	        	{
	        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Invalid token.'
					    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
				    	'status' => false,
				    	'message' => current( current( $validator->errors()->getMessages() ) )
				    ], 400);
		    }
		
		}
		else
		{
			return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
		}
    }

    function emailVerification(Request $request, $hash, $email = null)
    {
    	$user = Users::getRow([
				'token like ?' => [$hash]
			]);

		if($user)
		{
			// $user->token = null;
			$user->verified_at = date('Y-m-d H:i:s');
			
			if($email)
			{
				$email = General::decrypt($email);
				if($email)
				{
					$user->email = $email;
				}
				else
				{
					return Response()->json([
				    	'status' => false,
				    	'message' => 'Email could not be changed. Something went wrong.'
				    ], 400);
				}
			}

			if($user->save())
			{
				return Response()->json([
			    	'status' => true,
			    	'message' => 'Your email verification has been completed.'
			    ], 200);
			}
			else
			{
				return Response()->json([
			    	'status' => false,
			    	'message' => 'Email could not be verified. The link is expired or used.'
			    ], 400);
			}
		}
		else
		{
			return Response()->json([
			    	'status' => false,
			    	'message' => 'Email could not be verified. The link is expired or used.',
			    ], 400);
		}
    }

    function logout(Request $request)
    {
    	AdminAuth::logout();
    	
    	return redirect()->route('admin.login');	
    }
}
