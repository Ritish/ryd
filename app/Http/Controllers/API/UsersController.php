<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Libraries\Stripe;
use App\Models\API\Users;
use App\Models\Admin\UserLanguages;
use App\Models\API\ApiAuth;
use App\Models\API\Trips;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;
use App\Models\Admin\Services; 
use App\Models\Admin\UserServices;

class UsersController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

	function profile(Request $request)
	{
		
		$user = ApiAuth::getLoginUser();
		if($user)
		{
			return Response()->json([
		    	'status' => true,
	    		'user' => $user
		    ]);
		}
		else
		{
			return Response()->json([
		    	'status' => false
		    ], 403);
		}
	}


	function updateProfile(Request $request)
	{
		
		$userId = ApiAuth::getLoginId();

		$allowed = ['first_name', 'last_name', 'email', 'gender'];
    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'first_name' => 'required',
	            	'last_name' => 'required',	
	            	'email' => 'required|email',
	            	'gender' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	
	        	$exist = Users::select(['id'])
	        		->where('email', 'LIKE', $request->get('email'))
	        		->where('id', '!=', $userId)
	        		->first();
	        	if(!$exist)
	        	{
		        	if($user = Users::find($userId))
		        	{
			        	$data = [
			        		'first_name' => $request->get('first_name'),
			        		'last_name' => $request->get('last_name'),
			        		'email' => $request->get('email'),
			        		'gender' => $request->get('gender')
			        	];	

			        	if($user = Users::modify($userId, $data))
			        	{
			        		$existLang = UserLanguages::where('user_id', $user->id)->count();
			        		if(!empty($request->get('languages')) )
			        		{
			        			Users::handleLanguages($user->id, $request->get('languages'));
			        		}
			        		elseif($existLang < 1)
			        		{
			        			Users::handleLanguages($user->id, [1]);
			        		}
	        		
			        		if($user->stripe_customer_id)
			        		{
			        			Stripe::updateCustomer($user);
			        		}
			        		
		    				$user = ApiAuth::getLoginUser();
		    				if($user)
		    				{
		    					if($user->gender)
		    					{
			    					$services = UserServices::where('user_id', $user->id)->count();
			    					if($services < 1)
			    					{
			    						$services = Services::whereIn('allowed_gender', [strtolower($user->gender), 'both'])->pluck('id')->toArray();
			    						Users::handleServices($user->id, $services);
			    					}
			    				}

		    					return Response()->json([
							    	'status' => true,
							    	'message' => 'Profile Updated',
							    	'user' => $user
							    ]);
		    				}
		    				else
		    				{
		    					return Response()->json([
							    	'status' => false,
							    	'message' => 'Unable to update profile. Please try again.'
							    ], 400);
		    				}
			        	}
			        	else
			        	{
			        		return Response()->json([
						    	'status' => false,
						    	'message' => 'Unable to update profile. Please try again.'
						    ], 400);
			        	}
			        }
			        else
			        {
			        	return Response()->json([
					    	'status' => false
					    ], 403);
			        }
			    }
			    else
			    {
			    	return Response()->json([
					    	'status' => false,
					    	'message' => 'Email address is already registered with us.'
					    ], 400);
			    }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function updateEmail(Request $request)
	{
		
		$allowed = ['email'];
    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'email' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$userId = ApiAuth::getLoginId();
	        	$exist = Users::select(['id'])
	        		->where('email', 'LIKE', $request->get('email'))
	        		->where('id', '!=', $userId)
	        		->first();
	        	if(!$exist)
	        	{
		        	if($user = Users::find($userId))
		        	{
	        			$user->email = $request->get('email');
	        			if($user->save())
	        			{
		        			return Response()->json([
						    	'status' => true,
						    	'message' => 'Email updated.',
						    	'user' => ApiAuth::getLoginUser()
						    ]);
	    				}
	    				else
	    				{
	    					return Response()->json([
						    	'status' => false,
						    	'message' => 'Email could not be save. Please try again.'
						    ], 400);
	    				}
			        }
			        else
			        {
			        	return Response()->json([
					    	'status' => false
					    ], 403);
			        }
			    }
			    else
			    {
			    	return Response()->json([
					    	'status' => false,
					    	'message' => 'Email is already registered with us.'
					    ], 400);
			    }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}


	function updatePhonenumber(Request $request)
	{
		$allowed = ['phonenumber', 'country_code'];
    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'phonenumber' => 'required',
	            	'country_code' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$countryCode = str_replace('+', '', $request->get('country_code'));
	        	$userId = ApiAuth::getLoginId();
	        	$exist = Users::select(['id', 'last_login'])
	        		->where('phonenumber', 'LIKE', $request->get('phonenumber'))
	        		->where('country_code', $countryCode)
	        		->where('id', '!=', $userId)
	        		->first();
	        	if(!$exist)
	        	{
		        	if($user = Users::find($userId))
		        	{
	        			$user->country_code = $request->get('country_code');
	        			$user->phonenumber = $request->get('phonenumber');
	        			if($user->save())
	        			{
		        			return Response()->json([
						    	'status' => true,
						    	'message' => 'Phone number updated.',
						    	'user' => ApiAuth::getLoginUser()
						    ]);
	    				}
	    				else
	    				{
	    					return Response()->json([
						    	'status' => false,
						    	'message' => 'Phone number could not be update. Please try again.'
						    ], 400);
	    				}
			        }
			        else
			        {
			        	return Response()->json([
					    	'status' => false
					    ], 403);
			        }
			    }
			    else
			    {
			    	return Response()->json([
					    	'status' => false,
					    	'message' => 'Phone number is already registered with us.'
					    ], 400);
			    }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}



	function changePassword(Request $request)
	{
		$allowed = ['old_password', 'new_password', 'confirm_password'];
    	if($request->has($allowed))
    	{
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'old_password' => [
	                	'required',
					    'min:8'
	                ],
	                'new_password' => [
	                	'required',
					    'min:8'
	                ],
	                'confirm_password' => [
					    'required'
	                ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$userId = ApiAuth::getLoginId();
	        	if($user = Users::find($userId))
	        	{
	        		$data = $request->toArray();

	        		if(Hash::check($data['old_password'], $user->password))
		        	{
		        		if($data['new_password'] == $data['confirm_password'])
		        		{
			        		$user->password = $data['new_password'];

		        			if($user->save())
		        			{
		        				return Response()->json([
							    	'status' => true,
							    	'message' => 'Password updated successfully.'
							    ]);
		        			}
		        			else
		        			{
			    				return Response()->json([
							    	'status' => false,
							    	'message' => 'New password could be updated.'
							    ], 400);
		        			}
		        		}
		        		else
		        		{
		        			return Response()->json([
							    	'status' => false,
							    	'message' => 'Password does not match with confirmed password.'
							    ], 400);
		        		}
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Your current password is incorrect.'
					    ], 400);
		        	}
		        }
		        else
		        {
		        	return Response()->json([
				    	'status' => false
				    ], 403);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function uploadPicture(Request $request)
    {
    	$userId = ApiAuth::getLoginId();

		$allowed = ['file'];
		if($request->has($allowed))
		{
    		$data = $request->toArray();

            $validator = Validator::make(
	            $request->toArray(),
	            [
	                'file' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
		    	$user = Users::find($userId);
		    	if($user)
		    	{
		    		$oldImage = $user->image;

	    			$file = FileSystem::uploadBase64Image(
	    				$request->get('file'),
	    				'users'
	    			);
	    			
	    			if($file)
	    			{
	    				$user->image = $file;
	    				if($user->save())
	    				{
	    					$originalName = FileSystem::getFileNameFromPath($file);
	    					
	    					FileSystem::resizeImage($file, 'M-' . $originalName, "350*350");
	    					FileSystem::resizeImage($file, 'S-' . $originalName, "100*100");
	    					
	    					if( $oldImage && isset($oldImage['original']) )
	    					{
	    						FileSystem::deleteFile($oldImage['original']);
	    					}

	    					
	    					return Response()->json([
						    	'status' => true,
						    	'message' => 'Picture uploaded successfully.',
						    	'user' => ApiAuth::getLoginUser()
						    ]);		
	    				}
	    				else
	    				{
	    					FileSystem::deleteFile($file);
	    					return Response()->json([
						    	'status' => false,
						    	'message' => 'Picture could not be uploaded.'
						    ], 400);	
	    				}
	    				
	    			}
	    			else
	    			{
	    				return Response()->json([
					    	'status' => false,
					    	'message' => 'Picture could not be upload.'
					    ], 400);		
	    			}
				}
				else
				{
					return Response()->json([
				    	'status' => false,
				    	'message' => 'User is missing.'
				    ], 400);
				}
			}
			else
			{
				return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
			}
		}
		else
		{
			return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
		}
    }

	function deleteAccount(Request $request)
	{
		
		$id = ApiAuth::getLoginId();
		$user = Users::find($id);
		if($user)
		{
			if($user->delete())
			{
				Shops::where('user_id', $user->id)->delete();
				Products::where('user_id', $user->id)->delete();
				
				return Response()->json([
			    	'status' => true,
			    ]);
			}
			else
			{
				return Response()->json([
			    	'status' => false,
			    	'message' => 'You account could not deleted. Please try again.'
			    ], 400);
			}
		}
		else
		{
			return Response()->json([
			    	'status' => false,
			    	'message' => 'Something went wrong.'
			    ], 400);
		}
	}

	public static function getDriverStats(Request $request)
	{
		$userId = ApiAuth::getLoginId();

		$last = Trips::select(['driver_total'])
			->where('driver_id', $userId)
			->whereIn('status', [5, 6, 7])
			->orderBy('id', 'desc')
			->first();
		$last = $last && $last->driver_total > 0 ? $last->driver_total : 0;

		// $today = Trips::where('driver_id', $userId)
		// 	->whereIn('status', [5, 6, 7])
		// 	->where('created', '>=', date('Y-m-d 00:00:01'))
		// 	->sum('driver_total');

		$weekly = Trips::where('driver_id', $userId)
			->whereIn('status', [5, 6, 7])
			->where('created', '>=', date('Y-m-d 00:00:01', strtotime('monday this week')))
			->sum('driver_total');

		return Response()->json([
	    	'status' => true,
	    	'data' => [
	    		[
	    			'amount' => (string) ($last > 0 ?  round($last, 2) : '0.00'),
	    			'label' => 'Last Trip',
	    		],
	    		[
		    		'amount' => (string) ($weekly > 0 ?  round($weekly, 2) : '0.00'),
		    		'label' => 'Weekly'
	    		]
	    	]
	    ]);
	}
}