<?php
/**
 * Products Class
 *
 * @package    ProductsController
 * @copyright  Ritish Kumar
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Models\API\ApiAuth;
use App\Libraries\General;
use App\Models\API\Products;
use App\Models\API\ProductCategories;
use App\Models\API\UsersWishlist;
use App\Models\Admin\ProductCategoryRelation;
use App\Libraries\FileSystem;

class ProductsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

	function listing(Request $request)
	{
		$where = [];
		$products = Products::getListing($request, $where);
		$items = $products->items();
	    return Response()->json([
	    	'status' => true,
            'products' => $items,
            'page' => $products->currentPage(),
            'counter' => $products->perPage(),
            'count' => $products->total(),
            'pagination_counter' => $products->currentPage() * $products->perPage()
        ], 200);
	}

	function categories(Request $request)
	{
		$where = [];
		$categories = ProductCategories::getListing($request, $where);
		$items = $categories->items();
	    return Response()->json([
	    	'status' => true,
            'categories' => $items,
            'page' => $categories->currentPage(),
            'counter' => $categories->perPage(),
            'count' => $categories->total(),
            'pagination_counter' => $categories->currentPage() * $categories->perPage()
        ], 200);
	}

	function makeWishlist(Request $request)
	{
		
		if(!ApiAuth::isLogin())
		{
			/** MAKE SURE TO SEND 403 ALWAYS **/
			return Response()->json([
		    	'status' => false,
	        ], 403);
		}

		$validator = Validator::make(
            $request->toArray(),
            [
            	'id' => 'required',
            	'wishlist' => 'required',
            ]
        );

        
		if(!$validator->fails())
		{
			$product = Products::select(['id'])->where('id', $request->get('id'))->first();
			if($product)
			{
				$userId = ApiAuth::getLoginId();

				if($request->get('wishlist'))
				{
					$wishlist = UsersWishlist::create($request->get('id'), $userId);
					if($wishlist)
					{
						return Response()->json([
					    	'status' => true,
					    	'message' => 'Product added to your wishlist.',
				            'wishlist' => $request->get('wishlist')
				        ]);
					}
					else
					{
						return Response()->json([
					    	'status' => true,
					    	'message' => 'Product could not mark as wishlist.',
				            'wishlist' => $request->get('wishlist')
				        ]);
					}
				}
				else
				{
					$wishlist = UsersWishlist::remove($request->get('id'), $userId);
					if($wishlist)
					{
						return Response()->json([
					    	'status' => true,
					    	'message' => 'Product removed from your wishlist.',
				            'wishlist' => $request->get('wishlist')
				        ]);
					}
					else
					{
						return Response()->json([
					    	'status' => true,
					    	'message' => 'Product could not remove from wishlist.',
				            'wishlist' => $request->get('wishlist')
				        ]);
					}	
				}
				
			}
			else
			{
				return Response()->json([
			    	'status' => false,
		            'message' => 'Product is missing.'
		        ], 400);
			}
		}
		else
		{
			return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		}
	}

	/**
	* To Upload File
	* @param Request $request
	*/
    function uploadFile(Request $request)
    {
    	$validator = Validator::make(
            $request->toArray(),
            [
                'file' => 'required',
            ]
        );

    	if(!$validator->fails())
	    {
	    	if($request->file('file')->isValid())
	    	{
	    		$file = null;

	    		$file = FileSystem::uploadImage(
    				$request->file('file'),
    				'products'
    			);

    			if($file)
    			{
    				$names = explode('/', $file);
					return Response()->json([
				    	'status' => 'success',
				    	'message' => 'File uploaded successfully.',
				    	'url' => url($file),
				    	'name' => end($names),
				    	'path' => $file
				    ]);
    				
    			}
    			else
    			{
    				return Response()->json([
				    	'status' => 'error',
				    	'message' => 'File could not be upload.'
				    ]);		
    			}
	    	}
	    	else
	    	{
	    		return Response()->json([
		    	'status' => 'error',
		    	'message' => 'File could not be uploaded.'
		    ]);	
	    	}
	   	}
	    else
	    {
	    	return Response()->json([
		    	'status' => 'error',
		    	'message' => 'File could not be uploaded due to missing parameters.'
		    ]);	
	    }
    }

    /**
	* To Remove File
	* @param Request $request
	*/
    function removeFile(Request $request)
    {
    	$data = $request->toArray();

    	$validator = Validator::make(
            $request->toArray(),
            [
                'file' => 'required',
            ]
        );

    	if(!$validator->fails())
	    {
	    	if(FileSystem::deleteFile($data['file']))
    		{
    			return Response()->json([
			    	'status' => 'success',
			    	'message' => 'File removed successfully.'
			    ]);
    		}
    		else
    		{
	    		return Response()->json([
			    	'status' => 'error',
			    	'message' => 'File could not be removed.'
			    ]);
	    	}
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => 'error',
		    	'message' => 'File parameter is missing.'
		    ]);
	    }
    }
}