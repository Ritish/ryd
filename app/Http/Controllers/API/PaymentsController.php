<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Models\API\ApiAuth;
use App\Libraries\General;
use App\Libraries\Stripe;
use App\Models\Admin\Trips;
use App\Models\Admin\Transactions;
use App\Models\Admin\TripsRating;
use App\Models\Admin\TripActivies;
use App\Models\API\Trips as APITrips;

class PaymentsController extends AppController
{

	function charge(Request $request)
	{
		$allowed = ['trip_id', 'rating', 'tip'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'rating' => 'required',
	            	'tip' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$userId = ApiAuth::getLoginId();

	        	$trip = Trips::where(['id' => $request->get('trip_id'), 'user_id' => $userId])->first();

	        	if($trip && $trip->status == 5 && !$trip->customer_paid)
	        	{
	        		if($trip->customer_total == null || $trip->customer_total <= 0)
	        		{
	        			$trip = APITrips::updatePrices($trip->id);
	        		}

	        		$amount = $trip->customer_total;
	        		$tip = $request->get('tip');
	        		$totalAmount = round($amount + $tip, 2);
	        		$message = "Trip Id: {$trip->id}";
	        		
	        		if($charge = Stripe::chargeCard($trip->customer, $totalAmount, $message))
	        		{
	        			$trip->customer_paid = $charge->paid && $charge->status == 'succeeded' ? 1 : 0;
	        			$trip->tip = $tip;
	        			$trip->save();

	        			$symbol = Settings::get('currency_symbol');
	        			Transactions::create([
	        				'trip_id' => $trip->id,
	        				'driver_id' => $trip->driver_id,
	        				'driver_name' => $trip->driver->first_name ? ($trip->driver->first_name . ' ' . $trip->driver->last_name) : $trip->driver->phonenumber,
	        				'user_id' => $trip->user_id,
	        				'customer_name' => $trip->customer->first_name ? ($trip->customer->first_name . ' ' . $trip->customer->last_name) : $trip->customer->phonenumber,
	        				'service_id' => $trip->service_id,
	        				'service_name' => $trip->service->title,
	        				'vehicle_id' => $trip->vehicle_id,
	        				'vehicle_number' => $trip->vehicle->number,
	        				'currency' => Settings::get('currency_code'),
	        				'currency_symbol' => $symbol,
	        				'amount' => $totalAmount,
	        				'stripe_charge_id' => $charge->id,
	        				'status' => $charge->status,
	        				'type' => 'payment'
	        			]);

	        			TripsRating::create([
	        				'trip_id' => $trip->id,
	        				'from_id' => $trip->user_id,
	        				'to_id' => $trip->driver_id,
	        				'rating' => $request->get('rating')
	        			]);

	        			TripActivies::create([
		        				'trip_id' => $trip->id,
		        				'status' => 'customer_charged',
		        				'message' => 'Customer make payment of '.$symbol . $totalAmount.'.<br><small>Trip Id: '.$trip->id.', Customer: '.($trip->customer->first_name ? ($trip->customer->first_name . ' ' . $trip->customer->last_name) : $trip->customer->phonenumber).'</small>'
		        			]);

	        			return Response()->json([
					    	'status' => true,
					    	'totalAmount' => $totalAmount,
					    	'paid' => $charge->paid ? 1 : 0,
					    	'payment_status' => $charge->status
				    	]);	
	        		}
	        		else
	        		{
	        			return Response()->json([
					    	'status' => false,
					    	'message' => 'Payment could not be completed. Please try again.'
					    ], 400);   			
	        		}
		        }
		        else
		        {
		     		return Response()->json([
				    	'status' => false,
				    	'message' => 'Something went wrong. Please contact us.'
				    ], 400);   	
		        }
	        }
	        else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function transactions(Request $request)
	{
		$allowed = ['page', 'type'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'page' => 'required',
	            	'type' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$userId = ApiAuth::getLoginId();

	        	$where = ['trips.status' => 5,'trips.customer_paid' => 1];
	        	if($request->get('type') == 'driver')
	        		$where['trips.driver_id'] = $userId;
	        	else
	        		$where['trips.user_id'] = $userId;
	        	$trips = APITrips::getListing($request, $where);
	        	foreach($trips as $k => $t)
	        	{
	        		$trip = Trips::find($t->id);
	        		if($request->get('type') == 'customer')
	        			$charges = Trips::getCustomerTripCharges($trip);
	        		else
	        			$charges = Trips::getDriverTripAmount($trip);
	        		$t->charges = $charges;
	        		$t->status = Trips::getSatusLabel($t->status);
	        		$t->status = strip_tags($t->status);
	        		$trips[$k] = $t;
	        	}

	        	return Response()->json([
			    	'status' => true,
			    	'trips' => $trips->items()
		    	]);
			}
			else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}
}