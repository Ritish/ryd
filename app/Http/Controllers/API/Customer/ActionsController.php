<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Libraries\DateTime;
use App\Models\API\Users;
use App\Models\API\Vehicles;
use App\Models\API\Trips;
use App\Models\API\TripDestinations;
use App\Models\Admin\TripWaitings;
use App\Models\Admin\Messages;
use App\Models\Admin\VehiclesBrands;
use App\Models\Admin\VehiclesModels;
use App\Models\Admin\VehiclesColors;
use App\Models\Admin\VehicleServices;
use App\Models\Admin\TripActivies;
use App\Models\Admin\TripsRating;
use App\Models\Admin\Transactions;
use App\Models\API\ApiAuth;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;
use App\Libraries\Stripe;
use Illuminate\Support\Arr;

class ActionsController extends AppController
{
	function getServices(Request $request)
	{
		$allowed = ['id', 'drivers', 'pickup', 'destinations'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'id' => 'required',
	            	'drivers' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$destinations = $request->get('destinations');
	        	// foreach ($destinations as $k => $value) {
	        	// 	$value = explode('|', $value);
	        	// 	$destinations[$k] = [
	        	// 		'address' => $value[0],
	        	// 		'lat' => $value[1],
	        	// 		'lng' => $value[2],
	        	// 	];
	        	// }
	        	$user = Users::select(['id', 'gender', 'female_driver_only', 'spanish_speaking_only'])
	        			->where('driver', 0)
	        			->where('status', 1)
	        			->where('id', $request->get('id'))
	        			->first();
	        			
	        	if($user)
	        	{
	        		$drivers = $request->get('drivers');
	        		$ids = Arr::pluck($drivers, 'id');
	        		$services = VehicleServices::select([
	        				'services.id',
	        				'services.title',
	        				'services.image',
	        				'vehicles.user_id as driver_id',
	        				'vehicles.seats as seats'
	        			])
	        			->leftJoin('services', 'services.id', '=', 'vehicle_services.service_id')
	        			->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_services.vehicle_id')
	        			->leftJoin('users', 'users.id', '=', 'vehicles.user_id')
	        			->leftJoin('user_languages', function($join) {
					      	$join->on('user_languages.user_id', '=', 'users.id')
					      		->where('language_id', 2);
					    })
					    ->whereIn('vehicles.user_id', $ids)
	        			->where('users.driver', 1)
	        			->where('vehicles.status', 1)
	        			->where('users.status', 1);
	        		if($user->female_driver_only)
	        		{
	        			$services->whereRaw('(users.gender = "female" or users.female_driver_only = 1)');
	        		}

	        		if($user->spanish_speaking_only)
	        		{
	        			$services->whereRaw('(user_languages.language_id = 2 or users.spanish_speaking_only = 1)');
	        		}
	        		
	        		$final = [];
	        		
	        		$services = $services->get();
	        		if($services)
	        		{
		        		$ryds = [];
		        		foreach($services as $k => $v)
		        		{
		        			$v->distance = $drivers[$v->driver_id]['distance'];
		        			$v->minutes = $v->distance * Settings::get('distance_time_multiplier');
		        			$v->amount = VehicleServices::getAmount($drivers[$v->driver_id], $request->get('pickup'), $destinations, $v->id);
		        			$ryds[$v->distance][] = $v;
		        		}

		        		if($ryds)
		        		{
			        		ksort($ryds);
			        		
			        		foreach($ryds as $k => $v)
			        		{
			        			$final = array_merge($final, $v);
			        		}
			        	}
		        	}

	        		return Response()->json([
				    	'status' => true,
				    	'services' => $final
				    ], 200);
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function getRyds(Request $request)
	{
		$allowed = ['id', 'drivers', 'service_id', 'address', 'lat', 'lng', 'destinations'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'id' => 'required',
	            	'service_id' => 'required',
	            	'drivers' => 'required',
	            	'destinations' => 'required',
	            	'address' => 'required',
	            	'lat' => 'required',
	            	'lng' => 'required',
	            ]
	        );

	        if(!$validator->fails() && is_array($request->get('destinations')) && is_array($request->get('drivers')))
	        {
	        	$user = Users::select(['id', 'first_name', 'last_name', 'image', 'gender', 'female_driver_only', 'spanish_speaking_only'])
	        			->where('driver', 0)
	        			->where('status', 1)
	        			->where('id', $request->get('id'))
	        			->first();

	        	if($user)
	        	{
	        		$drivers = $request->get('drivers');
	        		$ids = Arr::pluck($drivers, 'id');
	        		$dbDrivers = VehicleServices::select([
	        				'vehicles.id',
	        				'vehicles.number',
	        				'vehicles.brand_id',
	        				'vehicles.model_id',
	        				'vehicles.color_id',
	        				'vehicles.image as vehicle_image',
	        				'vehicles.seats',
	        				'services.id as service_id',
	        				'services.title as service_title',
	        				'users.id as driver_id', 
	        				'first_name as driver_first_name', 
	        				'last_name as driver_last_name', 
	        				'users.image as driver_image'
	        			])
	        			->leftJoin('services', 'services.id', '=', 'vehicle_services.service_id')
	        			->leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_services.vehicle_id')
	        			->leftJoin('users', 'users.id', '=', 'vehicles.user_id')
	        			->leftJoin('user_languages', function($join) {
					      	$join->on('user_languages.user_id', '=', 'users.id')
					      		->where('language_id', 2);
					    })
					    ->where('services.id', $request->get('service_id'))
					    ->whereIn('vehicles.user_id', $ids)
	        			->where('users.driver', 1)
	        			->where('vehicles.status', 1)
	        			->where('users.status', 1);
	        		if($user->female_driver_only)
	        		{
	        			$dbDrivers->whereRaw('(users.gender = "female" or users.female_driver_only = 1)');
	        		}

	        		if($user->spanish_speaking_only)
	        		{
	        			$dbDrivers->whereRaw('(user_languages.language_id = 2 or users.spanish_speaking_only = 1)');	
	        		}

	        		$services = $dbDrivers->get();

	        		$driver = null;
	        		if($services)
	        		{
		        		$ryds = [];
		        		foreach($services as $k => $v)
		        		{
		        			$v->driver_image = FileSystem::getAllSizeImages($v->driver_image);
		        			$ryds[$v->distance][] = $v;
		        		}

		        		if($ryds)
		        		{
			        		ksort($ryds);
			        		$driver = current(current($ryds));
			        	}
		        	}

		        	if($driver)
		        	{
		        		$destinations = $request->get('destinations');
		        		$endTrip = end($destinations);
		        		$driver->distance = $drivers[$driver->driver_id]['distance'];
	        			$driver->minutes = $driver->distance * Settings::get('distance_time_multiplier');
	        			$driver->minutes = $driver->minutes * 60;
	        			$driver->amount = VehicleServices::getAmount(
	        					$drivers[$driver->driver_id], 
	        					[
	        						'address' => $request->get('address'),
	        						'lat' => $request->get('lat'),
	        						'lng' => $request->get('lng')
	        					], 
	        					$destinations,
	        					$driver->service_id
	        				);
	        			$driver->vehicle_id = $driver->id;
	        			$driver->vehicle_brand = VehiclesBrands::getTitle($driver->brand_id);
	        			$driver->vehicle_model = VehiclesModels::getTitle($driver->model_id);
	        			$driver->vehicle_color = VehiclesColors::getTitle($driver->color_id);
	        			$driver->vehicle_image = FileSystem::getAllSizeImages($driver->vehicle_image);
	        			$driver->vehicle_image = $driver->vehicle_image ? current($driver->vehicle_image) : null;

		        		$tripId = Trips::createTrip([
		        			'destinations' => $destinations,
		        			'user_id' => $user->id,
		        			'driver_id' => $driver->driver_id,
		        			'service_id' => $driver->service_id,
		        			'vehicle_id' => $driver->id,
		        			'from_address' => $drivers[$driver->driver_id]['address'],
		        			'from_address_lat' => $drivers[$driver->driver_id]['lat'],
		        			'from_address_lng' => $drivers[$driver->driver_id]['lng'],
		        			'pickup_address' => $request->get('address'),
		        			'pickup_address_lat' => $request->get('lat'),
		        			'pickup_address_lng' => $request->get('lng'),
		        			'to_address' => $endTrip['address'],
		        			'to_address_lat' => $endTrip['lat'],
		        			'to_address_lng' => $endTrip['lng'],
		        			'start_trip_time' => date('Y-m-d H:i:s'),
		        			'before_pickup_duration' => $driver->minutes,
		        			'before_pickup_distance' => $driver->distance,
		        			'status' => 0
		        		]);

		        		if($tripId)
		        		{
		        			$driver->trip_id = $tripId;
		        			$driver->from_address = $drivers[$driver->driver_id]['address'];
		        			$driver->from_address_lat = $drivers[$driver->driver_id]['lat'];
		        			$driver->from_address_lng = $drivers[$driver->driver_id]['lng'];
		        			$driver->pickup_address = $request->get('address');
		        			$driver->pickup_address_lat = $request->get('lat');
		        			$driver->pickup_address_lng = $request->get('lng');
		        			$driver->destinations = TripDestinations::where('trip_id', $tripId)
		        						->orderBy('id', 'asc')
		        						->get();
		        			/** Ratings for both **/
		        			$driver->rating = (int) TripsRating::where('to_id', $driver->driver_id)->avg('rating');
		        			$user->rating = "5";
		        			/** Ratings for both **/
		        			$driver->customer = $user;

		        			TripActivies::create([
		        				'trip_id' => $tripId,
		        				'status' => 'trip_initiated',
		        				'message' => 'Ride initiated. Driver and vehicle assigned.<br><small>'.$driver->driver_first_name . ' ' . $driver->driver_last_name . ' ('.$driver->number.' - '.$driver->vehicle_color.', '.$driver->vehicle_brand.', '.$driver->vehicle_model.')</small>'
		        			]);
		        		}
		        		
		        	}

	        		return Response()->json([
				    	'status' => true,
				    	'driver' => $driver
				    ], 200);
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function acceptRyd(Request $request)
	{
		$allowed = ['trip_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$trip->status = 1;
	        		if($trip->save())
	        		{
	        			TripActivies::create([
	        				'trip_id' => $trip->id,
	        				'status' => 'driver_accepted',
	        				'message' => 'Driver accepted the ride. Driving to <b>'.$trip->from_address.'</b>'
	        			]);

		        		return Response()->json([
					    	'status' => true,
					    	'trip_id' => $trip->id
					    ], 200);
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not allowed.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function declineRyd(Request $request)
	{
		$allowed = ['trip_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$trip->status = 8;

	        		if($trip->save())
	        		{
	        			TripActivies::create([
	        				'trip_id' => $trip->id,
	        				'status' => 'driver_decline',
	        				'message' => 'Driver declined or skip the ride.'
	        			]);

		        		return Response()->json([
					    	'status' => true,
					    	'trip_id' => $trip->id
					    ], 200);
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not allowed.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function reachedAtPickup(Request $request)
	{
		$allowed = ['trip_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$trip->status = 2;

	        		if($trip->save())
	        		{
	        			TripActivies::create([
	        				'trip_id' => $trip->id,
	        				'status' => 'driver_reached_at_pickup',
	        				'message' => 'Driver reached at pickup point.<b>Location: '.$trip->pickup_address.'</b>'
	        			]);

		        		return Response()->json([
					    	'status' => true,
					    	'trip_id' => $trip->id
					    ], 200);
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not allowed.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}
	

	function startWaiting(Request $request)
	{
		$allowed = ['trip_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$waiting = TripWaitings::select(['id'])
	        				->whereNull('end_time')
	        				->where('trip_id', $request->get('trip_id'))
	        				->first();
	        		if(!$waiting)
	        		{
		        		$waiting = TripWaitings::create([
		        			'trip_id' => $request->get('trip_id'),
		        			'start_time' => time(),
		        			'end_time' => null
		        		]);
		        	}

		        	if($waiting)
		        	{
			        	$trip->status = 4;
			        	if($trip->save())
			        	{
			        		$response = TripWaitings::get($waiting->id);
				        	$response->driver_id = $trip->driver_id;
				        	$response->customer_id = $trip->user_id;

				        	TripActivies::create([
	        					'trip_id' => $trip->id,
		        				'status' => 'waiting_start',
		        				'message' => 'Waiting timer start.'
		        			]);

			        		return Response()->json([
						    	'status' => true,
						    	'waiting' => $response
						    ], 200);
			        	}
			        	else
			        	{
			        		return Response()->json([
						    	'status' => false,
						    	'message' => 'Trip status could not be updated.'
						    ], 400);
			        	}
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Waiting entity is missing.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function endWaiting(Request $request)
	{
		$allowed = ['trip_id', 'waiting_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'waiting_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$waiting = TripWaitings::select(['id'])
	        				->whereNull('end_time')
	        				->where('trip_id', $request->get('trip_id'))
	        				->where('id', $request->get('waiting_id'))
	        				->first();
	        		if($waiting)
	        		{
	        			$waiting->end_time = $request->get('seconds') ? strtotime($waiting->start_time . " +{$request->get('seconds')} seconds") : time();
	        			if($waiting->save())
	        			{
			        		$response = TripWaitings::get($waiting->id);
				        	$response->driver_id = $trip->driver_id;
				        	$response->customer_id = $trip->user_id;

				        	$trip->status = 3;
				        	if($trip->save())
				        	{
				        		TripActivies::create([
		        					'trip_id' => $trip->id,
			        				'status' => 'waiting_start',
			        				'message' => 'Waiting timer ended.<br><small>Time: '.DateTime::secondsToTime($response->end_time - $response->start_time).'</small>'
			        			]);

				        		return Response()->json([
							    	'status' => true,
							    	'waiting' => $response
							    ], 200);
				        	}
				        	else
				        	{
				        		return Response()->json([
					    			'status' => false,
							    	'message' => 'Status could not be update.'
							    ], 400);		
				        	}
			        	}
			        	else
			        	{
			        		return Response()->json([
				    			'status' => false,
						    	'message' => 'Waiting could not be ended.'
						    ], 400);
			        	}
		        	}
		        	else
		        	{
	        			return Response()->json([
			    			'status' => false,
					    	'message' => 'Waiting record is missing.'
					    ], 400);
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Trip is missing.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	} 


	function startDestination(Request $request)
	{
		$allowed = ['trip_id', 'destination_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'destination_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$destination = TripDestinations::whereNull('end_time')
	        				->where('completed', 0)
	        				->where('trip_id', $request->get('trip_id'))
	        				->where('id', $request->get('destination_id'))
	        				->whereNull('start_time')
	        				->orderBy('id', 'asc')
	        				->first();
	        		if($destination)
	        		{
		        		$destination->start_time = date('Y-m-d H:i:s');
		        		if($destination->save())
		        		{

		        			//Start Trip Flag
		        			$allDestinations = TripDestinations::whereNull('end_time')
		        				->where('completed', 0)
		        				->where('trip_id', $request->get('trip_id'))
		        				->orderBy('id', 'asc')
		        				->pluck('id')
		        				->toArray();

		        			if($allDestinations && in_array($request->get('destination_id'), $allDestinations) && $request->get('destination_id') == $allDestinations[0])
		        			{
		        				$trip->status = 3;
		        				$trip->pickup_trip_time = date('Y-m-d H:i:s');
		        				$trip->save();
		        			}
		        			//Start Trip Flag

		        			$destination->driver_id = $trip->driver_id;
				        	$destination->customer_id = $trip->user_id;

				        	TripActivies::create([
	        					'trip_id' => $trip->id,
		        				'status' => 'destination_start',
		        				'message' => 'Destination start. Driving to <b>' . $destination->address . '</b>'
		        			]);

		        			return Response()->json([
						    	'status' => true,
						    	'destination' => $destination
						    ], 200);
		        		}
		        		else
		        		{
		        			return Response()->json([
						    	'status' => false,
						    	'message' => 'Trip destinaiton could not be updated.'
						    ], 400);
		        		}		        		
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Trip destinaiton already started or unavailable.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function endDestination(Request $request)
	{
		$allowed = ['trip_id', 'destination_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'destination_id' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		$destination = TripDestinations::whereNull('end_time')
	        				->where('completed', 0)
	        				->where('trip_id', $request->get('trip_id'))
	        				->where('id', $request->get('destination_id'))
	        				->whereNull('end_time')
	        				->orderBy('id', 'asc')
	        				->first();
	        		if($destination)
	        		{
		        		$destination->end_time = date('Y-m-d H:i:s');
		        		$destination->completed = 1;
		        		if($destination->save())
		        		{
				        	$destination->driver_id = $trip->driver_id;
				        	$destination->customer_id = $trip->user_id;

				        	$trip->completed_destinations = $trip->completed_destinations + 1;
				        	if($trip->save())
				        	{

					        	TripActivies::create([
		        					'trip_id' => $trip->id,
			        				'status' => 'destination_end',
			        				'message' => 'Destination end. Reached at <b>' . $destination->address . '</b>'
			        			]);

			        			return Response()->json([
							    	'status' => true,
							    	'destination' => $destination
							    ], 200);
			        		}
			        		else
			        		{
			        			return Response()->json([
							    	'status' => false,
							    	'message' => 'Trip status could not be updated.'
							    ], 400);		
			        		}
		        		}
		        		else
		        		{
		        			return Response()->json([
						    	'status' => false,
						    	'message' => 'Trip destinaiton could not be updated.'
						    ], 400);
		        		}		        		
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Trip destinaiton already ended or unavailable.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function endTrip(Request $request)
	{
		$allowed = ['trip_id', 'location', 'lat', 'lng'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'location' => 'required',
	            	'lat' => 'required',
	            	'lng' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip && $trip->status < 5)
	        	{
	        		$destination = TripDestinations::whereNull('end_time')
						->where('completed', 0)
						->where('trip_id', $trip->id)
						->whereNotNull('start_time')
						->orderBy('id', 'asc')
						->first();
					if($destination)
					{
						$destination->address = $request->get('location');
						$destination->lat = $request->get('lat');
						$destination->lng = $request->get('lng');
						$destination->completed = 1;
						$destination->end_time = date('Y-m-d H:i:s');
						$destination->save();

						$trip->completed_destinations = $trip->completed_destinations + 1;
						$trip->to_address = $request->get('location');
						$trip->to_address_lat = $request->get('lat');
						$trip->to_address_lat = $request->get('lng');
					}

	        		$trip->status = 5;
    				$trip->end_trip_time = date('Y-m-d H:i:s');
    				if($trip->save())
    				{
    					//Calculate and update the pricing and charges.
    					$trip = Trips::updatePrices($trip->id);

    					TripActivies::create([
        					'trip_id' => $trip->id,
	        				'status' => 'trip_completed',
	        				'message' => 'Ride completed.'
	        			]);

		        		return Response()->json([
					    	'status' => true,
					    	'trip' => $trip
					    ], 200);
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Not allowed.'
					    ], 400);
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function cancelRyd(Request $request)
	{
		$allowed = ['trip_id', 'cancel_by', 'lat', 'lng', 'location'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'cancel_by' => 'required',
	            	'lat' => 'required',
	            	'lng' => 'required',
	            	'location' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	$status = null;
	        	if($trip && !in_array($trip->status, [6,7,8]))
	        	{
	        		$trip->cancel_location = $request->get('location');
	        		$trip->cancel_lat = $request->get('lat');
	        		$trip->cancel_lng = $request->get('lng');
	        		/** Process if customer cancel and deduct pay **/

        			$cancelMinsLimit = Settings::get('cancelation_minutes');
        			if($request->get('cancel_by') == 'customer'&& time() > strtotime($trip->created . ' +' . $cancelMinsLimit . ' minutes'))
        			{
        				$amount = Settings::get('cancelation_charges_customer');
        				$driverPecentage = Settings::get('cancelation_percent_driver');

        				$trip->customer_total = $amount;
        				$trip->driver_total = $amount * $driverPecentage;
        				$status = 7;
        				
        				$message = "Trip canceled. \n Trip Id: {$trip->id}";
        				$charge = Stripe::chargeCard($trip->customer, $amount, $message);
        				if($charge)
        				{
	        				$symbol = Settings::get('currency_symbol');
		        			Transactions::create([
		        				'trip_id' => $trip->id,
		        				'driver_id' => $trip->driver_id,
		        				'driver_name' => $trip->driver->first_name ? ($trip->driver->first_name . ' ' . $trip->driver->last_name) : $trip->driver->phonenumber,
		        				'user_id' => $trip->user_id,
		        				'customer_name' => $trip->customer->first_name ? ($trip->customer->first_name . ' ' . $trip->customer->last_name) : $trip->customer->phonenumber,
		        				'service_id' => $trip->service_id,
		        				'service_name' => $trip->service->title,
		        				'vehicle_id' => $trip->vehicle_id,
		        				'vehicle_number' => $trip->vehicle->number,
		        				'currency' => Settings::get('currency_code'),
		        				'currency_symbol' => $symbol,
		        				'amount' => $amount,
		        				'stripe_charge_id' => $charge->id,
		        				'status' => $charge->status,
		        				'type' => 'payment'
		        			]);
		        		}
		        		/** Process if customer cancel and deduct pay **/
        			}
        			elseif($trip->status == 2 || $trip->status == 4)
        			{
        				/** Process if driver cancel**/
        				$freeTime = Settings::get('free_wait_time') * 60;
        				$waiting = TripWaitings::select(['id','start_time'])
        						->where('trip_id', $trip->id)
        						->whereNull('end_time')
        						->first();
        				
        				if($waiting)
        				{
        					$waitTimeNow = (time()-$waiting->start_time);
        					if($waitTimeNow > $freeTime)
        					{
	        					$message = Messages::where('from_id', $trip->driver_id)
	        						->where('trip_id', $trip->id)
	        						->count();
	        					if($message > 0)
	        					{
	        						$status = 8;
	        					}
	        					else
	        					{
	        						return Response()->json([
								    	'status' => false,
								    	'message' => 'Please drop the message to customer before cancel the trip'
								    ], 400);
	        					}
	        				}
	        				else
	        				{
	        					return Response()->json([
							    	'status' => false,
							    	'message' => 'Please wait atleast for ' . DateTime::secondsToTime($freeTime) . ' and message him before cancel'
							    ], 400);	
	        				}

        				}
        				else
        				{
        					return Response()->json([
						    	'status' => false,
						    	'message' => 'Please wait atleast for ' . DateTime::secondsToTime($freeTime) . ' and message him before cancel'
						    ], 400);		
        				}
        				/** Process if driver cancel**/
        			}
        			else
        			{
        				return Response()->json([
					    	'status' => false,
					    	'message' => 'Trip cannot be cancel at this moment.'
					    ], 400);
        			}
        			
        			
	        		
	        		if($status)
	        		{
	        			$trip->status = $status;
	        			if($trip->save())
	        			{
		        			TripActivies::create([
	        					'trip_id' => $trip->id,
		        				'status' => 'trip_canceled',
		        				'message' => 'Ride canceled by '.$request->get('cancel_by').'.',
		        			]);

		        			TripActivies::create([
	        					'trip_id' => $trip->id,
		        				'status' => 'trip_cancel_location',
		        				'message' => $request->get('location') . ' | ' . $request->get('lat') . ' | ' . $request->get('lng')
		        			]);

			        		return Response()->json([
						    	'status' => true,
						    	'trip_id' => $trip->id
						    ], 200);
			        	}
			        	else
			        	{
			        		return Response()->json([
						    	'status' => false,
						    	'message' => 'Something went wrong.'
						    ], 400);		
			        	}
		        	}
		        	else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Something went wrong.'
					    ], 400);		
		        	}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Not allowed.'
				    ], 400);
	        	}
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function message(Request $request)
	{
		$allowed = ['trip_id', 'from_id', 'to_id', 'message'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'trip_id' => 'required',
	            	'from_id' => 'required',
	            	'to_id' => 'required',
	            	'message' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$trip = Trips::find($request->get('trip_id'));
	        	if($trip)
	        	{
	        		if($message = Messages::create($request->toArray()))
	        		{
					    return Response()->json([
					    	'status' => true,
					    	'message' => $message
					    ], 200);
	        		}
	        		else
	        		{
	        			return Response()->json([
					    	'status' => false,
					    	'message' => 'Message could not be send.',
					    ], 400);
	        		}
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Trip could not be found.',
				    ], 400);
	        	}
	        }
	        else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}
}