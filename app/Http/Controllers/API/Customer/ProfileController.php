<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Models\API\Users;
use App\Models\API\UsersCards;
use App\Models\API\ApiAuth;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;
use App\Libraries\Stripe;

class ProfileController extends AppController
{

	function createdCard(Request $request)
	{
		$userId = ApiAuth::getLoginId();

		$allowed = ['card_name', 'card_number', 'expiry_date', 'billing_zip', 'cvv'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'card_name' => 'required',
	            	'card_number' => 'required|max:16',	
	            	'expiry_date' => 'required',
	            	'billing_zip' => 'required',
	            	'cvv' => 'required|max:5'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$user = Users::find($userId);
	        	if($user)
	        	{
	        		if(!$user->stripe_customer_id)
	        		{
	        			$response = Stripe::createCustomer($user);
	        			if($response)
	        			{
	        				$user->stripe_customer_id = $response;
	        			}
	        			else
	        			{
	        				return Response()->json([
						    	'status' => false,
						    	'message' => 'Stripe could not be linked with your account. Please contact us.',
						    ], 400);
	        			}
	        		}

		        	if($cardType = General::validateCard($request->get('card_number')))
		        	{
		        		$expire = explode('/', $request->get('expiry_date'));
		        		$token = Stripe::createCardToken([
		        			'card_number' => $request->get('card_number'),
		        			'exp_month' => $expire[0],
		        			'exp_year' => $expire[1],
		        			'cvv' => $request->get('cvv')
		        		]);

		        		if($token)
		        		{
				        	$card = new UsersCards();
				        	$card->user_id = $userId;
				        	$card->card_name = $request->get('card_name');
				        	$card->card_number = $request->get('card_number');
				        	$card->card_type = $cardType;
				        	$card->expiry_date = $request->get('expiry_date');
				        	$card->billing_zip = $request->get('billing_zip');
				 			$card->stripe_customer_id = $user->stripe_customer_id;
				        	$card->created = date('Y-m-d H:i:s');
				        	$card->modified = date('Y-m-d H:i:s');

				        	if($card->save())
				        	{
				        		if($card->stripe_customer_id)
				        		{
				        			$response = Stripe::createCard($card, $token);
				        			if($response)
				        			{
				        				return Response()->json([
									    	'status' => true,
									    	'message' => 'Card linked successfully.',
									    ]);
				        			}
				        			else
				        			{
				        				$card->delete();
				        				return Response()->json([
									    	'status' => false,
									    	'message' => 'Card could not linked with your account. Make sure to enter correct details or contact us.',
									    ], 400);
				        			}
				        		}
				        		else
				        		{
				        			$card->delete();
				        			return Response()->json([
								    	'status' => false,
								    	'message' => 'Stripe is not linked with your account. Please contact us.',
								    ], 400);
				        		}
				        	}
				        	else
				        	{
				        		return Response()->json([
							    	'status' => false,
							    	'message' => 'Unable to save card. Please try again.'
							    ], 400);
				        	}
				        }
				        else
				        {
				        	return Response()->json([
							    	'status' => false,
							    	'message' => 'Invalid card. Please enter a valid card details.'
							    ], 400);
				        }
			        }
			        else
			        {
			        	return Response()->json([
						    	'status' => false,
						    	'message' => 'Your card number is invalid. Please try again.'
						    ], 400);
			        }
			    }
			    else
		        {
		        	return Response()->json([
					    	'status' => false,
					    	'message' => 'User is missing.'
					    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function unlinkCard(Request $request)
	{
		$userId = ApiAuth::getLoginId();

		$allowed = ['card_id'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'card_id' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	
				$card = UsersCards::select(['id', 'stripe_customer_id', 'stripe_card_id'])->where('id', $card->id)->where('user_id', $userId)->first();
				if($card)
				{
        			$response = Stripe::deleteCard($card, $token);
        			if($response)
        			{
        				return Response()->json([
					    	'status' => true,
					    	'message' => 'Card linked successfully.',
					    ]);
        			}
        			else
        			{
        				$card->delete();
        				return Response()->json([
					    	'status' => false,
					    	'message' => 'Card is not liked with your account. Make sure to enter correct details or contact us.',
					    ], 400);
        			}
			    }
			    else
		        {
		        	return Response()->json([
					    	'status' => false,
					    	'message' => 'User is missing.'
					    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function preferences(Request $request)
	{
		$userId = ApiAuth::getLoginId();

    	if($user = Users::find($userId))
    	{
    		if($request->get('female_driver_only') !== null && $request->get('female_driver_only') !== "")
    		{
        		$user->female_driver_only = $request->get('female_driver_only');
        	}

        	if($request->get('spanish_speaking_only') !== null && $request->get('spanish_speaking_only') !== "")
        	{
        		$user->spanish_speaking_only = $request->get('spanish_speaking_only');
        	}

        	if($request->get('wheel_chair') !== null && $request->get('wheel_chair') !== '')
        	{
        		$user->wheel_chair = $request->get('wheel_chair');
        	}
        	
        	if($request->get('contact_by_email') !== null && $request->get('contact_by_email') !== "")
        	{
        		$user->contact_by_email = $request->get('contact_by_email');
        	}

        	if(!empty($request->get('languages')) )
    		{
    			Users::handleLanguages($user->id, $request->get('languages'));
    		}
    		else
    		{
    			Users::handleLanguages($user->id, []);
    		}

        	if($user->save())
        	{
        		$user = ApiAuth::getLoginUser();
				return Response()->json([
			    	'status' => true,
			    	'message' => 'Preferences updated.',
			    	'user' => $user
			    ]);
        	}
        	else
        	{
        		return Response()->json([
			    	'status' => false,
			    	'message' => 'Could not update preferences.'
			    ], 400);
        	}
        }
        else
        {
        	return Response()->json([
			    	'status' => false,
			    	'message' => 'Your card number is invalid. Please try again.'
			    ], 400);
        }
	    
	}
}