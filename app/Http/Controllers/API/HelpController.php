<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Models\Admin\HelpCategories;
use App\Models\Admin\Help;
use App\Models\API\ApiAuth;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;

class HelpController extends AppController
{
	function categories(Request $request)
	{
		$userId = ApiAuth::getLoginId();
        
        $categories = HelpCategories::orderBy('id', 'desc')->get();
		return Response()->json([
	    	'status' => true,
	    	'categories' => $categories
	    ]);
	}

	function request(Request $request)
	{
		$userId = ApiAuth::getLoginId();

		$allowed = ['email', 'message'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'help_category_id' => 'required',
	            	'email' => 'required',
	            	'message' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$help = Help::create([
	        		'user_id' => $userId,
	        		'help_category_id' => $request->get('help_category_id'),
	        		'email' => $request->get('email'),
	        		'issue' => $request->get('message'),
	        	]);

	        	if($help)
	        	{
	        		$user = ApiAuth::getLoginUser();
					return Response()->json([
				    	'status' => true,
				    	'message' => 'Help request submitted. We will revert on you soon on your email.',
				    ]);
		        }
		        else
		        {
		        	return Response()->json([
					    	'status' => false,
					    	'message' => 'Request could not be submitted. Please try again.'
					    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}
}