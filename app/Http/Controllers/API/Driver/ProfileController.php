<?php

namespace App\Http\Controllers\API\Driver;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Admin\Settings;
use App\Libraries\General;
use App\Models\API\Users;
use App\Models\API\Addresses;
use App\Models\API\UsersDocuments;
use App\Models\Admin\States;
use App\Models\API\ApiAuth;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;


class ProfileController extends AppController
{

	function saveMailingAddress(Request $request)
	{
		$userId = ApiAuth::getLoginId();

		$allowed = ['mailing_address', 'city', 'state', 'zip'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'mailing_address' => 'required',
	            	'city' => 'required',
	            	'state' => 'required',
	            	'zip' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	Addresses::where('user_id', $userId)->delete();
	        	$address = new Addresses();
	        	$address->user_id = $userId;
	        	$address->mailing_address = $request->get('mailing_address');
	        	$address->address_line = $request->get('address_line') ? $request->get('address_line') : null;
	        	$address->city = $request->get('city');
	        	$address->state = $request->get('state');
	        	$address->zip = $request->get('zip');
	        	$address->created = date('Y-m-d H:i:s');
	        	$address->modified = date('Y-m-d H:i:s');

	        	if($address->save())
	        	{
    				return Response()->json([
				    	'status' => true,
				    	'message' => 'Mailing address saved successfully.',
				    	'address' => $address,
				    	'user' => ApiAuth::getLoginUser()
				    ]);
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Unable to save address. Please try again.'
				    ], 400);
	        	}
		    }	
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function uploadDocuments(Request $request, $slug)
	{
		$userId = ApiAuth::getLoginId();

		$allowed = ['title', 'file'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'title' => 'required',
	            	'file' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	if($filePath = FileSystem::uploadBase64Image($request->get('file'), 'documents'))
	        	{
	        		$exist = UsersDocuments::where('slug', $slug)->where('user_id', $userId)->first();
	        		if(empty($exist))
	        		{
			        	$document = new UsersDocuments();
			        	$document->user_id = $userId;
			        	$document->title = $request->get('title');
			        	$document->slug = $slug;
			        	$document->file = $filePath;
			        	$document->created = date('Y-m-d H:i:s');
			        	$document->modified = date('Y-m-d H:i:s');
			        	if($document->save())
			        	{
	    					return Response()->json([
						    	'status' => true,
						    	'message' => $document->title . ' saved.',
						    	'document' => $document,
					    		'user' => ApiAuth::getLoginUser()
						    ]);
			        	}
			        	else
			        	{
			        		return Response()->json([
						    	'status' => false,
						    	'message' => 'Unable to save document. Please try again.'
						    ], 400);
			        	}
			        }
			        else
		        	{
		        		return Response()->json([
					    	'status' => false,
					    	'message' => 'Document is already uploaded.'
					    ], 400);
		        	}
		        }
		        else
		        {
		        	return Response()->json([
					    	'status' => false,
					    	'message' => 'Unable to upload document. Please try again.'
					    ], 400);
		        }
		    }
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}
}