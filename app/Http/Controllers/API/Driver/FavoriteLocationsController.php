<?php

namespace App\Http\Controllers\API\Driver;

use App\Http\Controllers\API\AppController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\API\Users;
use App\Models\API\FavoriteLocations;
use App\Models\API\ApiAuth;
use Illuminate\Support\Facades\Hash;

class FavoriteLocationsController extends AppController
{

	function listing(Request $request)
	{
		$userId = ApiAuth::getLoginId();
		$locations = FavoriteLocations::where('user_id', $userId)
			->get();

		return Response()->json([
		    	'status' => true,
		    	'locations' => $locations
		    ]);
	}

	function create(Request $request)
	{
		$allowed = ['location', 'lat', 'lng'];
		if($request->has($allowed))
		{
			$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'location' => 'required',
	            	'lat' => 'required',
	            	'lng' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$userId = ApiAuth::getLoginId();
	        	$location = FavoriteLocations::create([
	        		'user_id' => $userId,
	            	'location' => $request->get('location'),
	            	'lat' => $request->get('lat'),
	            	'lng' => $request->get('lng')
	            ]);
	        	
	        	if($location)
	        	{
    				return Response()->json([
				    	'status' => true,
				    	'message' => 'Location saved successfully.',
				    	'location' => $location
				    ]);
	        	}
	        	else
	        	{
	        		return Response()->json([
				    	'status' => false,
				    	'message' => 'Unable to save location. Please try again.'
				    ], 400);
	        	}
		    }	
		    else
		    {
		    	return Response()->json([
			    	'status' => false,
			    	'message' => current( current( $validator->errors()->getMessages() ) )
			    ], 400);
		    }
	    }
	    else
	    {
	    	return Response()->json([
		    	'status' => false,
		    	'message' => 'Some of inputs are invalid in request.',
		    ], 400);
	    }
	}

	function delete(Request $request)
	{
		$userId = ApiAuth::getLoginId();
		$location = FavoriteLocations::where('user_id', $userId)
			->where('id', $request->get('id'))
			->first();
		if($location)
		{
			if(FavoriteLocations::remove($request->get('id')))
			{
				return Response()->json([
				    	'status' => true
				    ]);
			}
			else
			{
				return Response()->json([
				    	'status' => false
				    ], 400);
			}
		}
		else
		{
			return Response()->json([
			    	'status' => false
			    ], 400);
		}
	}
}