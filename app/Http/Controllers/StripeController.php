<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\Users;
use App\Models\Admin\Trips;
use App\Models\Admin\TripActivies;
use App\Models\Admin\Transactions;
use App\Models\API\UsersTokens;
use App\Libraries\Stripe;

class StripeController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

	function accountLinking(Request $request)
	{
		if($request->get('token') || $request->get('user_id'))
		{
			if($request->get('token'))
			{
				$id = UsersTokens::getUserId($request->get('token'));
			}
			else
			{
				$id = convert_uudecode(base64_decode($request->get('user_id')));
			}

		    $user = Users::find($id);
		    if($user && $user->is_verified)
		    {
		    	if($user->stripe_account_id)
		    	{
		    		$accountId = $user->stripe_account_id;
		    	}
		    	else
		    	{
		    		$accountId = Stripe::createAccount($user);
		    	}

		    	if($accountId)
		    	{
    				$link = Stripe::createAccountLink($user->id, $accountId);
    				return redirect($link);
		    	}
		    	else
		    	{
		    		die('<h2>Error: Something went wrong while connecting to Stripe. Please try again or contact us.</h2>');
		    	}
		    }
		    else
		    {
		 		die('<h2>Error: Invalid token or url. Please contact adminstrator.</h2>');
		    }
		}
		else
		{
			die('<h2>Error: Something went wrong while connecting to Stripe. Please try again or contact us.</h2>');
		}
	}

	function linkedAccountSuccess()
	{
		if(isset($_GET['user_id']) && $_GET['user_id'])
		{
			$id = convert_uudecode(base64_decode($_GET['user_id']));
			$user = Users::find($id);
			if($user)
			{
				$user->stripe_connect_by_customer = 1;

				if($user->stripe_account_id)
				{
					$bankAc = Stripe::getAccount($user->stripe_account_id);
					if(isset($bankAc->requirements->currently_due) && empty($bankAc->requirements->currently_due))
					{
						$user->stripe_account_verified = 1;
					}
				}
				$user->save();
			}

            die('Sucess:Bank account linked.');
		}
		else
		{
			die('<h2>Error: Something went wrong while connecting to Stripe. Please try again or contact us.</h2>');
		}
	}

	function payouts(Request $request, $token)
	{
		$payoutToken = Settings::get('payout_cron_token');
		if($token == $payoutToken)
		{
			$symbol = Settings::get('currency_symbol');
			$trips = Trips::select(['id'])
				->where('status', 5)
				->where('customer_paid', 1)
				->where('driver_transfered', 0)
				->where('created', '<', date('Y-m-d 00:00:01'))
				->get();
			foreach($trips as $t)
			{
				$payment = Transactions::select(['id', 'stripe_charge_id'])
					->where('trip_id', $t->id)
					->where('status', 'succeeded')
					->where('amount', '>', 0)
					->where('type', 'payment')
					->first();

				if($payment)
				{
					$trip = Trips::find($t->id);
					if($transferId = Stripe::createTransfer($trip->driver, $payment->stripe_charge_id, $trip->driver_total))
					{
						$trip->driver_transfered = 1;
						$trip->save();

						Transactions::create([
	        				'trip_id' => $trip->id,
	        				'driver_id' => $trip->driver_id,
	        				'driver_name' => $trip->driver->first_name ? ($trip->driver->first_name . ' ' . $trip->driver->last_name) : $trip->driver->phonenumber,
	        				'user_id' => $trip->user_id,
	        				'customer_name' => $trip->customer->first_name ? ($trip->customer->first_name . ' ' . $trip->customer->last_name) : $trip->customer->phonenumber,
	        				'service_id' => $trip->service_id,
	        				'service_name' => $trip->service->title,
	        				'vehicle_id' => $trip->vehicle_id,
	        				'vehicle_number' => $trip->vehicle->number,
	        				'currency' => Settings::get('currency_code'),
	        				'currency_symbol' => $symbol,
	        				'amount' => $trip->driver_total,
	        				'stripe_charge_id' => $transferId,
	        				'status' => 'succeeded',
	        				'type' => 'transfer'
	        			]);

	        			TripActivies::create([
	        				'trip_id' => $trip->id,
	        				'status' => 'customer_charged',
	        				'message' => 'Driver recived payment of '.$symbol . $trip->driver_total.'.<br><small>Trip Id: '.$trip->id.', Driver: '.($trip->driver->first_name ? ($trip->driver->first_name . ' ' . $trip->driver->last_name) : $trip->driver->phonenumber).'</small>'
	        			]);
					}
				}
			}
		}
		else
		{
			die('<h2>Error: 404</h2>');
		}
	}
}