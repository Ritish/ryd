<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Settings;
use App\Models\Admin\Trips;
use App\Models\Admin\Transactions;
use App\Libraries\General;
use Illuminate\Support\Facades\DB;

class DashboardController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	$counts = [
            'pending' => Trips::select(['id'])->where('status', '5')->where('driver_transfered', 0)->get()->count(),
            'transfered' => Transactions::select(['id'])->where('type', 'transfer')->get()->count(),
            'pendingAmount' => Trips::where('status', '5')->where('driver_transfered', 0)->sum('driver_total'),
            'tranferedAmount' => Transactions::select(['id'])->where('type', 'transfer')->sum('amount'),
            'totalSale' => Trips::where('status', '5')->sum('customer_total'),
            'active' => Trips::select(['id'])->where('status', '>', '0')->where('status', '<', '5')->get()->count(),
            'completed' => Trips::select(['id'])->where('status', '5')->get()->count(),
            'canceled' => Trips::select(['id'])->where('status', '>', '5')->get()->count(),
            'completedAmount' => Trips::where('status', '5')->sum('customer_total'),
            'canceledAmount' => Trips::where('status', '>', '5')->sum('customer_total'),
            'totalDistance' => Trips::where('status', '>', '0')->sum(DB::raw('(trips.distance + before_pickup_distance)')),
        ];

    	return view("admin/dashboard/dashboard", ['counts' => $counts]);
    }
}
