<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\Permissions;
use App\Models\Admin\AdminAuth;
use App\Models\Admin\UsersDocuments;
use App\Models\Admin\States;
use App\Models\Admin\Addresses;
use App\Models\Admin\VehiclesBrands;
use App\Models\Admin\VehiclesColors;
use App\Models\Admin\VehiclesModels;
use App\Models\Admin\VehiclesYears;
use App\Models\Admin\UserServices;
use App\Models\Admin\Services;
use App\Libraries\General;
use App\Libraries\FileSystem;
use App\Libraries\Stripe;
use App\Models\Admin\Users;
use App\Models\Admin\Vehicles;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UsersController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('users', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(concat(first_name, "", last_name) LIKE ? or email LIKE ? or phonenumber LIKE ? or tag_number LIKE ?)'] = [$search, $search, $search, $search];
    	}

    	if($request->get('last_login'))
    	{
    		$lastLogin = $request->get('last_login');
    		if(isset($lastLogin[0]) && !empty($lastLogin[0]))
    			$where['last_login >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($lastLogin[0]))
    			];
    		if(isset($lastLogin[1]) && !empty($lastLogin[1]))
    			$where['last_login <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($lastLogin[1]))
    			];
    	}

    	if($request->get('created_on'))
    	{
    		$created = $request->get('created_on');
    		if(isset($created[0]) && !empty($created[0]))
    			$where['created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($created[0]))
    			];
    		if(isset($created[1]) && !empty($created[1]))
    			$where['created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($created[1]))
    			];
    	}

    	if($request->get('role'))
    	{
    		switch ($request->get('role')) {
    			case 'customer':
    				$where['driver'] = 0;
    			break;
    			case 'driver':
    				$where['driver'] = 1;
    			break;
    		}
    	}
    	else
    	{
    		$where['driver'] = 0;
    	}

    	if($request->get('status'))
    	{
    		switch ($request->get('status')) {
    			case 'active':
    				$where['status'] = 1;
    			break;
    			case 'non_active':
    				$where['status'] = 0;
    			break;
    		}
    		
    	}

    	$verified = null;
    	if($request->get('verified'))
    	{
    		switch ($request->get('verified')) {
    			case 'yes':
    				$verified = 1;
    			break;
    			case 'no':
    				$verified = 0;
    			break;
    		}
    		
    	}

    	$listing = Users::getListing($request, $where, $verified);

    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/users/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
			$counts = [
				'all' => Users::select([
						'id'
					])
					->where('driver', 1)
					->get()->count(),
				'verified' => Users::select([
						'id',
						DB::raw('( select count(users_documents.id) from users_documents where (users_documents.slug = "background-check" or users_documents.slug = "vehicle-registration" or users_documents.slug = "insurance-proof" or users_documents.slug =  "driving-license-back" or users_documents.slug = "driving-license-front") and verified = 1 and expiry_date >= current_date() and users_documents.user_id = users.id) as all_verified')
					])
					->where('driver', 1)
					->having('all_verified', '>=', 5)
					->get()->count(),
				'pending' => Users::select([
						'id',
						DB::raw('( select count(id) from users_documents where (users_documents.slug = "background-check" or users_documents.slug = "vehicle-registration" or users_documents.slug = "insurance-proof" or users_documents.slug =  "driving-license-back" or users_documents.slug = "driving-license-front") and verified = 1 and expiry_date >= current_date()  and users_documents.user_id = users.id) as all_verified')
					])
					->where('driver', 1)
					->having('all_verified', '<', 5)
					->get()->count(),
			];
	    	return view(
	    		"admin/users/index", 
	    		[
	    			'listing' => $listing,
	    			'counts' => $counts
	    		]
	    	);
	    }
    }

    function add(Request $request)
    {
    	die('disabled');
    	if(!Permissions::hasPermission('users', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

    		/** Set random password in case send email button is on **/
    		$sendPasswordEmail = isset($data['send_password_email']) && $data['send_password_email'] > 0 ? true : false;
        	if($sendPasswordEmail)
        	{
        		$data['password'] = Str::random(20);
        	}
        	/** Set random password in case send email button is on **/

    		$validator = Validator::make(
	            $data,
	            [
	                'first_name' => 'required',
	                'last_name' => 'required',
	                'email' => [
	                	'required',
	                	'email',
	                	Rule::unique('users')->whereNull('deleted_at')
	                ],
	                'phonenumber' => 'unique:users,phonenumber',
	                'send_password_email' => 'required',
	                'password' => [
	                	'required',
					    'min:8',
	                ]
	            ]
	        );


	        if(!$validator->fails())
	        {
	        	$password = $data['password'];
	        	unset($data['_token']);
	        	unset($data['send_password_email']);
	        	$data['verified_at'] = date('Y-m-d H:i:s');
	        	$user = Users::create($data);
	        	if($user)
	        	{
	        		//Send Email
	        		if($sendPasswordEmail)
	        		{
	        			$codes = [
	        				'{first_name}' => $user->first_name,
	        				'{last_name}' => $user->last_name,
	        				'{email}' => $user->email,
	        				'{password}' => $password
	        			];

	        			General::sendTemplateEmail(
	        				$user->email, 
	        				'customer-admin-registration', 
	        				$codes
	        			);
	        		}

	        		$request->session()->flash('success', 'Customer created successfully.');
	        		return redirect()->route('admin.users');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Customer could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/users/add", []);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('users', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$user = Users::get($id);
    	if($user)
    	{
	    	if($request->isMethod('post'))
	    	{
	    		$data = $request->toArray();

	    		/** Set random password in case send email button is on **/
	    		$sendPasswordEmail = isset($data['send_password_email']) && $data['send_password_email'] > 0 ? true : false;
	        	if($sendPasswordEmail)
	        	{
	        		$data['password'] = $password = Str::random(20);
	        	}
	        	elseif(!isset($data['password']) || !$data['password'])
	        	{
	        		unset($data['password']);
	        	}

	        	$address = isset($data['address']) && !empty($data['address']) ? $data['address'] : null;
	        	unset($data['address']);

	        	$services = isset($data['services']) && !empty($data['services']) ? $data['services'] : null;
	        	unset($data['services']);

	        	$languages = isset($data['languages']) && !empty($data['languages']) ? $data['languages'] : null;
	        	unset($data['languages']);

	        	/** Set random password in case send email button is on **/
	    		$validator = Validator::make(
		            $data,
		            [
		                'first_name' => 'required',
		                'last_name' => 'required',
		                'country_code' => 'required',
	            		'phonenumber' => 'required|max:11',
	            		'email' => [
		                	'required',
		                	'email',
		                	Rule::unique('users')->ignore($user->id)->whereNull('deleted_at')
		                ],
		                'password' => [
		                	'nullable',
						    'min:8',
		                ],
		            ]
		        );

		        if(!$validator->fails())
		        {
		        	unset($data['_token']);
		        	unset($data['send_password_email']);

		        	$countryCode = str_replace('+', '', $request->get('country_code'));
		        	$exist = Users::select(['id', 'last_login'])
		        		->where('phonenumber', 'LIKE', $request->get('phonenumber'))
		        		->where('country_code', $countryCode)
		        		->where('id', '!=', $user->id)
		        		->first();
		        	if(!$exist)
	        		{
			        	$user = Users::modify($id, $data);
			        	if($user)
			        	{
			        		//Send Email
			        		if($sendPasswordEmail)
			        		{
			        			$codes = [
			        				'{first_name}' => $user->first_name,
			        				'{last_name}' => $user->last_name,
			        				'{phonenumber}' => $user->country_code . $user->phonenumber,
			        				'{email}' => $user->email,
			        				'{password}' => $password
			        			];

			        			General::sendTemplateEmail(
			        				$user->email, 
			        				'customer-admin-registration',
			        				$codes
			        			);
			        		}

			        		if(!empty($address))
			        		{
			        			Addresses::where('user_id', $user->id)->delete();
			        			$address['user_id'] = $user->id;
			        			Addresses::create($address);
			        		}

			        		if(!empty($services))
			        		{
			        			Users::handleServices($user->id, $services);
			        		}

			        		if(!empty($languages))
			        		{
			        			Users::handleLanguages($user->id, $languages);
			        		}

			        		$request->session()->flash('success', 'User updated successfully.');
			        		return redirect($request->headers->get('referer'));
			        	}
			        	else
			        	{
			        		$request->session()->flash('error', 'User could not be save. Please try again.');
				    		return redirect()->back()->withErrors($validator)->withInput();
			        	}
			        }
			        else
			        {
			        	$request->session()->flash('error', 'Phone number is already registered with us.');
				    	return redirect()->back()->withErrors($validator)->withInput();
			        	
			        }
			    }
			    else
			    {
			    	$request->session()->flash('error', 'Please provide valid inputs.');
			    	return redirect()->back()->withErrors($validator)->withInput();
			    }
			}

			$states = States::select(['id', 'name', 'state_code'])->orderBy('state_code', 'asc')->get();
			$vehicle = Vehicles::where('user_id', $user->id)->first();
			$brands = VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get();
		    $colors = VehiclesColors::select(['id', 'title'])->orderBy('title', 'asc')->get();
		    $years = VehiclesYears::select(['id', 'year'])->orderBy('year', 'asc')->get();
		    $services = Services::orderBy('id', 'asc')->get();
		    $models = [];
		    if($vehicle && $vehicle->brand_id)
		    {
		    	$models = VehiclesModels::select(['id', 'title'])->where('brand_id', $vehicle->brand_id)->orderBy('title', 'asc')->get();
		    }
			return view("admin/users/edit", [
    			'user' => $user,
    			'states' => $states,
    			'vehicle' => $vehicle,
    			'models' => $models,
    			'brands' => $brands,
    			'colors' => $colors,
    			'years' => $years,
    			'services' => $services
    		]);
		}
		else
		{
			abort(404);
		}
    }

    function verify(Request $request, $id)
    {
    	if(!Permissions::hasPermission('users', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$user = Users::find($id);
    	if($user)
    	{
    		if($user->first_name && $user->last_name && $user->email)
    		{
	    		$user->is_verified = 1;
	    		$user->verified_at = date('Y-m-d H:i:s');
	    		if($user->save())
	    		{
	    			/*if($user->driver && !$user->stripe_customer_id)
	    			{
		    			$response = Stripe::createCustomer($user);
		    			if($response)
		    			{
		    				$request->session()->flash('success', 'Profile verified successfully.');
		        			return redirect($request->headers->get('referer'));
		    			}
		    			else
		    			{
		    				$request->session()->flash('error', 'Profile verified successfully. But Could not connected to stripe.');
		        			return redirect($request->headers->get('referer'));
		    			}
		    		}*/
	        		
	        		$request->session()->flash('success', 'Profile verified successfully.');
	        		return redirect($request->headers->get('referer'));
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Profile could not be verified. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
	        }
	        else
	        {
	        	$request->session()->flash('error', 'Profile enter the driver\'s information to verify account and stripe linking.');
		    	return redirect()->back()->withInput();
	        }
		}
		else
		{
			abort(404);
		}
    }

    function view(Request $request, $id)
    {
    	if(!Permissions::hasPermission('users', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$user = Users::get($id);
    	if($user)
    	{
			return view("admin/users/view", [
    			'user' => $user
    		]);
		}
		else
		{
			abort(404);
		}
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('users', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$user = Users::find($id);
    	if($user->delete())
    	{
    		$request->session()->flash('success', 'User deleted successfully.');
    		return redirect($request->headers->get('referer'));
    	}
    	else
    	{
    		$request->session()->flash('error', 'User could not be delete.');
    		return redirect()->route('admin.users');
    	}
    }

    function bulkActions(Request $request, $action)
    {
    	if( ($action != 'delete' && !Permissions::hasPermission('users', 'update')) || ($action == 'delete' && !Permissions::hasPermission('users', 'delete')) ) 
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$ids = $request->get('ids');
    	if(is_array($ids) && !empty($ids))
    	{
    		switch ($action) {
    			case 'active':
    				Users::modifyAll($ids, [
    					'status' => 1
    				]);
    				$message = count($ids) . ' records has been activate.';
    			break;
    			case 'inactive':
    				Users::modifyAll($ids, [
    					'status' => 0
    				]);
    				$message = count($ids) . ' records has been inactivate.';
    			break;
    			case 'verified':
    				Users::modifyAll($ids, [
    					'is_verified' => 1,
    					'verified_at' => date('Y-m-d H:i:s')
    				]);
    				$message = count($ids) . ' records has been verified.';
    			break;
    			case 'delete':
    				Users::removeAll($ids);
    				$message = count($ids) . ' records has been deleted.';
    			break;
    			case 'documents_verification':
    				UsersDocuments::modifyAll($ids, [
    					'verified' => 1
    				]);
    				$message = count($ids) . ' documents has been verified.';
    			break;
    		}

    		$request->session()->flash('success', $message);

    		return Response()->json([
    			'status' => 'success',
	            'message' => $message,
	        ], 200);		
    	}
    	else
    	{
    		return Response()->json([
    			'status' => 'error',
	            'message' => 'Please select atleast one record.',
	        ], 200);
    	}
    }

    function updatePicture(Request $request)
    {
    	if(!Permissions::hasPermission('users', 'update'))
    	{
    		return Response()->json([
			    	'status' => 'error',
			    	'message' => 'Permission denied.'
			    ]);
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

            $validator = Validator::make(
	            $request->toArray(),
	            [
	                'file' => 'mimes:jpg,jpeg,png,gif',
	            ]
	        );

	        if(!$validator->fails())
	        {
				$id = $data['id'];

		    	$user = Users::find($id);
		    	if($user)
		    	{
		    		$oldImage = $user->image;
		    		if($request->file('file')->isValid())
		    		{
		    			$file = FileSystem::uploadImage(
		    				$request->file('file'),
		    				'users'
		    			);

		    			if($file)
		    			{
		    				$user->image = $file;
		    				if($user->save())
		    				{
		    					$originalName = FileSystem::getFileNameFromPath($file);
		    					
		    					FileSystem::resizeImage($file, 'M-' . $originalName, "350*350");
		    					FileSystem::resizeImage($file, 'S-' . $originalName, "100*100");
		    					$picture = $user->getResizeImagesAttribute()['medium'];

		    					if($oldImage)
		    					{
		    						FileSystem::deleteFile($oldImage);
		    					}

		    					
		    					return Response()->json([
							    	'status' => 'success',
							    	'message' => 'Picture uploaded successfully.',
							    	'picture' => url($picture)
							    ]);		
		    				}
		    				else
		    				{
		    					FileSystem::deleteFile($file);
		    					return Response()->json([
							    	'status' => 'error',
							    	'message' => 'Picture could not be uploaded.'
							    ]);	
		    				}
		    				
		    			}
		    			else
		    			{
		    				return Response()->json([
						    	'status' => 'error',
						    	'message' => 'Picture could not be upload.'
						    ]);		
		    			}
		    		}
					else
					{
						return Response()->json([
					    	'status' => 'error',
					    	'message' => 'Picture could not be uploaded. Please try again.'
					    ]);
					}
				}
				else
				{
					return Response()->json([
				    	'status' => 'error',
				    	'message' => 'Admin member is missing.'
				    ]);
				}
			}
		}
		else
		{
			return Response()->json([
			    	'status' => 'error',
			    	'message' => 'Admin member is missing.'
			    ]);
		}
    }

    function documents(Request $request, $id)
    {
    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

    		$validator = Validator::make(
	            $data,
	            [
	                'title' => 'required',
	                'file' => 'required'
	            ]
	        );


	        if(!$validator->fails())
	        {
	        	$exist = UsersDocuments::select(['id'])->where('user_id', $id)->where('slug', 'LIKE', $request->get('title'))->first();
	        	if(!$exist)
	        	{
		        	$data = [
		        		'title' => UsersDocuments::getDocumentName($request->get('title')),
		        		'slug' => $request->get('title'),
		        		'file' => $request->get('file'),
		        		'user_id' => $id
		        	];

		        	$user = UsersDocuments::create($data);
		        	if($user)
		        	{
		        		$request->session()->flash('success', 'Document uploaded successfully.');
		        		return redirect($request->headers->get('referer'));
		        	}
		        	else
		        	{
		        		$request->session()->flash('error', 'Document could not be save. Please try again.');
			    		return redirect()->back()->withErrors($validator)->withInput();
		        	}
		        }		        else
		        {
		        	$request->session()->flash('error', UsersDocuments::getDocumentName($request->get('title')) . ' is already uploaded.');
			    	return redirect()->back()->withErrors($validator)->withInput();
		        }
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/users/add", []);
    }

    function documentsDelete(Request $request, $id)
    {
    	$document = UsersDocuments::find($id);

    	if($document->file)
    	{
    		if(file_exists(public_path($document->file)))
    		{
    			unlink(public_path($document->file));
    		}
    		$document->file = null;
    		$document->save();

    		$request->session()->flash('success', 'Document deleted successfully.');
    		return redirect($request->headers->get('referer'));
    	}
    	else
    	{
    		$request->session()->flash('error', 'No uploaded document found.');
    		return redirect($request->headers->get('referer'));
    	}
    }

    function editDocument(Request $request, $id)
    {
    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

    		$validator = Validator::make(
	            $data,
	            [
	                'expiry_date' => 'required',
	                'verified' => 'required'
	            ]
	        );


	        if(!$validator->fails())
	        {
	        	$document = UsersDocuments::find($id);
	        	if(strpos($document->slug, 'driving-license') > -1)
	        	{
	        		$updated = UsersDocuments::where('slug', 'LIKE', 'driving-license%')
	        			->where('user_id', $document->user_id)
	        			->update([
	        				'expiry_date' => $request->get('expiry_date'),
	        				'verified' => $request->get('verified')
	        			]);
	        		if($updated)
	        		{
	        			$request->session()->flash('success', 'Document updated successfully.');
		        		return redirect($request->headers->get('referer'));
	        		}
	        		else
	        		{
	        			$request->session()->flash('error', 'Document could not be save. Please try again.');
			    		return redirect()->back()->withErrors($validator)->withInput();
	        		}
	        	}
	        	else if($document)
	        	{
		        	$document->expiry_date = $request->get('expiry_date');
		        	$document->verified = $request->get('verified');
		        	if($document->save())
		        	{
		        		$request->session()->flash('success', 'Document updated successfully.');
		        		return redirect($request->headers->get('referer'));
		        	}
		        	else
		        	{
		        		$request->session()->flash('error', 'Document could not be save. Please try again.');
			    		return redirect()->back()->withErrors($validator)->withInput();
		        	}
		        }
		        else
		        {
		        	$request->session()->flash('error', "Document is missing.");
			    	return redirect()->back()->withErrors($validator)->withInput();
		        }
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
    }

    function vehicles(Request $request)
    {
    	if(!Permissions::hasPermission('users', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->back();
    	}

    	$data = $request->toArray();
    	unset($data['_token']);

		$validator = Validator::make(
	        $request->toArray(),
	        [
	        	'user_id' => 'required',
	            'number' => 'required',
	            'brand_id' => 'required',
	            'model_id' => 'required',
	            'color_id' => 'required',
	            'year' => 'required',
	            'trim' => 'required'
	        ]
	    );

	    if(!$validator->fails())
	    {
	    	$exist = Vehicles::select(['id', 'image'])->where('user_id', $data['user_id'])->first();
	    	if($exist)
	    	{
	    		if(isset($data['image']) && $data['image'])
        		{
        			$data['image'] = json_decode($data['image'], true);
        			$exist->image = $exist->image ? json_decode($exist->image) : [];
	        		$data['image'] = array_merge($exist->image, $data['image']);
	        		$data['image'] = json_encode($data['image']);
	        	}
	        	else
	        	{
	        		unset($data['image']);
	        	}

	    		$vehicle = Vehicles::modify($exist->id, $data);
	    	}
	    	else
	    	{
	    		$vehicle = Vehicles::create($data);
	    	}

	    	if($vehicle)
	    	{
	    		$services = UserServices::where('user_id', $vehicle->user_id)->pluck('service_id')->toArray();
	    		Vehicles::handleServices($vehicle->id, $services ? $services : []);

	    		$request->session()->flash('success', 'Vehicle updated successfully.');
	    		return redirect()->back();
	    	}
	    	else
	    	{
	    		$request->session()->flash('error', 'Vehicle could not be save. Please try again.');
	    		return redirect()->back()->withErrors($validator)->withInput();
	    	}
	    }
	    else
	    {
	    	$request->session()->flash('error', 'Please provide valid inputs.');
	    	return redirect()->back()->withErrors($validator)->withInput();
	    }
    }
}