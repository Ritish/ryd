<?php

namespace App\Http\Controllers\Admin\Help;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\HelpCategories;
use App\Models\Admin\Admins;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class HelpCategoriesController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('help_categories', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(help_categories.title LIKE ?)'] = [$search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['help_categories.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['help_categories.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'help_categories.created_by IN ('.$admins.')';
    	}

    	$listing = HelpCategories::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/help/categories/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/
	    	return view(
	    		"admin/help/categories/index", 
	    		[
	    			'listing' => $listing,
                    'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = HelpCategories::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
        if($adminIds)
        {
            $admins = Admins::getAll(
                [
                    'admins.id',
                    'admins.first_name',
                    'admins.last_name',
                    'admins.status',
                ],
                [
                    'admins.id in ('.implode(',', $adminIds).')'
                ],
                'concat(admins.first_name, admins.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('help_categories', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'title' => [
	            		'required',
	            		Rule::unique('help_categories')
	            	]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$brand = HelpCategories::create($data);
	        	if($brand)
	        	{
	        		if($request->ajax())
	        		{
	        			return Response()->json([
	        					'status' => 'success',
	        					'message' => 'Help category created successfully.',
	        					'category' => $brand
	        				]);
	        		}
	        		else
	        		{
		        		$request->session()->flash('success', 'Help category created successfully.');
		        		return redirect()->route('admin.helpCategories');
		        	}
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Help category could not be save. Please try again.');
			    	return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    elseif($request->ajax())
    		{
    			return Response()->json([
    					'status' => 'error',
    					'message' => current(current($validator->messages()))
    				]);
    		}
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/help/categories/add", []);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('help_categories', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$brand = HelpCategories::find($id);

    	if(!$brand)
        {
        	$request->session()->flash('error', 'Brand is missing.');
    		return redirect()->route('admin.helpCategories');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'title' => [
                        'required',
                        Rule::unique('help_categories')->ignore($id)
                    ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$brand = HelpCategories::modify($id, $data);
	        	if($brand)
	        	{
	        		$request->session()->flash('success', 'Brand updated successfully.');
	        		return redirect()->route('admin.helpCategories');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Brand could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}

	    return view("admin/help/categories/edit", [
	    			'brand' => $brand
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('help_categories', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = HelpCategories::find($id);
    	if($vehicle->delete())
    	{
    		$request->session()->flash('success', 'Category deleted successfully.');
    		return redirect()->route('admin.helpCategories');
    	}
    	else
    	{
    		$request->session()->flash('error', 'Category could not be delete.');
    		return redirect()->route('admin.helpCategories');
    	}
    }	

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('help_categories', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    HelpCategories::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }
}
