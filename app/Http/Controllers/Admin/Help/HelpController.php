<?php

namespace App\Http\Controllers\Admin\Help;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\Help;
use App\Models\Admin\Admins;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class HelpController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('help', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(help.title LIKE ?)'] = [$search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['help.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['help.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	$listing = Help::getListing($request, $where);

    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/help/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/
	    	return view(
	    		"admin/help/index", 
	    		[
	    			'listing' => $listing,
                    'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = Help::distinct()->whereNotNull('user_id')->pluck('user_id')->toArray();
        if($adminIds)
        {
            $admins = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $adminIds).')'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('help', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = Help::find($id);
    	if($vehicle->delete())
    	{
    		$request->session()->flash('success', 'Request deleted successfully.');
    		return redirect()->route('admin.help');
    	}
    	else
    	{
    		$request->session()->flash('error', 'Request could not be delete.');
    		return redirect()->route('admin.help');
    	}
    }	

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('help', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    Help::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }

    function markAsResolved(Request $request, $id, $status)
    {
        if(!Permissions::hasPermission('help', 'update'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $help = Help::find($id);
        $help->status = $status;
        if($help->save())
        {
            $request->session()->flash('success', $help->status ? 'Request has been resolved.' : 'Request marked as open.');
            return redirect()->route('admin.help');
        }
        else
        {
            $request->session()->flash('error', 'Request could not be updated.');
            return redirect()->route('admin.help');
        }
    }
}
