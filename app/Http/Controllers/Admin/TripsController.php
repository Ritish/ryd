<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Messages;
use App\Models\Admin\Permissions;
use App\Models\Admin\Trips;
use App\Models\Admin\TripDestinations;
use App\Models\Admin\Admins;
use App\Models\Admin\Services;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Libraries\DateTime;
use App\Http\Controllers\Admin\AppController;
use Illuminate\Support\Facades\DB;
use App\Libraries\Excel;

class TripsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('trips', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(trips.from_address LIKE ?  or trips.to_address LIKE ? or trips.pickup_address LIKE ? or concat(users.first_name, " ", users.last_name) LIKE ? or users.first_name LIKE ? or users.last_name LIKE ?  or users.phonenumber LIKE ?  or users.email LIKE ? or concat(driver.first_name, " ", driver.last_name) LIKE ? or driver.first_name LIKE ? or driver.last_name LIKE ? or driver.phonenumber LIKE ? or driver.email LIKE ? or vehicles.number  LIKE ? or services.title LIKE ?)'] = [$search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search];
    	}
        
    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['trips.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['trips.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

        if($request->get('customer_price'))
        {
            $range = $request->get('customer_price');
            if(is_array($range))
                $where['trips.customer_total >= ? and trips.customer_total <= ?'] = [$range[0], $range[1]];
            else
                $where['trips.customer_total <= ?'] = [$range];
        }

        if($request->get('driver_price'))
        {
            $range = $request->get('driver_price');
            if(is_array($range))
                $where['trips.driver_total >= ? and trips.driver_total <= ?'] = [$range[0], $range[1]];
            else
                $where['trips.driver_total <= ?'] = [$range];
        }

        if($request->get('distance'))
        {
            $range = $request->get('distance');
            if(is_array($range))
                $where['((trips.before_pickup_distance + trips.distance) >= ? and (trips.before_pickup_distance + trips.distance) <= ?)'] = [$range[0], $range[1]];
            else
                $where['(trips.before_pickup_distance + trips.distance) <= ?'] = [$range];
        }

        if($request->get('users'))
        {
            $users = $request->get('users');
            $users = $users ? implode(',', $users) : 0;
            $where[] = 'trips.user_id IN ('.$users.')';
        }

        if($request->get('drivers'))
        {
            $driver = $request->get('drivers');
            $driver = $driver ? implode(',', $driver) : 0;
            $where[] = 'trips.driver_id IN ('.$driver.')';
        }

        if($request->get('services'))
        {
            $service = $request->get('services');
            $service = $service ? implode(',', $service) : 0;
            $where[] = 'trips.service_id IN ('.$service.')';
        }

        if($request->get('status'))
        {
            $status = $request->get('status');
            $status = $status ? implode(',', $status) : 0;
            $where[] = 'trips.status IN ('.$status.')';
        }

    	$listing = Trips::getListing($request, $where);

    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/trips/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request);
            $filters['listing'] = $listing;
            /** Filter Data **/

            $filters['counts'] = [
                'active' => Trips::select(['id'])->where('status', '>', '0')->where('status', '<', '5')->get()->count(),
                'completed' => Trips::select(['id'])->where('status', '5')->get()->count(),
                'canceled' => Trips::select(['id'])->where('status', '>', '5')->get()->count(),
                'completedAmount' => Trips::where('status', '5')->sum('customer_total'),
                'canceledAmount' => Trips::where('status', '>', '5')->sum('customer_total'),
                'totalDistance' => Trips::where('status', '>', '0')->sum(DB::raw('(trips.distance + before_pickup_distance)')),
            ];

	    	return view(
	    		"admin/trips/index",
	    		$filters
	    	);
	    }
    }

    function filters(Request $request)
    {
        $users = [];
        $userIds = Trips::distinct()->whereNotNull('user_id')->pluck('user_id')->toArray();
        if($userIds)
        {
            $users = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $userIds).')',
                    'driver = 0'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $drivers = [];
        $driverIds = Trips::distinct()->whereNotNull('driver_id')->pluck('driver_id')->toArray();
        if($driverIds)
        {
            $drivers = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $driverIds).')',
                    'driver = 1'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $services = Services::orderBy('title', 'asc')->get();
        $maxCustomerTotal = Trips::max('customer_total');
        $minCustomerTotal = Trips::min('customer_total');
        $maxDriverTotal = Trips::max('driver_total');
        $minDriverTotal = Trips::min('driver_total');
        $maxDistance = Trips::max(DB::raw('(trips.before_pickup_distance + trips.distance)'));
        $minDistance = Trips::max(DB::raw('(trips.before_pickup_distance + trips.distance)'));
        $statuses = Trips::getStatus();
        return [
            'users' => $users,
            'statuses'=> $statuses,
            'drivers' => $drivers,
            'services' => $services,
            'maxCustomerTotal' => $maxCustomerTotal,
            'minCustomerTotal' => $minCustomerTotal,
            'maxDriverTotal' => $maxDriverTotal,
            'minDriverTotal' => $minDriverTotal,
            'maxDistance' => $maxDistance,
            'minDistance' => $minDistance,
        ];
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('trips', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = trips::find($id);
    	if($vehicle->delete())
    	{
    		$request->session()->flash('success', 'Request deleted successfully.');
    		return redirect()->route('admin.trips');
    	}
    	else
    	{
    		$request->session()->flash('error', 'Request could not be delete.');
    		return redirect()->route('admin.trips');
    	}
    }	

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('trips', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    Trips::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }

    function view(Request $request, $id)
    {
        $trip = Trips::get($id);
        if($trip)
        {
            $messages = Messages::where('messages.trip_id', $id)->orderBy('messages.id', 'asc')->get();
            return view("admin/trips/view", [
                'trip' => $trip,
                'messages' => $messages
            ]);
        }
        else
        {
            abort(404);
        }
    }

    function export(Request $request)
    {
        $data = $request->toArray();
        if($data && isset($data['t']) && $data['t'])
        {
            $headers = [
                "Trip Id",
                "Service",
                "Status",
                "Customer",
                "Customer Mobile",
                "Driver",
                "Driver Mobile",
                "Vehicle",
                "Driver Address",
                "Pickup Address",
                "Destinations",
                "Date",
                "Start Time",
                "Pickup Time",
                "End Time",
                "Waiting Time",
                "Time before passenger",
                "Trip Duration",
                "Distance before passenger",
                "Trip Distance",
                "Customer Charges",
                "Driver Payable"
            ];
            
            $trips = Trips::limit(3000);

            
            if($data['t'] == 'all' && isset($data['d']) && $data['d'])
            {
                $trips->orderBy('trips.created', 'asc');

                $dates = explode('-', $data['d']);
                if(count($dates)  === 2)
                {
                    $trips->where('trips.created', '>=', date( 'Y-m-d 00:00:01', strtotime($dates[0])) )
                    ->where('trips.created', '<=', date( 'Y-m-d 23:59:59', strtotime($dates[1])) );
                }
            }
            elseif($data['t'] == 'filtered')
            {
                $where = [];
                if($request->get('created_on'))
                {
                    $createdOn = $request->get('created_on');
                    if(isset($createdOn[0]) && !empty($createdOn[0]))
                        $where['trips.created >= ?'] = [
                            date('Y-m-d 00:00:00', strtotime($createdOn[0]))
                        ];
                    if(isset($createdOn[1]) && !empty($createdOn[1]))
                        $where['trips.created <= ?'] = [
                            date('Y-m-d 23:59:59', strtotime($createdOn[1]))
                        ];
                }

                if($request->get('customer_price'))
                {
                    $range = $request->get('customer_price');
                    if(is_array($range))
                        $where['trips.customer_total >= ? and trips.customer_total <= ?'] = [$range[0], $range[1]];
                    else
                        $where['trips.customer_total <= ?'] = [$range];
                }

                if($request->get('driver_price'))
                {
                    $range = $request->get('driver_price');
                    if(is_array($range))
                        $where['trips.driver_total >= ? and trips.driver_total <= ?'] = [$range[0], $range[1]];
                    else
                        $where['trips.driver_total <= ?'] = [$range];
                }

                if($request->get('distance'))
                {
                    $range = $request->get('distance');
                    if(is_array($range))
                        $where['((trips.before_pickup_distance + trips.distance) >= ? and (trips.before_pickup_distance + trips.distance) <= ?)'] = [$range[0], $range[1]];
                    else
                        $where['(trips.before_pickup_distance + trips.distance) <= ?'] = [$range];
                }

                if($request->get('users'))
                {
                    $users = $request->get('users');
                    $users = $users ? implode(',', $users) : 0;
                    $where[] = 'trips.user_id IN ('.$users.')';
                }

                if($request->get('drivers'))
                {
                    $driver = $request->get('drivers');
                    $driver = $driver ? implode(',', $driver) : 0;
                    $where[] = 'trips.driver_id IN ('.$driver.')';
                }

                if($request->get('services'))
                {
                    $service = $request->get('services');
                    $service = $service ? implode(',', $service) : 0;
                    $where[] = 'trips.service_id IN ('.$service.')';
                }

                if($request->get('status'))
                {
                    $status = $request->get('status');
                    $status = $status ? implode(',', $status) : 0;
                    $where[] = 'trips.status IN ('.$status.')';
                }

                foreach($where as $query => $values)
                {
                    if(is_array($values))
                        $trips->whereRaw($query, $values);
                    elseif(!is_numeric($query))
                        $trips->where($query, $values);
                    else
                        $trips->whereRaw($values);
                }
            }
            else
            {
                $trips->orderBy('trips.id', 'desc');
            }
            
            $data = [];
            
            foreach($trips->get() as $t)
            {
                $destinations = TripDestinations::where('trip_id', $t->id)->orderBy('id', 'asc')->pluck('address')->toArray();
                $destinations = count($destinations) > 0 ? implode("\n", $destinations) : "";
                $destinations = $destinations . ($t->cancel_location ? "\n--Cancel at Location--\n" . $t->cancel_location : "");
                $waitingTime = $t->waiting_time ? DateTime::hoursToSecondsFormat($t->waiting_time) - DateTime::hoursToSecondsFormat($t->free_wait_time) : "";
                $waitingTime = $waitingTime > 0 ? DateTime::secondsToTime($waitingTime) : "";
                
                $data[] = [
                    $t->id,
                    $t->service ? $t->service->title : "",
                    Trips::getStatus($t->status),
                    $t->customer ? $t->customer->first_name . " " . $t->customer->last_name : "",
                    $t->customer ? $t->customer->phonenumber : "",
                    $t->driver ? $t->driver->first_name . ' ' . $t->driver->last_name :"",
                    $t->driver ? $t->driver->phonenumber :"",
                    $t->vehicle ? $t->vehicle->number : "",
                    $t->from_address,
                    $t->pickup_address,
                    $destinations,
                    date("m/d/Y", strtotime($t->created)),
                    $t->start_trip_time ? date("h:i A", strtotime($t->start_trip_time)) : "",
                    $t->pickup_trip_time ? date("h:i A", strtotime($t->pickup_trip_time)) : "",
                    $t->end_trip_time ? date("h:i A", strtotime($t->end_trip_time)) : "",
                    $waitingTime,
                    $t->before_pickup_duration ? $t->before_pickup_duration : "",
                    $t->duration_time ? $t->duration_time : "",
                    $t->before_pickup_distance ? $t->before_pickup_distance : "",
                    $t->distance ? $t->distance : "",
                    $t->customer_total ? $t->customer_total : "",
                    $t->driver_total ? $t->driver_total : ""
                ];
            }
            
            Excel::download($data, $headers, 'trips.xlsx');
        }
        else
        {
            $request->session()->flash('error', 'Invalid request.');
            return redirect()->route('admin.trips');
        }
    }
}
