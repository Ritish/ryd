<?php
/**
 * Years Class
 *
 * @package    YearsController
 * @copyright  Ritish Vermani
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\Admin\Vehicles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Admins;
use App\Models\Admin\Permissions;
use App\Models\Admin\VehiclesYears;
use App\Models\Admin\Vehicles;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class YearsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_years', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(vehicle_years.year LIKE ?)'] = [$search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['vehicle_years.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['vehicle_years.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'vehicle_years.created_by IN ('.$admins.')';
    	}

    	$listing = VehiclesYears::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/vehicles/years/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/

	    	return view(
	    		"admin/vehicles/years/index", 
	    		[
	    			'listing' => $listing,
                    'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = VehiclesYears::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
        if($adminIds)
        {
            $admins = Admins::getAll(
                [
                    'admins.id',
                    'admins.first_name',
                    'admins.last_name',
                    'admins.status',
                ],
                [
                    'admins.id in ('.implode(',', $adminIds).')'
                ],
                'concat(admins.first_name, admins.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_years', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'year' => [
	            		'required',
                        'numeric',
	            		Rule::unique('vehicle_years')
	            	]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$year = VehiclesYears::create($data);
	        	if($year)
	        	{
	        		if($request->ajax())
	        		{
	        			return Response()->json([
	        					'status' => 'success',
	        					'message' => 'Year created successfully.',
	        					'year' => $year
	        				]);
	        		}
	        		else
	        		{
		        		$request->session()->flash('success', 'Year created successfully.');
		        		return redirect()->route('admin.years');
		        	}
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Year could not be save. Please try again.');
			    	return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    elseif($request->ajax())
    		{
    			return Response()->json([
    					'status' => 'error',
    					'message' => current(current($validator->messages()))
    				]);
    		}
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/vehicles/years/add", []);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_years', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$year = VehiclesYears::find($id);

    	if(!$year)
        {
        	$request->session()->flash('error', 'Year is missing.');
    		return redirect()->route('admin.years');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'year' => [
                        'required',
                        'numeric',
                        Rule::unique('vehicle_years')->ignore($id)
                    ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$year = VehiclesYears::modify($id, $data);
	        	if($year)
	        	{
	        		$request->session()->flash('success', 'Year updated successfully.');
	        		return redirect()->route('admin.years');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Year could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}

	    return view("admin/vehicles/years/edit", [
	    			'year' => $year
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_years', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = VehiclesYears::find($id);
    	if(Vehicles::where('year', $vehicle->year)->count() < 1)
    	{
	    	
	    	if($vehicle->delete())
	    	{
	    		$request->session()->flash('success', 'Year deleted successfully.');
	    		return redirect()->route('admin.years');
	    	}
	    	else
	    	{
	    		$request->session()->flash('error', 'Year could not be delete.');
	    		return redirect()->route('admin.years');
	    	}
	    }
	    else
	    {
	    	$request->session()->flash('error', 'This year is already in use with some vehicles.');
	    	return redirect()->route('admin.brands');
	    }
    }

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('vehicles_years', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    VehiclesYears::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }
}
