<?php
/**
 * Brands Class
 *
 * @package    BrandsController
 * @copyright  Ritish Vermani
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\Admin\Vehicles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\VehiclesBrands;
use App\Models\Admin\Admins;
use App\Models\Admin\Vehicles;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class BrandsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_brands', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(vehicle_brands.title LIKE ?)'] = [$search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['vehicle_brands.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['vehicle_brands.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'vehicle_brands.created_by IN ('.$admins.')';
    	}

    	$listing = VehiclesBrands::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/vehicles/brands/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/
	    	return view(
	    		"admin/vehicles/brands/index", 
	    		[
	    			'listing' => $listing,
                    'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = VehiclesBrands::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
        if($adminIds)
        {
            $admins = Admins::getAll(
                [
                    'admins.id',
                    'admins.first_name',
                    'admins.last_name',
                    'admins.status',
                ],
                [
                    'admins.id in ('.implode(',', $adminIds).')'
                ],
                'concat(admins.first_name, admins.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_brands', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'title' => [
	            		'required',
	            		Rule::unique('vehicle_brands')
	            	]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$brand = VehiclesBrands::create($data);
	        	if($brand)
	        	{
	        		if($request->ajax())
	        		{
	        			return Response()->json([
	        					'status' => 'success',
	        					'message' => 'Brand created successfully.',
	        					'brand' => $brand
	        				]);
	        		}
	        		else
	        		{
		        		$request->session()->flash('success', 'Brand created successfully.');
		        		return redirect()->route('admin.brands');
		        	}
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Brand could not be save. Please try again.');
			    	return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    elseif($request->ajax())
    		{
    			return Response()->json([
    					'status' => 'error',
    					'message' => current(current($validator->messages()))
    				]);
    		}
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/vehicles/brands/add", []);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_brands', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$brand = VehiclesBrands::find($id);

    	if(!$brand)
        {
        	$request->session()->flash('error', 'Brand is missing.');
    		return redirect()->route('admin.brands');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'title' => [
                        'required',
                        Rule::unique('vehicle_brands')->ignore($id)
                    ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$brand = VehiclesBrands::modify($id, $data);
	        	if($brand)
	        	{
	        		$request->session()->flash('success', 'Brand updated successfully.');
	        		return redirect()->route('admin.brands');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Brand could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}

	    return view("admin/vehicles/brands/edit", [
	    			'brand' => $brand
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_brands', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if(Vehicles::where('brand_id', $id)->count() < 1)
    	{
	    	$vehicle = VehiclesBrands::find($id);
	    	if($vehicle->delete())
	    	{
	    		$request->session()->flash('success', 'Brand deleted successfully.');
	    		return redirect()->route('admin.brands');
	    	}
	    	else
	    	{
	    		$request->session()->flash('error', 'Brand could not be delete.');
	    		return redirect()->route('admin.brands');
	    	}
	    }
	    else
	    {
	    	$request->session()->flash('error', 'This brand is already in use with some vehicles.');
	    	return redirect()->route('admin.brands');
	    }
    }	

    function getModelsDropdown(Request $request, $brandId)
    {
    	$models = VehiclesModels::select(['id','title'])
    		->where('brand_id', $brandId)
    		->orderBy('title', 'asc')
    		->get();

    	$html = "<option></option>";
    	foreach($models as $m)
    	{
    		$html .= "<option value='{$m->id}'>{$m->title}</option>";
    	}

    	return Response()->json([
			'status' => 'success',
            'html' => $html,
        ], 200);
    }

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('vehicles_brands', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    VehiclesBrands::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }
}
