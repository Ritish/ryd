<?php
/**
 * Colors Class
 *
 * @package    ColorsController
 * @copyright  Ritish Vermani
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\Admin\Vehicles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\VehiclesColors;
use App\Models\Admin\Admins;
use App\Models\Admin\Vehicles;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class ColorsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_colors', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(vehicle_colors.title LIKE ?)'] = [$search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['vehicle_colors.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['vehicle_colors.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'vehicle_colors.created_by IN ('.$admins.')';
    	}

    	$listing = VehiclesColors::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/vehicles/colors/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
			/** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/
	    	return view(
	    		"admin/vehicles/colors/index", 
	    		[
	    			'listing' => $listing,
	    			'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = VehiclesColors::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
        if($adminIds)
        {
            $admins = Admins::getAll(
                [
                    'admins.id',
                    'admins.first_name',
                    'admins.last_name',
                    'admins.status',
                ],
                [
                    'admins.id in ('.implode(',', $adminIds).')'
                ],
                'concat(admins.first_name, admins.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_colors', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'title' => [
	            		'required',
	            		Rule::unique('vehicle_colors')
	            	]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$color = VehiclesColors::create($data);
	        	if($color)
	        	{
	        		if($request->ajax())
	        		{
	        			return Response()->json([
	        					'status' => 'success',
	        					'message' => 'Color created successfully.',
	        					'color' => $color
	        				]);
	        		}
	        		else
	        		{
		        		$request->session()->flash('success', 'Color created successfully.');
		        		return redirect()->route('admin.colors');
		        	}
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Color could not be save. Please try again.');
			    	return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    elseif($request->ajax())
    		{
    			return Response()->json([
    					'status' => 'error',
    					'message' => current(current($validator->messages()))
    				]);
    		}
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    return view("admin/vehicles/colors/add", []);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_colors', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$color = VehiclesColors::find($id);

    	if(!$color)
        {
        	$request->session()->flash('error', 'Color is missing.');
    		return redirect()->route('admin.colors');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'title' => [
                        'required',
                        Rule::unique('vehicle_colors')->ignore($id)
                    ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$color = VehiclesColors::modify($id, $data);
	        	if($color)
	        	{
	        		$request->session()->flash('success', 'Color updated successfully.');
	        		return redirect()->route('admin.colors');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Color could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}

	    return view("admin/vehicles/colors/edit", [
	    			'color' => $color
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_colors', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if(Vehicles::where('color_id', $id)->count() < 1)
    	{
	    	$vehicle = VehiclesColors::find($id);
	    	if($vehicle->delete())
	    	{
	    		$request->session()->flash('success', 'Color deleted successfully.');
	    		return redirect()->route('admin.colors');
	    	}
	    	else
	    	{
	    		$request->session()->flash('error', 'Color could not be delete.');
	    		return redirect()->route('admin.colors');
	    	}
	    }
	    else
	    {
	    	$request->session()->flash('error', 'This color is already in use with some vehicles.');
	    	return redirect()->route('admin.brands');
	    }
    }

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('vehicles_colors', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    VehiclesColors::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }
}
