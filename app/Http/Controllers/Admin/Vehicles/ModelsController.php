<?php
/**
 * Models Class
 *
 * @package    ModelsController
 * @copyright  Ritish Vermani
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\Admin\Vehicles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\VehiclesModels;
use App\Models\Admin\VehiclesBrands;
use App\Models\Admin\Admins;
use App\Models\Admin\Vehicles;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class ModelsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_models', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(vehicle_models.title LIKE ? or brand.title LIKE ?)'] = [$search, $search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['vehicle_models.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['vehicle_models.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'vehicle_models.created_by IN ('.$admins.')';
    	}

    	if($request->get('brands'))
    	{
    		$brands = $request->get('brands');
    		$brands = $brands ? implode(',', $brands) : 0;
    		$where[] = 'vehicle_models.brand_id IN ('.$brands.')';
    	}

    	$listing = VehiclesModels::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/vehicles/models/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
			/** Filter Data **/
            $filters = $this->filters($request);
            /** Filter Data **/
	    	return view(
	    		"admin/vehicles/models/index", 
	    		[
	    			'listing' => $listing,
	    			'admins' => $filters['admins'],
	    			'brands' => VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get()
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
        $admins = [];
        $adminIds = VehiclesModels::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
        if($adminIds)
        {
            $admins = Admins::getAll(
                [
                    'admins.id',
                    'admins.first_name',
                    'admins.last_name',
                    'admins.status',
                ],
                [
                    'admins.id in ('.implode(',', $adminIds).')'
                ],
                'concat(admins.first_name, admins.last_name) desc'
            );
        }
        return [
            'admins' => $admins
        ];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles_models', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'brand_id' => [
	            		'required'
	            	],
	            	'title' => [
	            		'required',
	            		Rule::unique('vehicle_models')
	            	],
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$model = VehiclesModels::create($data);
	        	if($model)
	        	{
	        		if($request->ajax())
	        		{
	        			return Response()->json([
	        					'status' => 'success',
	        					'message' => 'Model created successfully.',
	        					'model' => $model
	        				]);
	        		}
	        		else
	        		{
		        		$request->session()->flash('success', 'Model created successfully.');
		        		return redirect()->route('admin.models');
		        	}
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Model could not be save. Please try again.');
			    	return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    elseif($request->ajax())
    		{
    			return Response()->json([
    					'status' => 'error',
    					'message' => current(current($validator->messages()))
    				]);
    		}
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    $brands = VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    return view("admin/vehicles/models/add", [
	    		'brands' => $brands
	    	]);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_models', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$model = VehiclesModels::find($id);

    	if(!$model)
        {
        	$request->session()->flash('error', 'Model is missing.');
    		return redirect()->route('admin.models');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'brand_id' => [
	            		'required'
	            	],
	            	'title' => [
	            		'required',
	            		Rule::unique('vehicle_models')->ignore($id)
	            	],
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$model = VehiclesModels::modify($id, $data);
	        	if($model)
	        	{
	        		$request->session()->flash('success', 'Model updated successfully.');
	        		return redirect()->route('admin.models');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Model could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}

		$brands = VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    return view("admin/vehicles/models/edit", [
	    			'model' => $model,
	    			'brands' => $brands
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles_models', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if(Vehicles::where('model_id', $id)->count() < 1)
    	{
	    	$vehicle = VehiclesModels::find($id);
	    	if($vehicle->delete())
	    	{
	    		$request->session()->flash('success', 'Model deleted successfully.');
	    		return redirect()->route('admin.models');
	    	}
	    	else
	    	{
	    		$request->session()->flash('error', 'Model could not be delete.');
	    		return redirect()->route('admin.models');
	    	}
	    }
	    else
	    {
	    	$request->session()->flash('error', 'This model is already in use with some vehicles.');
	    	return redirect()->route('admin.brands');
	    }
    }

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('vehicles_models', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    VehiclesModels::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }
}
