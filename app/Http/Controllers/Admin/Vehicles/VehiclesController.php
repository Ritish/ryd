<?php
/**
 * Vehicles Class
 *
 * @package    VehiclesController
 * @copyright  Ritish Vermani
 * @author     Ritish Vermani <ritish.vermani@hotmail.com>
 * @version    Release: 1.0.0
 * @since      Class available since Release 1.0.0
 */


namespace App\Http\Controllers\Admin\Vehicles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Vehicles;
use App\Models\Admin\Admins;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\VehiclesBrands;
use App\Models\Admin\VehiclesColors;
use App\Models\Admin\VehiclesModels;
use App\Models\Admin\VehiclesYears;
use App\Models\Admin\Services;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;

class VehiclesController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = [];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(vehicles.number LIKE ? or vehicle_brands.title LIKE ? or vehicle_colors.title LIKE ? or vehicle_models.title LIKE ? or vehicles.year LIKE ?)'] = [$search, $search, $search, $search, $search];
    	}

    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['vehicles.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['vehicles.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

    	if($request->get('admins'))
    	{
    		$admins = $request->get('admins');
    		$admins = $admins ? implode(',', $admins) : 0;
    		$where[] = 'vehicles.created_by IN ('.$admins.')';
    	}

    	if($request->get('status') !== "" && $request->get('status') !== null)
    	{    		
    		$where['vehicles.status'] = $request->get('status');
    	}

    	$listing = Vehicles::getListing($request, $where);


    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/vehicles/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
			/** Filter Data **/
			$filters = $this->filters($request);
	    	/** Filter Data **/
	    	return view(
	    		"admin/vehicles/index", 
	    		[
	    			'listing' => $listing,
	    			'admins' => $filters['admins']
	    		]
	    	);
	    }
    }

    function filters(Request $request)
    {
		$admins = [];
		$adminIds = Vehicles::distinct()->whereNotNull('created_by')->pluck('created_by')->toArray();
		if($adminIds)
		{
	    	$admins = Admins::getAll(
	    		[
	    			'admins.id',
	    			'admins.first_name',
	    			'admins.last_name',
	    			'admins.status',
	    		],
	    		[
	    			'admins.id in ('.implode(',', $adminIds).')'
	    		],
	    		'concat(admins.first_name, admins.last_name) desc'
	    	);
	    }
    	return [
	    	'admins' => $admins
    	];
    }

    function add(Request $request)
    {
    	if(!Permissions::hasPermission('vehicles', 'create'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'user_id' => 'required',
	                'number' => 'required',
	                'brand_id' => 'required',
	                'model_id' => 'required',
	                'color_id' => 'required',
	                'year' => 'required',
	                'trim' => 'required',
	                'services' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$services = isset($data['services']) && !empty($data['services']) ? $data['services'] : [];
	        	unset($data['services']);
	        	$vehicle = Vehicles::create($data);
	        	if($vehicle)
	        	{
	        		if(!empty($services))
	        		{
	        			Vehicles::handleServices($vehicle->id, $services);
	        		}

	        		$request->session()->flash('success', 'Vehicle created successfully.');
	        		return redirect()->route('admin.vehicles');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Vehicle could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    $drivers = Users::select(['id', 'first_name', 'last_name', 'country_code', 'phonenumber', 'status'])->where('driver', 1)->get();
	    $brands = VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    $colors = VehiclesColors::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    $years = VehiclesYears::select(['id', 'year'])->orderBy('year', 'asc')->get();
	    $services = Services::orderBy('id', 'asc')->get();
	    return view("admin/vehicles/add", [
	    			'drivers' => $drivers,
	    			'brands' => $brands,
	    			'colors' => $colors,
	    			'years' => $years,
	    			'services' => $services
	    		]);
    }

    function edit(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles', 'update'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = Vehicles::find($id);

    	if(!$vehicle)
        {
        	$request->session()->flash('error', 'Vehicle is missing.');
    		return redirect()->route('admin.vehicles');
        }

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		unset($data['_token']);

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	            	'user_id' => 'required',
	                'number' => 'required',
	                'brand_id' => 'required',
	                'model_id' => 'required',
	                'color_id' => 'required',
	                'year' => 'required',
	                'trim' => 'required',
	                'services' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	
        		$services = isset($data['services']) && !empty($data['services']) ? $data['services'] : [];
        		unset($data['services']);
        		if(isset($data['image']) && $data['image'])
        		{
        			$data['image'] = json_decode($data['image'], true);
        			$vehicle->image = $vehicle->image ? json_decode($vehicle->image) : [];
	        		$data['image'] = array_merge($vehicle->image, $data['image']);
	        		$data['image'] = json_encode($data['image']);
	        	}
	        	else
	        	{
	        		unset($data['image']);
	        	}

	        	$vehicle = Vehicles::modify($id, $data);
	        	if($vehicle)
	        	{
	        		if(!empty($services))
	        		{
	        			Vehicles::handleServices($vehicle->id, $services);
	        		}

	        		$request->session()->flash('success', 'Vehicle updated successfully.');
	        		return redirect()->route('admin.vehicles');
	        	}
	        	else
	        	{
	        		$request->session()->flash('error', 'Vehicle could not be save. Please try again.');
		    		return redirect()->back()->withErrors($validator)->withInput();
	        	}
		    }
		    else
		    {
		    	$request->session()->flash('error', 'Please provide valid inputs.');
		    	return redirect()->back()->withErrors($validator)->withInput();
		    }
		}
	    
	    $drivers = Users::select(['id', 'first_name', 'last_name', 'country_code', 'phonenumber', 'status'])->where('driver', 1)->get();
	    $brands = VehiclesBrands::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    $colors = VehiclesColors::select(['id', 'title'])->orderBy('title', 'asc')->get();
	    $models = VehiclesModels::select(['id', 'title'])->where('brand_id', $vehicle->brand_id)->orderBy('title', 'asc')->get();
	    $years = VehiclesYears::select(['id', 'year'])->orderBy('year', 'asc')->get();
	    $services = Services::orderBy('id', 'asc')->get();
	    return view("admin/vehicles/edit", [
	    			'vehicle' => $vehicle,
	    			'drivers' => $drivers,
	    			'brands' => $brands,
	    			'colors' => $colors,
	    			'models' => $models,
	    			'years' => $years,
	    			'services' => $services
	    		]);
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('vehicles', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = Vehicles::find($id);
    	if($vehicle->delete())
    	{
    		$request->session()->flash('success', 'Vehicle deleted successfully.');
    		return redirect()->route('admin.vehicles');
    	}
    	else
    	{
    		$request->session()->flash('error', 'Vehicle could not be delete.');
    		return redirect()->route('admin.vehicles');
    	}
    }

    function bulkActions(Request $request, $action)
    {
    	if( ($action != 'delete' && !Permissions::hasPermission('vehicles', 'update')) || ($action == 'delete' && !Permissions::hasPermission('vehicles', 'delete')) ) 
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$ids = $request->get('ids');
    	if(is_array($ids) && !empty($ids))
    	{
    		switch ($action) {
    			case 'active':
    				Vehicles::modifyAll($ids, [
    					'status' => 1
    				]);
    				$message = count($ids) . ' records has been published.';
    			break;
    			case 'inactive':
    				Vehicles::modifyAll($ids, [
    					'status' => 0
    				]);
    				$message = count($ids) . ' records has been unpublished.';
    			break;
    			case 'delete':
    				Vehicles::removeAll($ids);
    				$message = count($ids) . ' records has been deleted.';
    			break;
    		}

    		$request->session()->flash('success', $message);

    		return Response()->json([
    			'status' => 'success',
	            'message' => $message,
	        ], 200);		
    	}
    	else
    	{
    		return Response()->json([
    			'status' => 'error',
	            'message' => 'Please select atleast one record.',
	        ], 200);	
    	}
    }

    function getModelsDropdown(Request $request, $brandId)
    {
    	$models = VehiclesModels::select(['id','title'])
    		->where('brand_id', $brandId)
    		->orderBy('title', 'asc')
    		->get();

    	$html = "<option></option>";
    	foreach($models as $m)
    	{
    		$html .= "<option value='{$m->id}'>{$m->title}</option>";
    	}

    	return Response()->json([
			'status' => 'success',
            'html' => $html,
        ], 200);
    }
}
