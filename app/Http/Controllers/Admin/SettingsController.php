<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Libraries\General;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Models\Admin\Services;
use App\Models\Admin\Permissions;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Libraries\FileSystem;
use App\Libraries\DateTime;

class SettingsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
		if(!AdminAuth::isAdmin())
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.settings');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'company_name' => 'required',
	                'company_address' => 'required',
	                'pagination_method' => 'required',
	                'admin_second_auth_factor' => 'required',
	                'currency_code' => 'required',
	                'currency_symbol' => 'required',
	                'admin_notification_email' => [
	                	'required',
	                	'email'
	                ]
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$logo = null;
	        	if(isset($data['logo']) && $data['logo']) 
	        	{
	        		$logo = $data['logo'];
	        	}
	        	
	        	$favicon = null;
	        	if(isset($data['favicon']) && $data['favicon']) 
	        	{
	        		$favicon = $data['favicon'];
	        	}

	        	unset($data['logo']);
	        	unset($data['favicon']);
	        	unset($data['_token']);

	        	foreach ($data as $key => $value) {
	        		Settings::put($key, $value);
	        	}

	        	if($logo)
	        	{
	        		if(Settings::put('logo', $logo))
	        		{
		        		$originalName = FileSystem::getFileNameFromPath($logo);
		        		FileSystem::resizeImage($logo, $originalName, '400*120');
		        	}
	        	}

	        	if($favicon)
	        	{
	        		if(Settings::put('favicon', $favicon))
	        		{
		        		$originalName = FileSystem::getFileNameFromPath($favicon);
		        		FileSystem::resizeImage($favicon, $originalName, '50*50');
		        	}
	        	}
	        	
        		$request->session()->flash('success', 'Settings updated successfully.');
        		return redirect()->route('admin.settings');
			}
			else
			{
				$request->session()->flash('error', 'Please provide valid inputs.');
			    	return redirect()->back()->withErrors($validator)->withInput();
			}
		}

		return view("admin/settings/index", []);
	}

	function recaptcha(Request $request)
    {
		if(!AdminAuth::isAdmin())
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.settings');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'admin_recaptcha' => 'required',
	                'recaptcha_key' => 'required',
	                'recaptcha_secret' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	unset($data['_token']);
	        	foreach ($data as $key => $value) {
	        		Settings::put($key, $value);
	        	}
	
        		$request->session()->flash('success', 'Recaptcha settings updated.');
        		return redirect()->route('admin.settings');
			}
			else
			{
				$request->session()->flash('error', 'Please provide valid inputs.');
			    	return redirect()->back()->withErrors($validator)->withInput();
			}
		}
		else
		{
			abort(404);
		}
	}

	function email(Request $request)
    {
		if(!AdminAuth::isAdmin())
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.settings');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'email_method' => 'required',
	                'from_email' => 'required',
	                'smtp_host' => 'required',
	                'smtp_encryption' => 'required',
	                'smtp_port' => 'required',
	                'smtp_username' => 'required',
	                'smtp_email' => 'required',
	                'sendgrid_email' => 'required',
	                'sendgrid_api_key' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	unset($data['_token']);
	        	
	        	$password = null;
	        	if(isset($data['smtp_password']) && $data['smtp_password'])
	        	{
	        		$password = $data['smtp_password'];
	        	}
	        	unset($data['smtp_password']);

	        	foreach($data as $key => $value) {
	        		Settings::put($key, $value);
	        	}

	        	if(isset($password) && $password)
	        	{
	        		$password = General::encrypt($password);
	        		Settings::put('smtp_password', $password);
	        	}
	
        		$request->session()->flash('success', 'Password updated successfully.');
        		return redirect()->route('admin.settings');
			}
			else
			{
				$request->session()->flash('error', 'Please provide valid inputs.');
			    	return redirect()->back()->withErrors($validator)->withInput();
			}
		}
		else
		{
			abort(404);
		}
	}

	function dateTimeFormats(Request $request)
	{
		if($request->isMethod('post'))
    	{
    		$data = $request->toArray();
    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'date_format' => 'required',
	                'time_format' => 'required'
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	unset($data['_token']);
	        	
	        	foreach ($data as $key => $value) {
	        		Settings::put($key, $value);
	        	}

	        	$request->session()->flash('success', 'Date and time format updated.');
        		return redirect()->route('admin.settings');
			}
			else
			{
				$request->session()->flash('error', 'Please provide valid inputs.');
			    return redirect()->back()->withErrors($validator)->withInput();
			}
		}
		else
		{
			abort(404);
		}
	}

	function home(Request $request)
    {
		if(!AdminAuth::isAdmin())
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.settings');
    	}

    	if($request->isMethod('post'))
    	{
    		$data = $request->toArray();

    		$validator = Validator::make(
	            $request->toArray(),
	            [
	                'home_page_text' => 'required',
	                'home_page_search_text' => 'required',
	            ]
	        );

	        if(!$validator->fails())
	        {
	        	$banner = null;
	        	if(isset($data['home_page_banner']) && $data['home_page_banner'])
	        	{
	        		$banner = $data['home_page_banner'];
	        	}

	        	unset($data['home_page_banner']);
	        	unset($data['_token']);

	        	foreach ($data as $key => $value) {
	        		Settings::put($key, $value);
	        	}

	        	if($banner)
	        	{
	        		if(Settings::put('home_page_banner', $banner))
	        		{
		        		$originalName = FileSystem::getFileNameFromPath($banner);
		        		FileSystem::resizeImage($banner, $originalName, '612*378');
		        	}
	        	}
	        	
        		$request->session()->flash('success', 'Settings updated successfully.');
        		return redirect()->route('admin.settings.home');
			}
			else
			{
				$request->session()->flash('error', 'Please provide valid inputs.');
			    return redirect()->back()->withErrors($validator)->withInput();
			}
		}

		return view("admin/settings/home", []);
	}

	function pricing(Request $request, $id)
	{
		if(!AdminAuth::isAdmin())
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.settings');
    	}

    	$service = Services::find($id);

    	if($service)
    	{
	    	if($request->isMethod('post'))
	    	{
	    		$data = $request->toArray();
		        if(!empty($data))
		        {
		        	if(isset($data['driver_free_wait_time']) && $data['driver_free_wait_time'])
		        	{
		        		$seconds = DateTime::hoursToSecondsFormat(implode(':',$data['driver_free_wait_time']));
		        		$seconds = DateTime::secondsToQuantity($seconds);
		        		$data['driver_free_wait_time'] = $seconds;
		        	}

		        	if(isset($data['free_wait_time']) && $data['free_wait_time'])
		        	{
		        		$seconds = DateTime::hoursToSecondsFormat(implode(':',$data['free_wait_time']));
		        		$seconds = DateTime::secondsToQuantity($seconds);
		        		$data['free_wait_time'] = $seconds;
		        	}

		        	foreach ($data as $key => $value) {
		        		Services::putPriceValue($id, $key, $value);
		        	}
		        	
	        		$request->session()->flash('success', 'Pricing updated successfully.');
	        		return redirect()->route('admin.settings.pricing', ['id', $id]);
				}
				else
				{
					$request->session()->flash('error', 'Please provide valid inputs.');
				    return redirect()->back()->withErrors($validator)->withInput();
				}
			}

			return view("admin/settings/pricing", ['id' => $id, 'service' => $service]);
		}
		else
		{
    		return redirect()->route('admin.settings');
		}
	}
}
