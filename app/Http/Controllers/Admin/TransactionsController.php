<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Settings;
use App\Models\Admin\AdminAuth;
use App\Libraries\General;
use App\Models\Admin\Users;
use App\Models\Admin\Permissions;
use App\Models\Admin\Transactions;
use App\Models\Admin\Admins;
use App\Models\Admin\Services;
use App\Models\Admin\Trips;
use Illuminate\Validation\Rule;
use App\Libraries\FileSystem;
use App\Http\Controllers\Admin\AppController;
use Illuminate\Support\Facades\DB;

class TransactionsController extends AppController
{
	function __construct()
	{
		parent::__construct();
	}

    function index(Request $request)
    {
    	if(!Permissions::hasPermission('transactions', 'listing'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$where = ['type' => 'payment'];
    	if($request->get('search'))
    	{
    		$search = $request->get('search');
    		$search = '%' . $search . '%';
    		$where['(transactions.driver_name LIKE ?  or transactions.customer_name LIKE ? or transactions.vehicle_number LIKE ? or transactions.service_name LIKE ? or transactions.trip_id LIKE ? or transactions.stripe_charge_id LIKE ? or transactions.amount LIKE ? or transactions.status LIKE ?)'] = [$search, $search, $search, $search, $search, $search, $search, $search];
    	}
        
    	if($request->get('created_on'))
    	{
    		$createdOn = $request->get('created_on');
    		if(isset($createdOn[0]) && !empty($createdOn[0]))
    			$where['transactions.created >= ?'] = [
    				date('Y-m-d 00:00:00', strtotime($createdOn[0]))
    			];
    		if(isset($createdOn[1]) && !empty($createdOn[1]))
    			$where['transactions.created <= ?'] = [
    				date('Y-m-d 23:59:59', strtotime($createdOn[1]))
    			];
    	}

        if($request->get('users'))
        {
            $users = $request->get('users');
            $users = $users ? implode(',', $users) : 0;
            $where[] = 'transactions.user_id IN ('.$users.')';
        }

        if($request->get('drivers'))
        {
            $driver = $request->get('drivers');
            $driver = $driver ? implode(',', $driver) : 0;
            $where[] = 'transactions.driver_id IN ('.$driver.')';
        }

        if($request->get('services'))
        {
            $service = $request->get('services');
            $service = $service ? implode(',', $service) : 0;
            $where[] = 'transactions.service_id IN ('.$service.')';
        }

        if($request->get('customer_price'))
        {
            $where['transactions.amount <= ?'] = [$request->get('customer_price')];
        }

        if($request->get('status'))
        {
            $status = $request->get('status');
            $or = [];
            foreach($status as $s)
            {
                $or[] = 'transactions.status LIKE "'.$s.'"';
            }
            $where[] = '('.implode(' or ', $or).')';
        }

    	$listing = Transactions::getListing($request, $where);

    	if($request->ajax())
    	{
		    $html = view(
	    		"admin/transactions/listingLoop", 
	    		[
	    			'listing' => $listing
	    		]
	    	)->render();

		    return Response()->json([
		    	'status' => 'success',
	            'html' => $html,
	            'page' => $listing->currentPage(),
	            'counter' => $listing->perPage(),
	            'count' => $listing->total(),
	            'pagination_counter' => $listing->currentPage() * $listing->perPage()
	        ], 200);
		}
		else
		{
            /** Filter Data **/
            $filters = $this->filters($request, 'payment');
            $filters['listing'] = $listing;
            /** Filter Data **/
	    	return view(
	    		"admin/transactions/index",
	    		$filters
	    	);
	    }
    }

    function filters(Request $request, $type = null)
    {
        $users = [];
        $userIds = Transactions::distinct()
            ->whereNotNull('user_id')
            ->where('type', $type)
            ->pluck('user_id')
            ->toArray();
        if($userIds)
        {
            $users = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $userIds).')',
                    'driver = 0'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $drivers = [];
        $driverIds = Transactions::distinct()
            ->whereNotNull('driver_id')
            ->where('type', $type)
            ->pluck('driver_id')
            ->toArray();
        if($driverIds)
        {
            $drivers = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $driverIds).')',
                    'driver = 1'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $services = Services::orderBy('title', 'asc')->get();
        $maxCustomerTotal = Transactions::where('type', $type)->max('amount');
        $minCustomerTotal = Transactions::where('type', $type)->min('amount');
        $statuses = Transactions::orderBy('status', 'asc')
            ->where('type', $type)
            ->groupBy('status')
            ->pluck('status')
            ->toArray();
        
        return [
            'users' => $users,
            'drivers' => $drivers,
            'services' => $services,
            'maxCustomerTotal' => $maxCustomerTotal,
            'minCustomerTotal' => $minCustomerTotal,
            'statuses' => $statuses,
            'type' => $type
        ];
    }

    function delete(Request $request, $id)
    {
    	if(!Permissions::hasPermission('transactions', 'delete'))
    	{
    		$request->session()->flash('error', 'Permission denied.');
    		return redirect()->route('admin.dashboard');
    	}

    	$vehicle = Transactions::find($id);
    	if($vehicle->delete())
    	{
    		$request->session()->flash('success', 'Request deleted successfully.');
    		return redirect()->route('admin.transactions');
    	}
    	else
    	{
    		$request->session()->flash('error', 'Request could not be delete.');
    		return redirect()->route('admin.transactions');
    	}
    }	

    function bulkActions(Request $request, $action)
    {
        if(!Permissions::hasPermission('transactions', 'delete'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $ids = $request->get('ids');

        if(is_array($ids) && !empty($ids))
        {
            switch ($action) {
                case 'delete':
                    Transactions::removeAll($ids);
                    $message = count($ids) . ' records has been deleted.';
                break;
            }

            $request->session()->flash('success', $message);

            return Response()->json([
                'status' => 'success',
                'message' => $message,
            ], 200);        
        }
        else
        {
            return Response()->json([
                'status' => 'error',
                'message' => 'Please select atleast one record.',
            ], 200);    
        }
    }

    function payouts(Request $request)
    {
        if(!Permissions::hasPermission('payouts', 'listing'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $where = ['type' => 'transfer'];
        if($request->get('search'))
        {
            $search = $request->get('search');
            $search = '%' . $search . '%';
            $where['(transactions.driver_name LIKE ?  or transactions.customer_name LIKE ? or transactions.vehicle_number LIKE ? or transactions.service_name LIKE ? or transactions.trip_id LIKE ? or transactions.stripe_charge_id LIKE ? or transactions.amount LIKE ? or transactions.status LIKE ?)'] = [$search, $search, $search, $search, $search, $search, $search, $search];
        }
        
        if($request->get('created_on'))
        {
            $createdOn = $request->get('created_on');
            if(isset($createdOn[0]) && !empty($createdOn[0]))
                $where['transactions.created >= ?'] = [
                    date('Y-m-d 00:00:00', strtotime($createdOn[0]))
                ];
            if(isset($createdOn[1]) && !empty($createdOn[1]))
                $where['transactions.created <= ?'] = [
                    date('Y-m-d 23:59:59', strtotime($createdOn[1]))
                ];
        }

        if($request->get('users'))
        {
            $users = $request->get('users');
            $users = $users ? implode(',', $users) : 0;
            $where[] = 'transactions.user_id IN ('.$users.')';
        }

        if($request->get('drivers'))
        {
            $driver = $request->get('drivers');
            $driver = $driver ? implode(',', $driver) : 0;
            $where[] = 'transactions.driver_id IN ('.$driver.')';
        }

        if($request->get('services'))
        {
            $service = $request->get('services');
            $service = $service ? implode(',', $service) : 0;
            $where[] = 'transactions.service_id IN ('.$service.')';
        }

        if($request->get('customer_price'))
        {
            $where['transactions.amount <= ?'] = [$request->get('customer_price')];
        }

        if($request->get('status'))
        {
            $status = $request->get('status');
            $or = [];
            foreach($status as $s)
            {
                $or[] = 'transactions.status LIKE "'.$s.'"';
            }
            $where[] = '('.implode(' or ', $or).')';
        }

        $listing = Transactions::getListing($request, $where);

        if($request->ajax())
        {
            $html = view(
                "admin/payouts/listingLoop", 
                [
                    'listing' => $listing
                ]
            )->render();

            

            return Response()->json([
                'status' => 'success',
                'html' => $html,
                'page' => $listing->currentPage(),
                'counter' => $listing->perPage(),
                'count' => $listing->total(),
                'pagination_counter' => $listing->currentPage() * $listing->perPage(),

            ], 200);
        }
        else
        {
            $counts = [
                'pending' => Trips::select(['id'])->where('status', '5')->where('driver_transfered', 0)->get()->count(),
                'transfered' => Transactions::select(['id'])->where('type', 'transfer')->get()->count(),
                'pendingAmount' => Trips::where('status', '5')->where('driver_transfered', 0)->sum('driver_total'),
                'tranferedAmount' => Transactions::select(['id'])->where('type', 'transfer')->sum('amount'),
                'totalSale' => Trips::where('status', '5')->sum('customer_total')
            ];

            /** Filter Data **/
            $filters = $this->filters($request, 'transfer');
            /** Filter Data **/
            $data = $filters;
            $data['listing'] = $listing;
            $data['counts'] = $counts;
            return view(
                "admin/payouts/index",
                $data
            );
        }
    }

    function pendingPayouts(Request $request)
    {
        if(!Permissions::hasPermission('payouts', 'listing'))
        {
            $request->session()->flash('error', 'Permission denied.');
            return redirect()->route('admin.dashboard');
        }

        $where = ['trips.status' => '5', 'trips.driver_transfered' => 1];
        if($request->get('search'))
        {
            $search = $request->get('search');
            $search = '%' . $search . '%';
            $where['(users.driver_total LIKE ? or concat(users.first_name, " ", users.last_name) LIKE ? or users.first_name LIKE ? or users.last_name LIKE ?  or users.phonenumber LIKE ?  or users.email LIKE ? or concat(driver.first_name, " ", driver.last_name) LIKE ? or driver.first_name LIKE ? or driver.last_name LIKE ? or driver.phonenumber LIKE ? or driver.email LIKE ? or vehicles.number  LIKE ? or services.title LIKE ?)'] = [$search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search, $search];
        }
        
        if($request->get('created_on'))
        {
            $createdOn = $request->get('created_on');
            if(isset($createdOn[0]) && !empty($createdOn[0]))
                $where['trips.created >= ?'] = [
                    date('Y-m-d 00:00:00', strtotime($createdOn[0]))
                ];
            if(isset($createdOn[1]) && !empty($createdOn[1]))
                $where['trips.created <= ?'] = [
                    date('Y-m-d 23:59:59', strtotime($createdOn[1]))
                ];
        }

        if($request->get('users'))
        {
            $users = $request->get('users');
            $users = $users ? implode(',', $users) : 0;
            $where[] = 'trips.user_id IN ('.$users.')';
        }

        if($request->get('drivers'))
        {
            $driver = $request->get('drivers');
            $driver = $driver ? implode(',', $driver) : 0;
            $where[] = 'trips.driver_id IN ('.$driver.')';
        }

        if($request->get('services'))
        {
            $service = $request->get('services');
            $service = $service ? implode(',', $service) : 0;
            $where[] = 'trips.service_id IN ('.$service.')';
        }

        if($request->get('driver_price'))
        {
            $where['trips.driver_total <= ?'] = [$request->get('driver_price')];
        }

        $listing = Trips::getListing($request, $where);

        if($request->ajax())
        {
            $html = view(
                "admin/payouts/pending/listingLoop", 
                [
                    'listing' => $listing
                ]
            )->render();

            

            return Response()->json([
                'status' => 'success',
                'html' => $html,
                'page' => $listing->currentPage(),
                'counter' => $listing->perPage(),
                'count' => $listing->total(),
                'pagination_counter' => $listing->currentPage() * $listing->perPage(),

            ], 200);
        }
        else
        {
            $counts = [
                'pending' => Trips::select(['id'])->where('status', '5')->where('driver_transfered', 0)->get()->count(),
                'transfered' => Transactions::select(['id'])->where('type', 'transfer')->get()->count(),
                'pendingAmount' => Trips::where('status', '5')->where('driver_transfered', 0)->sum('driver_total'),
                'tranferedAmount' => Transactions::select(['id'])->where('type', 'transfer')->sum('amount'),
                'totalSale' => Trips::where('status', '5')->sum('customer_total')
            ];

            /** Filter Data **/
            $filters = $this->pendingPayoutFilters($request);
            /** Filter Data **/
            $data = $filters;
            $data['listing'] = $listing;
            $data['counts'] = $counts;
            return view(
                "admin/payouts/pending/index",
                $data
            );
        }
    }

    function pendingPayoutFilters(Request $request)
    {
        $users = [];
        $userIds = Trips::distinct()->whereNotNull('user_id')->where('status', 5)->where('driver_transfered', 0)->pluck('user_id')->toArray();
        if($userIds)
        {
            $users = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $userIds).')',
                    'driver = 0'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $drivers = [];
        $driverIds = Trips::distinct()->whereNotNull('driver_id')->where('status', 5)->where('driver_transfered', 0)->pluck('driver_id')->toArray();
        if($driverIds)
        {
            $drivers = Users::getAll(
                [
                    'users.id',
                    'users.first_name',
                    'users.last_name',
                    'users.status',
                ],
                [
                    'users.id in ('.implode(',', $driverIds).')',
                    'driver = 1'
                ],
                'concat(users.first_name, users.last_name) desc'
            );
        }

        $services = Services::orderBy('title', 'asc')->get();
        $maxDriverTotal = Trips::where('status', 5)->where('driver_transfered', 0)->max('driver_total');
        $maxDistance = Trips::where('status', 5)->where('driver_transfered', 0)->max(DB::raw('(trips.before_pickup_distance + trips.distance)'));
        return [
            'users' => $users,
            'drivers' => $drivers,
            'services' => $services,
            'maxDriverTotal' => $maxDriverTotal,
            'maxDistance' => $maxDistance,
        ];
    }
}
