<?php
namespace App\Libraries;

use App\Models\Admin\Settings;
use App\Libraries\General;

class Stripe
{

	private static function getClient()
	{
		$key = Settings::get('stripe_key');
		return new \Stripe\StripeClient($key);
	}

	public static function createCustomer($user)
	{
		try
        {
			$stripe = Stripe::getClient();
			$customer = $stripe->customers->create([
				// 'address' => [
	                // 'line1' => isset($user->address) ? $user->address : '',
	                // 'city' => isset($user->city_name) ? $user->city_name : '',
	                // 'state' => isset($user->state_name) ? $user->state_name : '',
	                // 'postal_code' => isset($user->zipcode) ? $user->zipcode : '',
	            // ],
	            'email' => isset($user->email) ? $user->email : '',
	            'name' =>  isset($user->first_name) ? ( $user->first_name . ($user->last_name ? ' ' . $user->last_name : '') ) : '',
	            'metadata' => [
	                'Role' => isset($user->driver) && $user->driver ? 'Driver' : 'Customer'
	            ]
	        ]);

	        if($customer && isset($customer->id) && $customer->id)
            {
                $user->stripe_customer_id = $customer->id;
                if($user->save())
                {
                	return $customer->id;
                }
                else
                {
             		return null;
                }
            }
            else
            {
                return null;
            }

		}
        catch(\Exception $e)
        {
            return null;
        }
	}

	public static function updateCustomer($user)
	{
		try
        {
        	if($user->stripe_customer_id)
        	{
				$stripe = Stripe::getClient();
				$customer = $stripe->customers->update($user->stripe_customer_id, [
					// 'address' => [
		            //     'line1' => isset($user->address) ? $user->address : '',
		            //     'city' => isset($user->city_name) ? $user->city_name : '',
		            //     'state' => isset($user->state_name) ? $user->state_name : '',
		            //     'postal_code' => isset($user->zipcode) ? $user->zipcode : '',
		            // ],
		            'email' => isset($user->email) ? $user->email : '',
		            'name' =>  isset($user->first_name) ? ( $user->first_name . ($user->last_name ? ' ' . $user->last_name : '') ) : '',
		            'metadata' => [
		                'Role' => isset($user->driver) && $user->driver ? 'Driver' : 'Customer'
		            ]
		        ]);

		        if($customer && isset($customer->id) && $customer->id)
	            {
	                $user->stripe_customer_id = $customer->id;
	                if($user->save())
	                {
	                	return $customer->id;
	                }
	                else
	                {
	             		return null;
	                }
	            }
	            else
	            {
	                return null;
	            }
	        }
	        else
	        {
	        	return null;
	        }

		}
        catch(\Exception $e)
        {
            return null;
        }
	}

	public static function createCardToken($data)
	{
		try
		{
			$stripe = Stripe::getClient();
			$token = $stripe->tokens->create([
			  'card' => [
			    'number' => $data['card_number'],
			    'exp_month' => $data['exp_month'],
			    'exp_year' => strlen($data['exp_year']) > 2 ? $data['exp_year'] : '20' . $data['exp_year'] ,
			    'cvc' => $data['cvv'],
			  ],
			]);

			if($token && isset($token->id) && $token->id)
	        {
	            return $token->id;
	        }
	        else
	        {
	            return null;
	        }
	    }
	    catch(\Exception $e)
	    {
	    	return null;
	    }
	}

	public static function createCard($card, $token)
	{
		try
		{
			$stripe = Stripe::getClient();
			$stripCard = $stripe->customers->createSource(
				  	$card->stripe_customer_id,
				  	['source' => $token]
				);

			if($stripCard && isset($stripCard->id) && $stripCard->id)
	        {
	        	$card->stripe_card_id = $stripCard->id;
	        	if($card->save())
	        	{
	            	return $stripCard->id;
	            }
	            else
	            {
	         		return null;   	
	            }
	        }
	        else
	        {
	            return null;
	        }
	    }
	    catch(\Exception $e)
	    {
	    	return null;
	    }
	}

	public static function chargeCard($user, $amount, $description)
    {
        try
        {
            if($user->stripe_customer_id)
            {
            	$stripe = Stripe::getClient();
                $charge = $stripe->charges->create([
                  'amount' => $amount*100,
                  'currency' => Settings::get('currency_code'),
                  'customer' => $user->stripe_customer_id,
                  'description' => $description
                ]);
                
                if($charge && isset($charge->id) && $charge->id)
                {
                    return $charge;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        catch(\Exception $e)
        {
            return null;
        }
    }


    public static function getAccount($id)
    {
        try
        {
        	$stripe = Stripe::getClient();
            $account = $stripe->accounts->retrieve($id);
            return $account;
            
        }
        catch(\Exception $e)
        {
            return null;
        }
    }

    public static function createAccount($user)
    {     
        try
        {
            $stripe = Stripe::getClient();
            $account = $stripe->accounts->create([
                'type' => 'express',
                'country' => 'US',
                'email' => isset($user->email) ? $user->email : '',
                'business_type' => 'individual',
                'individual' => [
                    'email' => isset($user->email) ? $user->email : '',
                    'first_name' =>  isset($user->first_name) ? $user->first_name : '',
                    'last_name' =>  isset($user->last_name) ? $user->last_name : '',
                ],
                'capabilities' => [
                    'card_payments' => ['requested' => true],
                    'transfers' => ['requested' => true],
                ],
            ]);

            if($account && isset($account->id) && $account->id)
            {
            	$user->stripe_account_id = $account->id;
            	$user->stripe_account_verified = 0;
            	$user->stripe_connect_by_customer = 0;
            	if($user->save())
            	{
                	return $account->id;
                }
                else
                {
                	return null;
                }
            }
            else
            {
                return null;
            }
        }
        catch(\Exception $e)
        {
            return null;
        }
    }

     public static function createAccountLink($userId, $accountId)
    {
    	try
    	{
	    	$stripe = Stripe::getClient();
	        $link = $stripe->accountLinks->create([
	          'account' => $accountId,
	          'refresh_url' => url('/stripe/account-linking?user_id=' . urlencode(base64_encode(convert_uuencode($userId)))),
	          'return_url' => url('/stripe/link-account-success?user_id=' . urlencode(base64_encode(convert_uuencode($userId)))),
	          'type' => 'account_onboarding',
	        ]);

	        if ($link && isset($link->url) && $link->url) 
	        {
	            return $link->url;
	        }

	        return null;
	    }
	    catch(\Exception $e)
	    {
	    	return null;	
	    }
	        
    }

    public static function createTransfer($user, $source, $amount)
    {   
        // try
        // {
        	if($user->stripe_account_id && $user->stripe_account_verified && $user->stripe_connect_by_customer)
        	{
            	$stripe = Stripe::getClient();
                $transfer = $stripe->transfers->create(
                    [
                      'amount' => $amount*100,
                      'currency' => Settings::get('currency_code'),
                      'destination' => $user->stripe_account_id,
                      'source_transaction' => $source
                    ]
                );

                if($transfer && isset($transfer->id) && $transfer->id)
                {
                    return $transfer->id;
                }
                else
                {
                    return null;
                }
	        }
	        else
            {
                return null;
            }
        // }
        // catch(\Exception $e)
        // {
        //     return null;
        // }
    }

}