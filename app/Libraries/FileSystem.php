<?php
namespace App\Libraries;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as ResizeImage;

class FileSystem
{
	public static function uploadImage($file, $path)
	{
		$extension = $file->extension();
		$name = $file->getClientOriginalName();
		if( in_array($file->getClientMimeType(), array('image/jpeg', 'image/gif', 'image/png')) )
		{
			$name = explode('.', $name);
			$name =time() . mt_rand(99, 9999) . '-' . Str::slug(current($name)) . '.' . $extension;
			if($file->storeAs($path, $name))
			{
				$path = '/uploads/' . $path . '/' . $name;

				return file_exists(public_path($path)) ? $path : false;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public static function uploadBase64Image($file, $path)
	{
		list($type, $file) = explode(';', $file);
	    list(,$extension) = explode('/',$type);
	    list(,$file)      = explode(',', $file);
	    if( in_array($extension, array('jpeg', 'jpg', 'png')) )
	    {
	    	if(!is_dir(public_path('/uploads/' . $path)))
	    	{
		    	mkdir(public_path('/uploads/' . $path), 0777);
		    }

		    $fileName = '/uploads/' . $path. '/' . time() . rand(999, 99999) . '.' . $extension;
		    $file = base64_decode($file);
		    file_put_contents(public_path($fileName), $file);
		    return file_exists(public_path($fileName)) ? $fileName : false;
		}
		else
		{
			return false;
		}
	}

	public static function resizeImage($file, $name, $size)
	{
		$path = FileSystem::getOnlyPath($file);
		$path = $path . '/' . $name;
		$size = explode('*', $size);
		
		ResizeImage::make(public_path($file))
			->fit($size[0], $size[1], function ($constraint) {
			    $constraint->aspectRatio();
			})
			->save(public_path($path));
		
		return file_exists(public_path($path)) ? $path : null;
	}

	public static function uploadFile($file, $path)
	{
		$extension = $file->extension();
		$name = $file->getClientOriginalName();
		if( in_array($file->getClientMimeType(),  array('image/jpeg', 'image/gif', 'image/png', 'application/pdf', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', ' application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/csv', 'text/plain', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip', 'image/svg+xml')) )
		{
			$name = explode('.', $name);
			$name = time() . mt_rand(99, 9999) . '-' . Str::slug(current($name)) . '.' . $extension;
			if($file->storeAs($path, $name))
			{
				$path = '/uploads/' . $path . '/' . $name;
				return file_exists(public_path($path)) ? $path : false;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public static function deleteFile($path)
	{
		if($path)
		{
			$path = public_path($path);
			if(file_exists($path))
			{
				unlink($path);

				$folderPath = FileSystem::getOnlyPath($path);
				$name = FileSystem::getFileNameFromPath($path);
				
				if(file_exists($folderPath . '/L-' . $name))
					unlink($folderPath . '/L-' . $name);
				if(file_exists($folderPath . '/M-' . $name))
					unlink($folderPath . '/M-' . $name);

				if(file_exists($folderPath . '/S-' . $name))
					unlink($folderPath . '/S-' . $name);
				
				return true;
			}
		}

		return false;
	}

	public static function getOnlyPath($path)
	{
		$names = explode('/', $path);
		unset($names[ count($names)-1 ]);
		
		return implode('/', $names);
	}

	public static function getFileNameFromPath($path)
	{
		$names = explode('/', $path);
		return end($names);
	}
	
	public static function getExtension($value)
	{
		return pathinfo($value, PATHINFO_EXTENSION);
	}

	public static function getAllSizeImages($file)
	{
		$multiple = json_decode($file, true);
		$allFiles = $multiple && is_array($multiple) ? $multiple : ($file ? [$file] : null);		
		if($allFiles)
		{
			foreach($allFiles as $k => $a)
			{
				$name = FileSystem::getFileNameFromPath($a);
				$path = FileSystem::getOnlyPath($a);
				$allFiles[$k] = [
					'original' => $a,
					'large' => file_exists(public_path($path . '/L-' . $name)) ? $path . '/L-' . $name : "",
					'medium' => file_exists(public_path($path . '/M-' . $name)) ? $path . '/M-' . $name : "",
					'small' => file_exists(public_path($path . '/S-' . $name)) ? $path . '/S-' . $name : "",
				];
			}

			return $multiple && is_array($multiple) ? $allFiles : current($allFiles);
		}
		
		return null;
	}
}