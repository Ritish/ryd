<?php
namespace App\Libraries;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use App\Models\Admin\Settings;
use App\Models\Admin\EmailTemplates;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use App\Libraries\SendGrid;
use App\Libraries\Twillio;
use App\Mail\MyMail;
use App\Models\Admin\EmailLogs;
use Hashids\Hashids;


class General
{
	/** 
	* To make random hash string
	*/	
	public static function hash($limit = 32)
	{
		return Str::random($limit);
	}

	/** 
	* To make random hash string
	*/	
	public static function randomNumber($limit = 8)
	{
		$characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $limit; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	/** 
	* To encrypt
	*/
	public static function encrypt($string)
	{
		return Crypt::encryptString($string);
	}

	/** 
	* To decrypt
	*/
	public static function decrypt($string)
	{
		return Crypt::decryptString($string);
	}

	/** 
	* To encode
	*/
	public static function encode($string)
	{
		$hashids = new Hashids(config('app.key'), 6);
		return $hashids->encode($string);
	}

	/** 
	* To decode
	*/
	public static function decode($string)
	{
		$hashids = new Hashids(config('app.key'), 6);
		return current($hashids->decode($string));
	}

	/** 
	* Url to Anchor Tag
	* @param 
	*/
	public static function urlToAnchor($url)
	{
		return '<a href="' . $url . '" target="_blank">'.$url.'</a>';
	}

	/**
	* To validate the captcha
	* @param $token 
	**/
	public static function validateReCaptcha($token)
	{
		$data = [
			'secret' => Settings::get('recaptcha_secret'),
			'response' => $token,
			'remoteip' => $_SERVER['REMOTE_ADDR']
		];

		$response = Http::asForm()
			->post(
				'https://www.google.com/recaptcha/api/siteverify',
				$data
			);
			
		return $response->successful() && $response->json() && isset($response->json()['success']) && $response->json()['success'];
	}

	public static function sendOtp($countryCode, $phone, $otp)
	{
		$message = 'Your Ryd verification code is: ' . $otp;
		
		if($countryCode == '1')
			$send = Twillio::sendSms($countryCode, $phone, $message);
		else
			$send = true;
		
		$log = EmailLogs::create([
			'slug' => 'auth-otp-sms',
			'subject' => 'Auth OTP SMS',
			'description' => $message,
			'from' => null,
			'to' => $countryCode . $phone,
			'cc' => null,
			'bcc' => null,
			'open' => 0,
			'sent' => $send ? true : false
		]);

		return $send;
	}

	/**
	* To send template email
	**/
	public static function sendTemplateEmail($to, $template, $shortCodes = [], $attachments = [], $cc = null, $bcc = null)
	{	
		$template = EmailTemplates::getRow([
				'slug LIKE ?', [$template]
			]);

		if($template)
		{
			$shortCodes = array_merge($shortCodes, [
				'{company_name}' => Settings::get('company_name'),
				'{admin_link}' => General::urlToAnchor(url('/admin')),
				'{website_link}' => General::urlToAnchor(url('/'))
			]);
			$subject = $template->subject;
			$message = $template->description;
			$subject = str_replace (
				array_keys($shortCodes), 
				array_values($shortCodes), 
				$subject
			);

			$message = str_replace (
				array_keys($shortCodes), 
				array_values($shortCodes), 
				$message
			);

			return General::sendEmail(
				$to,
				$subject,
				$message,
				$cc,
				$bcc,
				$attachments,
				$template->slug
			);
		}
		else
		{
			throw new \Exception("Tempalte could be found.", 500);
		}
	}

	/**
	* To send email
	**/
	public static function sendEmail($to, $subject, $message, $cc = null, $bcc = null, $attachments = [], $slug = null)
	{
		$from = Settings::get('from_email');
		$emailMethod = Settings::get('email_method');
		$sent = false;

		$log = EmailLogs::create([
			'slug' => $slug,
			'subject' => $subject,
			'description' => $message,
			'from' => $from,
			'to' => $to,
			'cc' => $cc,
			'bcc' => $bcc,
			'open' => 0,
			'sent' => 0
		]);

		if($log)
		{
			if($emailMethod == 'smtp')
			{
				$company = Settings::get('company_name');

				/** OVERWRITE SMTP SETTIGS AS WE HAVE IN DB. CHECK config/mail.php **/
				$password = Settings::get('smtp_password');
				$password = $password ? General::decrypt($password) : "";
				config([
					'mail.mailers.smtp.host' => Settings::get('smtp_host'),
					'mail.mailers.smtp.port' => Settings::get('smtp_port'),
					'mail.mailers.smtp.encryption' => Settings::get('smtp_encryption'),
					'mail.mailers.smtp.username' => Settings::get('smtp_username'),
					'mail.mailers.smtp.password' => $password,
				]);
				/** OVERWRITE SMTP SETTIGS AS WE HAVE IN DB. CHECK config/mail.php **/

				$mail = Mail::mailer('smtp')
					->to($to);

				if($cc)
					$mail->cc($cc);
				if($bcc)
					$mail->bcc($bcc);
				try
				{
					$mail->send( 
						new MyMail($from, $company, $subject, $message, $attachments) 
					);
					$sent = true;
				}
				catch(\Exception $e)
				{
					$sent = false;
				}
			}
			else if($emailMethod == 'sendgrid')
			{
				$message = view(
		    		"mail", 
		    		[
		    			'content' => $message
		    		]
		    	)->render();

				$sent = SendGrid::sendEmail(
					$to,
					$subject,
					$message,
					$cc,
					$bcc,
					$attachments
				);

			}
			else
			{
				throw new \Exception("Email method does not exist.", 500);	
			}

			// Create email log
			if($sent && $log && $log->id)
			{
				$log->sent = 1;
				$log->save();
			}

			return $sent;
		}
		else
		{
			throw new \Exception("Not able to make email log.", 500);
		}
	}

	public static function validateCard($cc, $extra_check = false)
	{
	    $cards = array(
	        "visa" => "(4\d{12}(?:\d{3})?)",
	        "amex" => "(3[47]\d{13})",
	        "jcb" => "(35[2-8][89]\d\d\d{10})",
	        "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
	        "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
	        "mastercard" => "(5[1-5]\d{14})",
	        "switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
	    );
	    $names = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch");
	    $matches = array();
	    $pattern = "#^(?:".implode("|", $cards).")$#";
	    $result = preg_match($pattern, str_replace(" ", "", $cc), $matches);
	    if($extra_check && $result > 0){
	        $result = (validatecard($cc)) ? 1 : 0;
	    }
	    return ($result>0) ? $names[sizeof($matches)-2] : false;
	}

	public static function formatUSPhoneNumber($countryCode, $phoneNumber)
	{
		$data = "+{$countryCode}{$phoneNumber}";

		if(  preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', $data,  $matches ) || preg_match( '/^\+\d(\d{3})(\d{3})(\d{5})$/', $data,  $matches ) )
		{
		    $result = '(' . $matches[1] . ') ' .$matches[2] . '-' . $matches[3];
		    return $result;
		}

		return $data;

	}

	public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'M') 
	{
	  	$theta = $lon1 - $lon2;
	  	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  	$dist = acos($dist);
	  	$dist = rad2deg($dist);
	  	$miles = $dist * 60 * 1.1515;
	  	$unit = strtoupper($unit);

	  	if ($unit == "K") {
	      	return ($miles * 1.609344);
	  	} else if ($unit == "N") {
	      	return ($miles * 0.8684);
	  	} else {
	    	return $miles;
	  	}
	}
}