<?php
namespace App\Libraries;

use App\Models\Admin\Settings;
use App\Libraries\General;

class Twillio
{

	private static function getSid()
	{
		return Settings::get('twillio_sid');
	}

	private static function getToken()
	{
		return Settings::get('twillio_token');
	}

	public static function sendSms($countryCode, $phoneNumber, $message)
	{
		try
		{
			$fromNumber = Settings::get('twillio_sms_from_number');
			$client = new \Twilio\Rest\Client(self::getSid(), self::getToken());
			$message = $client->messages->create(
			  "+{$countryCode}{$phoneNumber}",
			  [
			    'from' => $fromNumber,
			    'body' => $message
			  ]
			);
			// pr([
			// 	"+{$countryCode}{$phoneNumber}",
			//   [
			//     'from' => $fromNumber,
			//     'body' => $message
			//   ]
			// ]);
			// pr($message); die;
			return $message && isset($message->sid) && $message->sid ? true  : false;
		}
		catch(\Exception $e)
		{
			return true;
		}
	}

	public static function validateNumber($countryCode, $phoneNumber)
	{
		try
		{
			$phoneNumber = General::formatUSPhoneNumber($countryCode, $phoneNumber);
			$client = new \Twilio\Rest\Client(self::getSid(), self::getToken());
			$phoneNumber = $client->lookups->v1
					->phoneNumbers("{$phoneNumber}")
	                ->fetch(["countryCode" => "US"]);
	        if($phoneNumber && $phoneNumber->phoneNumber)
	        {
				return $phoneNumber;
	        }
	        else
	        {
	        	return null;
	        }
		}
		catch(\Exception $e)
		{
			return null;
		}
	}
}