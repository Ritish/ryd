<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['guest:api'])->group(function () {
	include "API/auth.php";
	include "API/pages.php";
	include "API/customer/actions.php";
});

Route::middleware(['apiAuth'])->group(function () {	
	include "API/users.php";
	include "API/favorites.php";
	include "API/help.php";
	include "API/payments.php";
});
