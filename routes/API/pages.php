<?php
Route::get('/pages/{slug}', '\App\Http\Controllers\API\PagesController@get')
    ->name('api.pages');

Route::get('/states', '\App\Http\Controllers\API\PagesController@states')
    ->name('api.pages.states');

Route::get('/languages', '\App\Http\Controllers\API\PagesController@languages')
    ->name('api.pages.languages');

Route::get('/messages', '\App\Http\Controllers\API\PagesController@messages')
    ->name('api.pages.messages');