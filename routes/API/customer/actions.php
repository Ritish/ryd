<?php
Route::post('/customer/actions/get-ryds', '\App\Http\Controllers\API\Customer\ActionsController@getRyds')
    ->name('api.customer.getRyds');

Route::post('/driver/actions/accept-ryd', '\App\Http\Controllers\API\Customer\ActionsController@acceptRyd')
    ->name('api.customer.acceptRyd');

Route::post('/driver/actions/decline-ryd', '\App\Http\Controllers\API\Customer\ActionsController@declineRyd')
    ->name('api.customer.declineRyd');

Route::post('/customer/actions/cancel-ryd', '\App\Http\Controllers\API\Customer\ActionsController@cancelRyd')
    ->name('api.customer.cancelRyd');

Route::post('/driver/actions/reached-at-pickup', '\App\Http\Controllers\API\Customer\ActionsController@reachedAtPickup')
    ->name('api.customer.reachedAtPickup');

Route::post('/customer/actions/get-services', '\App\Http\Controllers\API\Customer\ActionsController@getServices')
    ->name('api.customer.getServices');

Route::post('/customer/actions/start-waiting', '\App\Http\Controllers\API\Customer\ActionsController@startWaiting')
    ->name('api.customer.startWaiting');

Route::post('/customer/actions/end-waiting', '\App\Http\Controllers\API\Customer\ActionsController@endWaiting')
    ->name('api.customer.endWaiting');

Route::post('/customer/actions/start-destination', '\App\Http\Controllers\API\Customer\ActionsController@startDestination')
    ->name('api.customer.startDestination');

Route::post('/customer/actions/end-destination', '\App\Http\Controllers\API\Customer\ActionsController@endDestination')
    ->name('api.customer.endDestination');

Route::post('/customer/actions/end-trip', '\App\Http\Controllers\API\Customer\ActionsController@endTrip')
    ->name('api.customer.endTrip');

Route::post('/customer/actions/start-waiting', '\App\Http\Controllers\API\Customer\ActionsController@startWaiting')
    ->name('api.customer.startWaiting');

Route::post('/customer/actions/message', '\App\Http\Controllers\API\Customer\ActionsController@message')
    ->name('api.customer.message');

