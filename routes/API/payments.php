<?php
Route::post('/payments/charge', '\App\Http\Controllers\API\PaymentsController@charge')
    ->name('api.payments.charge');

Route::get('/payments/transactions', '\App\Http\Controllers\API\PaymentsController@transactions')
    ->name('api.payments.transactions');