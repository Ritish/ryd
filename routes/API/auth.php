<?php

Route::post('/auth/signup', '\App\Http\Controllers\API\AuthController@signup')
    ->name('api.signup');

Route::post('/auth/login', '\App\Http\Controllers\API\AuthController@login')
    ->name('api.login');

Route::post('/auth/auth-state', '\App\Http\Controllers\API\AuthController@authState')
    ->name('api.authState');

Route::post('/auth/forgot-password', '\App\Http\Controllers\API\AuthController@forgotPassword')
    ->name('api.forgotPassword');

Route::post('/auth/verify-forgot-password', '\App\Http\Controllers\API\AuthController@verifyForgotPassword')
    ->name('api.forgotPassword.verify');

Route::post('/auth/email-verification/{hash}', '\App\Http\Controllers\API\AuthController@emailVerification')
    ->name('api.emailVerification');

Route::post('/auth/email-verification/{hash}/{email}', '\App\Http\Controllers\API\AuthController@emailVerification')
    ->name('api.emailVerification');

Route::post('/auth/recover-password', '\App\Http\Controllers\API\AuthController@recoverPassword')
    ->name('api.recoverPassword');

Route::post('/auth/second-auth', '\App\Http\Controllers\API\AuthController@secondAuth')
    ->name('api.secondAuth');