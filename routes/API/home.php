<?php
Route::get('/home/information', '\App\Http\Controllers\API\HomeController@information')
    ->name('api.home.information');

Route::get('/home/products', '\App\Http\Controllers\API\HomeController@products')
    ->name('api.home.products');

Route::get('/home/address', '\App\Http\Controllers\API\HomeController@address')
    ->name('api.home.address');
