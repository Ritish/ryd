<?php
Route::get('/driver/favorite-locations', '\App\Http\Controllers\API\Driver\FavoriteLocationsController@listing')
    ->name('api.favoriteLocations.listing');

Route::post('/driver/favorite-locations', '\App\Http\Controllers\API\Driver\FavoriteLocationsController@create')
    ->name('api.favoriteLocations.create');

Route::delete('/driver/favorite-locations', '\App\Http\Controllers\API\Driver\FavoriteLocationsController@delete')
    ->name('api.favoriteLocations.delete');