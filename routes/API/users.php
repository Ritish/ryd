<?php
Route::get('/profile', '\App\Http\Controllers\API\UsersController@profile')
    ->name('api.users.profile');

Route::get('/driver-stats', '\App\Http\Controllers\API\UsersController@getDriverStats')
    ->name('api.users.getDriverStats');

Route::put('/update-profile', '\App\Http\Controllers\API\UsersController@updateProfile')
    ->name('api.users.updateProfile');

Route::post('/upload-picture', '\App\Http\Controllers\API\UsersController@uploadPicture')
    ->name('api.users.uploadPicture');

Route::put('/update-email', '\App\Http\Controllers\API\UsersController@updateEmail')
    ->name('api.users.updateEmail');

Route::put('/update-phonenumber', '\App\Http\Controllers\API\UsersController@updatePhonenumber')
    ->name('api.users.updatePhonenumber');

Route::put('/change-password', '\App\Http\Controllers\API\UsersController@changePassword')
    ->name('api.users.changePassword');

Route::delete('/delete-account', '\App\Http\Controllers\API\UsersController@deleteAccount')
    ->name('api.users.deleteAccount');

Route::post('/customer/create-card', '\App\Http\Controllers\API\Customer\ProfileController@createdCard')
    ->name('api.customer.createdCard');

Route::post('/customer/unlink-card', '\App\Http\Controllers\API\Customer\ProfileController@unlinkCard')
    ->name('api.customer.unlinkCard');

Route::post('/customer/preferences', '\App\Http\Controllers\API\Customer\ProfileController@preferences')
    ->name('api.customer.preferences');

Route::post('/driver/save-address', '\App\Http\Controllers\API\Driver\ProfileController@saveMailingAddress')
    ->name('api.driver.saveMailingAddress');

Route::post('/driver/upload-documents/{slug}', '\App\Http\Controllers\API\Driver\ProfileController@uploadDocuments')
    ->name('api.driver.uploadDocuments');
