<?php
Route::get('/help/categories', '\App\Http\Controllers\API\HelpController@categories')
    ->name('api.help.categories');

Route::post('/help/request', '\App\Http\Controllers\API\HelpController@request')
    ->name('api.help.request');