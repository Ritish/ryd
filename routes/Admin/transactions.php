<?php
Route::get('/transactions', '\App\Http\Controllers\Admin\TransactionsController@index')
    ->name('admin.transactions');

Route::get('/transactions/payouts', '\App\Http\Controllers\Admin\TransactionsController@payouts')
    ->name('admin.transactions.payouts');

Route::get('/transactions/payouts/pending', '\App\Http\Controllers\Admin\TransactionsController@pendingPayouts')
    ->name('admin.transactions.pendingPayouts');

// Route::post('/transactions/bulkActions/{action}', '\App\Http\Controllers\Admin\transactionsController@bulkActions')
//     ->name('admin.transactions.bulkActions');

// Route::get('/transactions/{id}/delete', '\App\Http\Controllers\Admin\transactionsController@delete')
//     ->name('admin.transactions.delete');