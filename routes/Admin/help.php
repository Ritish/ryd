<?php
Route::get('/help/categories', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@index')
    ->name('admin.helpCategories');

Route::get('/help/categories/add', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@add')
    ->name('admin.helpCategories.add');

Route::post('/help/categories/add', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@add')
    ->name('admin.helpCategories.add');

Route::get('/help/categories/{id}/edit', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@edit')
    ->name('admin.helpCategories.edit');

Route::post('/help/categories/{id}/edit', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@edit')
    ->name('admin.helpCategories.edit');

Route::post('/help/categories/bulkActions/{action}', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@bulkActions')
    ->name('admin.helpCategories.bulkActions');

Route::get('/help/categories/{id}/delete', '\App\Http\Controllers\Admin\Help\HelpCategoriesController@delete')
    ->name('admin.helpCategories.delete');


Route::get('/help', '\App\Http\Controllers\Admin\Help\HelpController@index')
    ->name('admin.help');

Route::get('/help/add', '\App\Http\Controllers\Admin\Help\HelpController@add')
    ->name('admin.help.add');

Route::post('/help/add', '\App\Http\Controllers\Admin\Help\HelpController@add')
    ->name('admin.help.add');

Route::get('/help/{id}/edit', '\App\Http\Controllers\Admin\Help\HelpController@edit')
    ->name('admin.help.edit');

Route::get('/help/{id}/markAsResolved/{status}', '\App\Http\Controllers\Admin\Help\HelpController@markAsResolved')
    ->name('admin.help.markAsResolved');

Route::post('/help/{id}/edit', '\App\Http\Controllers\Admin\Help\HelpController@edit')
    ->name('admin.help.edit');

Route::post('/help/bulkActions/{action}', '\App\Http\Controllers\Admin\Help\HelpController@bulkActions')
    ->name('admin.help.bulkActions');

Route::get('/help/{id}/delete', '\App\Http\Controllers\Admin\Help\HelpController@delete')
    ->name('admin.help.delete');