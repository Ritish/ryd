<?php
Route::get('/trips', '\App\Http\Controllers\Admin\TripsController@index')
    ->name('admin.trips');

Route::get('/trips/export', '\App\Http\Controllers\Admin\TripsController@export')
    ->name('admin.trips.export');

Route::get('/trips/{id}/view', '\App\Http\Controllers\Admin\TripsController@view')
    ->name('admin.trips.view');

Route::post('/trips/bulkActions/{action}', '\App\Http\Controllers\Admin\TripsController@bulkActions')
    ->name('admin.trips.bulkActions');

Route::get('/trips/{id}/delete', '\App\Http\Controllers\Admin\TripsController@delete')
    ->name('admin.trips.delete');