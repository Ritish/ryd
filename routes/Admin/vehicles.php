<?php
Route::get('/vehicles', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@index')
    ->name('admin.vehicles');

// Route::get('/vehicles/add', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@add')
//     ->name('admin.vehicles.add');

// Route::post('/vehicles/add', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@add')
//     ->name('admin.vehicles.add');

Route::get('/vehicles/{id}/view', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@view')
    ->name('admin.vehicles.view');

// Route::get('/vehicles/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@edit')
//     ->name('admin.vehicles.edit');

// Route::post('/vehicles/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@edit')
//     ->name('admin.vehicles.edit');

// Route::post('/vehicles/bulkActions/{action}', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@bulkActions')
//     ->name('admin.vehicles.bulkActions');

// Route::get('/vehicles/{id}/delete', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@delete')
//     ->name('admin.vehicles.delete');

Route::get('/vehicles/{brandId}/getModelsDropdown', '\App\Http\Controllers\Admin\Vehicles\VehiclesController@getModelsDropdown')
    ->name('admin.vehicles.getModelsDropdown');

/** Brands **/
Route::get('/brands', '\App\Http\Controllers\Admin\Vehicles\BrandsController@index')
    ->name('admin.brands');

Route::get('/brands/add', '\App\Http\Controllers\Admin\Vehicles\BrandsController@add')
    ->name('admin.brands.add');

Route::post('/brands/add', '\App\Http\Controllers\Admin\Vehicles\BrandsController@add')
    ->name('admin.brands.add');

Route::get('/brands/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\BrandsController@edit')
    ->name('admin.brands.edit');

Route::post('/brands/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\BrandsController@edit')
    ->name('admin.brands.edit');

Route::post('/brands/bulkActions/{action}', '\App\Http\Controllers\Admin\Vehicles\BrandsController@bulkActions')
    ->name('admin.brands.bulkActions');

Route::get('/brands/{id}/delete', '\App\Http\Controllers\Admin\Vehicles\BrandsController@delete')
    ->name('admin.brands.delete');

/** Models **/
Route::get('/models', '\App\Http\Controllers\Admin\Vehicles\ModelsController@index')
    ->name('admin.models');

Route::get('/models/add', '\App\Http\Controllers\Admin\Vehicles\ModelsController@add')
    ->name('admin.models.add');

Route::post('/models/add', '\App\Http\Controllers\Admin\Vehicles\ModelsController@add')
    ->name('admin.models.add');

Route::get('/models/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\ModelsController@edit')
    ->name('admin.models.edit');

Route::post('/models/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\ModelsController@edit')
    ->name('admin.models.edit');

Route::post('/models/bulkActions/{action}', '\App\Http\Controllers\Admin\Vehicles\ModelsController@bulkActions')
    ->name('admin.models.bulkActions');

Route::get('/models/{id}/delete', '\App\Http\Controllers\Admin\Vehicles\ModelsController@delete')
    ->name('admin.models.delete');

/** Colors **/
Route::get('/colors', '\App\Http\Controllers\Admin\Vehicles\ColorsController@index')
    ->name('admin.colors');

Route::get('/colors/add', '\App\Http\Controllers\Admin\Vehicles\ColorsController@add')
    ->name('admin.colors.add');

Route::post('/colors/add', '\App\Http\Controllers\Admin\Vehicles\ColorsController@add')
    ->name('admin.colors.add');

Route::get('/colors/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\ColorsController@edit')
    ->name('admin.colors.edit');

Route::post('/colors/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\ColorsController@edit')
    ->name('admin.colors.edit');

Route::post('/colors/bulkActions/{action}', '\App\Http\Controllers\Admin\Vehicles\ColorsController@bulkActions')
    ->name('admin.colors.bulkActions');

Route::get('/colors/{id}/delete', '\App\Http\Controllers\Admin\Vehicles\ColorsController@delete')
    ->name('admin.colors.delete');


/** Years **/
Route::get('/years', '\App\Http\Controllers\Admin\Vehicles\YearsController@index')
    ->name('admin.years');

Route::get('/years/add', '\App\Http\Controllers\Admin\Vehicles\YearsController@add')
    ->name('admin.years.add');

Route::post('/years/add', '\App\Http\Controllers\Admin\Vehicles\YearsController@add')
    ->name('admin.years.add');

Route::get('/years/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\YearsController@edit')
    ->name('admin.years.edit');

Route::post('/years/{id}/edit', '\App\Http\Controllers\Admin\Vehicles\YearsController@edit')
    ->name('admin.years.edit');

Route::post('/years/bulkActions/{action}', '\App\Http\Controllers\Admin\Vehicles\YearsController@bulkActions')
    ->name('admin.years.bulkActions');

Route::get('/years/{id}/delete', '\App\Http\Controllers\Admin\Vehicles\YearsController@delete')
    ->name('admin.years.delete');