<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['guest'])->group(function () {

	//Admin public
	include "Admin/auth.php";

	//Public Routes
	Route::get('/', function () {
	    return redirect('/admin');
	});

	/** Stripe **/
	Route::get('/stripe/account-linking', '\App\Http\Controllers\StripeController@accountLinking')
    	->name('admin.stripe.accountLinking');

    Route::get('/stripe/link-account-success', '\App\Http\Controllers\StripeController@linkedAccountSuccess')
    ->name('admin.stripe.linkedAccountSuccess');

    Route::get('/stripe/payouts/{token}', '\App\Http\Controllers\StripeController@payouts')
    	->name('admin.stripe.payouts');
    /** Stripe **/
});

Route::prefix('admin')->middleware(['adminAuth'])->group(function () {	
	include "Admin/dashboard.php";
	include "Admin/admins.php";
	include "Admin/vehicles.php";
	include "Admin/users.php";
	include "Admin/pages.php";
	include "Admin/profile.php";
	include "Admin/emailTemplates.php";
	include "Admin/actions.php";
	include "Admin/activities.php";
	include "Admin/settings.php";
	include "Admin/help.php";
	include "Admin/trips.php";
	include "Admin/transactions.php";
});

//Frontend Routes
// Route::middleware(['frontendauth'])->group(function () {
	
// 	Auth Routes
// 	include "Frontend/auth.php";

// });
