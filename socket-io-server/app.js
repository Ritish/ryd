const express = require("express");
const app = express();
const server = require("http").createServer(app);
const socketIo = require("socket.io");
const port = 7000;
const io = socketIo(server);
const APIs = require("./services/apis");
const users = {};
const drivers = {};

const getDistanceFromLatLng = (lat1, lng1, lat2, lng2, miles)  => {
	if (typeof miles === "undefined"){miles=false;}
	function deg2rad(deg){return deg * (Math.PI/180);}
	function square(x){return Math.pow(x, 2);}
	var r=6371; // radius of the earth in km
	lat1=deg2rad(lat1);
	lat2=deg2rad(lat2);
	var lat_dif=lat2-lat1;
	var lng_dif=deg2rad(lng2-lng1);
	var a=square(Math.sin(lat_dif/2))+Math.cos(lat1)*Math.cos(lat2)*square(Math.sin(lng_dif/2));
	var d=2*r*Math.asin(Math.sqrt(a));
	if (miles){return d * 0.621371;} //return miles
	else{return d;} //return km
};

app.get('/', (req, res) => {
	console.log(req);
	res.send('<h1>Hello world</h1>');
});

io.on("connection", (socket) => {
	let loginId = socket.handshake.query.user_id;

	if(loginId)
	{
		if(socket.handshake.query.role == 'driver')
		{
			drivers[loginId] = {
				id: loginId,
				socket_id: socket.id,	
				address: socket.handshake.query.address,
				lat: socket.handshake.query.lat,
				lng: socket.handshake.query.lng,
				busy: 0
			};
		}
		else
		{
			users[loginId] = {
				id: loginId,
				socket_id: socket.id,
				lat: socket.handshake.query.lat,
				lng: socket.handshake.query.lng
			};
		}

	}
	
	console.log('drivers', drivers);
	console.log('users', users);

	socket.on("isonline", async (data) => {
		if(data.id && data.lat && data.lng)
		{
			if(typeof data.driver != undefined && data.driver)
			{
				io.emit( "isonline", Object.values(drivers));
			}
			else
			{
				let around = {};
				for(let i in drivers)
				{
					let distance = getDistanceFromLatLng(data.lat, data.lng, drivers[i].lat, drivers[i].lng, true);
					distance = distance * 1;
					if(distance > -1)
					{
						around[distance] = drivers[i];
						around[distance].distance = distance;
					}
				}
				const ordered = Object.keys(around)
				.sort((a, b) => {
					return a - b
				})
				.reduce(
					(obj, key) => { 
					obj[key] = around[key]; 
					return obj;
					}, 
					{}
				);
				io.emit( "isonline", Object.values(ordered));
			}
		}
		
	});	

	socket.on("update-location", async (data) => {
		if(data.customer_id && data.driver_id && data.type && data.lat && data.lng && data.trip_id)
		{
			drivers[data.driver_id].lat = data.lat;
			drivers[data.driver_id].lng = data.lng;
			users[data.customer_id].lat = data.lat;
			users[data.customer_id].lng = data.lng;

			io.emit( "update-location", {
				user: {lat: users[data.customer_id].lat, lng: users[data.customer_id].lng},
				driver: {lat: drivers[data.driver_id].lat, lng: drivers[data.driver_id].lng},
				trip_id: data.trip_id
			});
		}
	});

	socket.on("find-services", async (data) => {
		console.log(data);
		if(data.id && data.lat && data.lng && data.address  && data.destinations && data.destinations.length > 0)
		{
			let around = {};
			for(let i in drivers)
			{
				let d = drivers[i];
				if(!d.busy)
				{
					let distance = getDistanceFromLatLng(data.lat, data.lng, d.lat, d.lng, true);
					distance = distance * 1;
					if(distance > -1 && distance <= 40)
					{
						d.distance = distance;
						around[d.id] = d;
					}
				}
			}

			let pickup = {
				lat: data.lat,
				lng: data.lng,
				address: data.address
			};
			//console.log('req', [data.id, around, pickup], data.destinations);
			let response = await new APIs().getRydServices(data.id, around, pickup, data.destinations);
			if(response)
			{
				io.emit( "find-services", response);
			}
		}
	});

	socket.on("find-ryd", async (data) => {
		if(data.id && data.address && data.lat && data.lng && data.service_id && data.destinations && data.destinations.length > 0)
		{
			let around = {};
			for(let i in drivers)
			{
				let d = drivers[i];
				if(!d.busy)
				{
					let distance = getDistanceFromLatLng(data.lat, data.lng, d.lat, d.lng, true);
					distance = distance * 1;
					if(distance > -1 && distance <= 40)
					{
						d.distance = distance;
						around[d.id] = d;
					}
				}
			}
				
			let response = await new APIs().getRyds({
				id: data.id, 
				drivers: around, 
				service_id: data.service_id, 
				destinations: data.destinations,
				address: data.address,
				lat: data.lat,
				lng: data.lng,
			});
			if(response)
			{
				drivers[response.driver_id].busy = 1;
				io.emit( "find-ryd", response);	
			}
		}
	});

	socket.on("accept-ryd", async (data) => {
		if(data.trip_id && data.driver_id && data.customer_id)
		{
			let response = await new APIs().acceptRyd(data.trip_id);
			if(response)
			{
				io.emit( "accept-ryd", {
					trip_id: data.trip_id,
					driver: drivers[data.driver_id],
					customer: users[data.customer_id]
				});
			}
		}
	});

	socket.on("decline-ryd", async (data) => {
		if(data.trip_id && data.driver_id && data.customer_id)
		{
			let response = await new APIs().declineRyd(data.trip_id);
			if(response)
			{
				drivers[data.driver_id].busy = 0;
				io.emit( "decline-ryd", {
					trip_id: data.trip_id,
					driver: drivers[data.driver_id],
					customer: users[data.customer_id]
				});
			}
		}
	});

	socket.on("driver-outside", async (data) => {
		if(data.trip_id && data.driver_id && data.customer_id)
		{
			let response = await new APIs().reachedAtPickup(data.trip_id);
			if(response)
			{
				io.emit( "driver-outside", {
					trip_id: data.trip_id,
					driver: drivers[data.driver_id],
					customer: users[data.customer_id]
				});
			}
		}
	});

	socket.on("start-waiting", async (data) => {
		if(data.trip_id)
		{
			console.log('data', data);
			let response = await new APIs().startWaiting(data.trip_id);

			if(response)
			{
				io.emit( "start-waiting", response);
			}
		}
	});

	socket.on("end-waiting", async (data) => {
		if(data.trip_id && data.waiting_id && data.seconds)
		{
			let response = await new APIs().endWaiting(data.trip_id, data.waiting_id, data.waiting_id);

			if(response)
			{
				io.emit( "end-waiting", response);
			}
		}
	});

	socket.on("start-destination", async (data) => {
		if(data.trip_id && data.destination_id)
		{
			let response = await new APIs().startDestination(data.trip_id, data.destination_id);

			if(response)
			{
				io.emit( "start-destination", response);
			}
		}
	});

	socket.on("end-destination", async (data) => {
		if(data.trip_id && data.destination_id)
		{
			let response = await new APIs().endDestination(data.trip_id, data.destination_id);

			if(response)
			{
				io.emit( "end-destination", response);
			}
		}
	});

	socket.on("end-ryd", async (data) => {
		if(data.trip_id)
		{
			let response = await new APIs().endTrip(data.trip_id);

			if(response)
			{
				io.emit( "end-ryd", response);
			}
		}
	});

	socket.on("cancel-ryd", async (data) => {
		if(data.trip_id && data.cancel_by && data.lat && data.lng && data.location)
		{
			let response = await new APIs().cancelTrip(data.trip_id, data.cancel_by, data.lat, data.lng, data.location);
			if(response)
			{
				io.emit( "cancel-ryd", {
					trip_id: data.trip_id,
					message: "Trip cancelled"
				});
			}
		}
	});

	socket.on("message", async (data) => {
		if(data.trip_id && data.from_id && $data.to_id && $data.message)
		{
			let response = await new APIs().message(data.trip_id, data.from_id, $data.to_id, data.message);
			if(response)
			{
				io.emit( "message", response.message);
			}
		}
	});
	
	 // DISCONNECT EVENT
	socket.on('disconnect', (reason) => {
		
		for(let i in users)
		{
			if(users[i].socket_id == socket.id)
			{
				delete users[i];
				io.emit( "isoffline", Object.keys(users) );
				break;
			}
		}

		for(let i in drivers)
		{
			if(drivers[i].socket_id == socket.id)
			{
				delete drivers[i];
				io.emit( "isoffline", Object.keys(drivers) );
				break;
			}
		}
		socket.disconnect();
	});

	
	
});

server.listen(port, () => console.log("server running on port:" + port));
