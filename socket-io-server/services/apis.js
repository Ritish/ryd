const axios = require('axios');
const baseUrl = 'https://nowryd.com/public/api';

class APIs {

	async getRyds(data)
	{
		let params = {
			id: data.id, 
			drivers: data.drivers, 
			service_id: data.service_id, 
			destinations: data.destinations,
			address: data.address,
			lat: data.lat,
			lng: data.lng
		};
		
		let url = baseUrl + `/customer/actions/get-ryds`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.driver;
		}
		else
		{
			return null;
		}
	}

	async getRydServices(id, drivers, pickup, destinations)
	{
		let params = {
			id: id,
			drivers: drivers,
			pickup: pickup,
			destinations: destinations
		}
		console.log(params);
		let url = baseUrl + `/customer/actions/get-services`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.services;
		}
		else
		{
			return null;
		}
	}

	async startWaiting(tripId)
	{
		let params = {
			trip_id: tripId
		}
		
		let url = baseUrl + `/customer/actions/start-waiting`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.waiting;
		}
		else
		{
			return null;
		}
	}

	async endWaiting(tripId, waitId, seconds)
	{
		let params = {
			trip_id: tripId,
			waiting_id: waitId,
			seconds: seconds
		}
		
		let url = baseUrl + `/customer/actions/end-waiting`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.waiting;
		}
		else
		{
			return null;
		}
	}

	async startDestination(tripId, destinationId)
	{
		let params = {
			trip_id: tripId,
			destination_id: destinationId
		}
		
		let url = baseUrl + `/customer/actions/start-destination`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.destination;
		}
		else
		{
			return null;
		}
	}

	async endDestination(tripId, destinationId)
	{
		let params = {
			trip_id: tripId,
			destination_id: destinationId
		}
		
		let url = baseUrl + `/customer/actions/end-destination`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.destination;
		}
		else
		{
			return null;
		}
	}

	async endTrip(tripId)
	{
		let params = {
			trip_id: tripId
		}
		
		let url = baseUrl + `/customer/actions/end-trip`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.trip;
		}
		else
		{
			return null;
		}
	}

	async acceptRyd(tripId)
	{
		let params = {
			trip_id: tripId
		}
		
		let url = baseUrl + `/driver/actions/accept-ryd`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.trip_id;
		}
		else
		{
			return null;
		}
	}

	async declineRyd(tripId)
	{
		let params = {
			trip_id: tripId
		}
		
		let url = baseUrl + `/driver/actions/decline-ryd`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.trip_id;
		}
		else
		{
			return null;
		}
	}

	async reachedAtPickup(tripId)
	{
		let params = {
			trip_id: tripId
		}
		
		let url = baseUrl + `/driver/actions/reached-at-pickup`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.trip_id;
		}
		else
		{
			return null;
		}
	}

	async cancelTrip(tripId, cancelBy,lat, lng, location)
	{
		let params = {
			trip_id: tripId,
			cancel_by: cancelBy,
			lat: lat,
			lng: lng,
			location: location
		}
		
		let url = baseUrl + `/customer/actions/cancel-ryd`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.trip_id;
		}
		else
		{
			return null;
		}
	}

	async message(tripId, from, to, message)
	{
		let params = {
			trip_id: tripId,
			from_id: from,
			to_id: to,
			message: message
		}
		
		let url = baseUrl + `/customer/actions/message`;
		let response = await axios.post(url, params);
		if(response && response.data && response.data.status)
		{
			return response.data.message;
		}
		else
		{
			return null;
		}
	}

}

module.exports = APIs;