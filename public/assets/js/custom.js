var lang = {"field_required":"This field is required","provide_valid_email":"Please enter a valid email address","password_validation_msg":"Your password is not secure.","pwd_not_match":"Password did not match.","delete_message":"Are you sure to delete this record?","no_record_selected":"Please select atleast one record.","remove":"remove","cancel":"Cancel"};

$('.time-inteval .time').keyup(function() {
    let val  = $(this).val() * 1;
    $(this).val(val < 10 ? '0' + val : val);
});

$('.time-inteval .time').change(function() {
    let val  = $(this).val() * 1;
    $(this).val(val < 10 ? '0' + val : val);
});


var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};

rangeSlider();

$.validator.setDefaults({ 
    errorClass: 'text-danger',
    errorElement: 'small',
    errorPlacement: function(error, element) {
        inputGroup = element.parents('.form-group');
        if(inputGroup.length > 0)
        {
            inputGroup.find('small').remove();
            inputGroup.append(error);
        }
        else
        {
            element.next('small').remove();
            error.insertAfter( element );   
        }

    },
    success: function(label) {
        label.remove();
    }
});

$.validator.addMethod("pwcheck", function(value) {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(value);
});
$.extend($.validator.messages, {
    required: 'This field is required',
    email: 'Email address is invalid'
});

var urlregx = new RegExp("^[http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)]?$");

$.validator.addMethod("urlcheck", function(value) {
 if(value != ''){
    return urlregx.test(value);
}
return true;
});

$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please");

// Generate a password string
function random_string(limit){
    limit = limit > 0 ? limit : 10;
    var possible = '';
    possible += 'abcdefghijklmnopqrstuvwxyz';
    possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    possible += '0123456789';
    possible += '![]{}()%&*$#^<>~@|';

    var text = '';
    for(var i=0; i < limit; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}


function handleGenderRelations(value)
{
    if(value == 'male')
    {
        $('#female_driver_only0').prop('checked', true); 
        $('.female-services').hide(); 
        $('.male-services').show();
    }
    else
    {
        $('#female_driver_only1').prop('checked', true); 
        $('.female-services').show();
        $('.male-services').hide();
    }
}

if($('#gender').length > 0 && $('#gender').val())
{
    handleGenderRelations($('#gender').val());
}

if($('#editor1').length) {   
    init_editor('#editor1');
}

function set_notification(type, text, placementFrom, placementAlign, animateEnter, animateExit)
{

    if(type == 'success')
        var colorName = 'bg-green';
    else if(type == 'error')
        var colorName = 'bg-red';
    else
        var colorName = 'bg-black';

    if (!placementFrom) { placementFrom = 'bottom'; }
    if (!placementAlign) { placementAlign = 'right'; }
    if (!animateEnter) { animateEnter = 'animated fadeInDown'; }
    if (!animateExit) { animateExit = 'animated fadeOutUp'; }


    var allowDismiss = true;

    $.notify({
        message: text
    },
    {
        type: colorName,
        allow_dismiss: allowDismiss,
        newest_on_top: true,
        timer: 3000,
        offset: {
            "x": 30,
            "y": 50
        },
        placement: {
            from: placementFrom,
            align: placementAlign
        },
        animate: {
            enter: animateEnter,
            exit: animateExit
        },
        template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="icon"></span> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}

if($('#success_alert').length)
{
    msg = $('#success_alert').html().trim();
    set_notification('success', msg);
}
if($('#error_alert').length)
{
    msg = $('#error_alert').html().trim();
    set_notification('error', msg);
}

if($(".owl-carousel").length)
{
    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplaySpeed: 1000,
        dots: true,
        nav: false,
        navText: ['<i class="fal fa-chevron-left"></i>','<i class="fal fa-chevron-right"></i>']
    });
}
$('[required]').parents('.form-group').find('.form-control-label').append('<span class="text-danger">*</span>');

/** Select Boxes **/
$('select').selectpicker({
    liveSearch: true,
    liveSearchStyle: 'contains',
    showContent: true
})
/** Select Boxes **/

/** Password **/
$('body').on('click', '.passwordGroup .regeneratePassword', function() {
    var input = $(this).parents('.passwordGroup').find('input');
    input.val(random_string(20));
});

$('body').on('click', '.passwordGroup .viewPassword', function() {
    var input = $(this).parents('.passwordGroup').find('input');
    if(input.attr('type') == 'password')
    {
        input.attr('type', 'text');
        $(this).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
    }
    else
    {
        input.attr('type', 'password');   
        $(this).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }
});

$('#sendPasswordEmail').on('change', function(){
    if($(this).is(':checked'))
    {
        $('.passwordGroup input').removeAttr('required');
        $('.passwordGroup').addClass('d-none');
    }
    else
    {
        $('.passwordGroup input').attr('required', 'required');
        $('.passwordGroup').removeClass('d-none');   
    }
});
/** Password **/

/** Filter Dropdown **/
$('body').on('click', '.filter-dropdown .dropdown-btn', function() {
    $(this).next().toggle();
});
$('body').on('click', '.filter-dropdown .closeit', function() {
    $(this).parents('.dropdown-menu').hide();
});
/** Filter Dropdown **/

$('body').on('click', '.row-edit', function(){

    tr = $(this).parents('tr');
    tr.find('.e-title').toggle();
    tr.find('.e-edit').toggle();
});
$('body').on('click', '.e-edit button', function(){
    tr = $(this).parents('tr');
    edit = $(this).parents('.e-edit');
    $.ajax({
        url: edit.attr('action'),
        type: 'post',
        data: {title: edit.find('input').val()},
        success: function(resp)
        {
            if(resp)
            {
                resp = JSON.parse(resp);
                tr.find('.e-title').html(resp.title);
                tr.find('.e-title').show();
                tr.find('.e-edit').hide();
            }
        }
    });
});

function bulk_actions(url, action)
{
    if($('table.listing-table').find('.listing_check:checked').length > 0)
    {
        if(action == 'delete')
            var confirmation = lang.delete_message;
        else
            var confirmation = 'Are you sure to perform this action?';

        if(confirm(confirmation))
        {
            ids = [];
            $('table.listing-table').find('.listing_check').each(function(){
                if($(this).is(':checked'))
                    ids.push($(this).val());
            });
            if(ids.length > 0)
            {
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {
                        ids : ids,
                        _token: csrf_token()
                    },
                    success: function(resp){
                        if(resp.status == 'success')
                        {
                            window.location.reload();
                        }
                        else
                        {
                            set_notification('error', resp.message);
                        }
                    }
                })
            }
            else
            {
                set_notification('error', lang.no_record_selected);
            }
        }
    }
    else
    {
        set_notification('error', lang.no_record_selected);
    }
}

function switch_action(url, that)
{
    $.ajax({
        url: url,
        type: 'post',
        data: {
            flag: $(that).is(':checked') ? 1 : 0,
            _token: csrf_token()
        },
        success: function(resp){

        }
    })
}

function updateSelectOptions(select, options, value)
{
    html = '<option value=""></option>';
    for(i in options)
    {
        html += '<option'+(value && value == options[i].id ? ' selected' : '')+' value="'+options[i].id+'">'+options[i].title+'</option>';
    }
    select.html(html);
    select.selectpicker('refresh');
}

function save_product_category(that)
{

    if(!$(that).is(':disabled'))
    {
        box = $(that).parents('.category-group');
        buttons = $(that).parent().children();
        input = $(that).parents('.category-add').find('input[type=text]');
        title = input.val();
        select = box.find('.category-select select');

        buttons.prop('disabled', true);
        $.ajax({
            url: site_url + 'products/add_category',
            data: {title: title},
            type: 'post',
            success: function(resp){
                if(resp)
                {
                    updateSelectOptions(select, resp.categories, resp.insert_id);
                    toggleCategoryInline(box);
                    set_notification('success', resp.message);
                }
                else
                {
                    set_notification('error', resp.message);
                }
            }
        })
    }
}
function toggleCategoryInline(categoryGroup)
{
    categoryGroup.find('.category-add input').val('');
    categoryGroup.find('.category-add button').prop('disabled', false);
    categoryGroup.find('.category-select').toggleClass('hidden');
    categoryGroup.find('.category-add').toggleClass('hidden');
    if(!categoryGroup.find('.category-add').hasClass('hidden')) {
        categoryGroup.find('.category-add input').focus();
    }
    else {
        categoryGroup.find('.category-select select').focus();
    }
}

function render_add_edit_item_html(data)
{
    html = '';
    html += '<tr class="item-row" data-id="'+data.product.id+'">'+
    '<td width="5%"><i class="material-icons move_lines">drag_indicator</i><input type="hidden" name="data['+data.product.id+'][product_id]" value="'+data.product.id+'"></td>'+
    '<td width="45%"><p>'+data.product.title+'<input type="hidden" name="data['+data.product.id+'][subject]" value="'+data.product.title+'"></p><p><small>'+data.product.description+'</small><input type="hidden" name="data['+data.product.id+'][description]" value="'+data.product.description+'"></p></td>'+
    '<td width="20%"><div class="input_n_dropdown"><input type="number" class="form-control amount" name="data['+data.product.id+'][amount]" value="1.00"><div class="btn-group-vertical"><input type="hidden" class="form-control unit" name="data['+data.product.id+'][unit]" value="'+data.product.measurement_unit+'"><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'+data.product.measurement_unit+'<span class="caret"></span></button><ul class="dropdown-menu">';
    for(k in data.measurements){
        html += '<li><a href="javascript:;" data-value="'+data.measurements[k].unit+'">'+data.measurements[k].name + '(' + data.measurements[k].unit + ')'+'</a></li>'
    }
    html += '</ul></div></div></div></td><td width="15%"><input type="number" class="form-control price" name="data['+data.product.id+'][price]" placeholder="'+data.currency_symbol+'" value="'+data.product.price+'"></td>'+
    '<td class="text-center"  width="10%">'+data.currency_symbol+' <span class="row_total">0</span></td><th width="5%" class="text-right" data-toggle="tooltip" data-title="'+lang.remove+'"><a href="javascript:;" onclick="remove_add_edit_entry(this)" class="col-pink" ><i class="material-icons">cancel</i></a></th></tr>';
    return html;
}

var itemSelectReq;
function make_add_edit_item_entry(id)
{
    if(itemSelectReq && itemSelectReq.readyState != 4)
        itemSelectReq.abort();

    itemSelectReq = $.ajax({
        url: site_url + 'products/get_details/' + id,
        success: function(resp){
            $('._add_edit_items table tbody tr.empty').remove();
            html = render_add_edit_item_html(resp);
            $('._add_edit_items table tbody').prepend(html);
            $('#item_select').selectpicker('val', '');
            $('._add_edit_items table tbody [data-toggle=tooltip]').tooltip();
            calculate_total();
        }
    });
}

function remove_add_edit_entry(that)
{
    $(that).parents('.item-row').remove();
}

function calculate_total()
{
    var row_total;
    $('._add_edit_items table tbody tr.item-row').each(function(){
        amount = $(this).find('.amount').val() * 1;
        price = $(this).find('.price').val() * 1;
        $(this).find('.row_total').text(amount * price);
        row_total += amount * price;
    });
}

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$(function () {
    $('[data-toggle=tooltip]').tooltip();
    $('.form-validation').each(function(){
        $(this).validate({
            ignore: "[type=hidden]"
        });
    });
    $('#user-information-form').validate({
        ignore: "[type=hidden]",
        rules: {
            long_trips_min: {
                max: function () {
                    return parseInt($('#long_trips_max').val()-1);
                }
            },
            tag_number: {
                alphanumeric: true
            }
        }
    });
    
    $('#sign_in').validate();
    $('.add_product_form').validate();
    $('#forgot_password').validate();
    $('#site_settings').validate({
     rules: {
        "stripe_key": {
            required: true
        },
        "stripe_public_key": {
            required: true
        },
        "service_charge_markup":{
            required: true
        }
    }

});


    $('#isAdmin').on('click', function(){
        if($(this).is(':checked'))
            $('#permissionTable').addClass('d-none')
        else
            $('#permissionTable').removeClass('d-none')
    })
    $('#recover-password').validate({
        rules: {
            "new_password": {
                required: true,
                pwcheck: true
            },
            confirm_password : {
                equalTo : "[name=new_password]"
            }
        },
        messages: {
            'new_password' : {
                pwcheck: lang.password_validation_msg
            },
            'confirm_password' : {
                equalTo: lang.pwd_not_match
            }
        }
    });
    $('#edit_profile').validate();   
    $("#change_password").validate({
        rules: {
            "new_password": {
                required: true,
                pwcheck: true
            },
            confirm_password : {
                equalTo : "[name=new_password]"
            }
        },
        messages: {
            'new_password' : {
                pwcheck: lang.password_validation_msg
            },
            'confirm_password' : {
                equalTo: lang.pwd_not_match
            }
        }
    }); 
    

    $('#add_staff').validate({
        rules: {
            password: {
                pwcheck: true
            }
        },
        messages: {
            password : {
                pwcheck: lang.password_validation_msg
            }
        }
    });
    $('#reset_password').validate({
        rules: {
            password: {
                pwcheck: true
            }
        },
        messages: {
            password : {
                pwcheck: lang.password_validation_msg
            }
        }
    });

    $('body').on('click', '.listing-table .mark_all', function(){
        $('.listing-table .listing_check').prop('checked', $(this).is(':checked'));
    });
    $('body').on('click', '._delete', function(){
        if(!confirm(lang.delete_message))
            return false;
        window.location.href = $(this).attr('data-link');
    });

    // Enable in case ajax paniations working
    if($('.listing-table .ajaxPaginationEnabled').length > 0)
    {
        var tableReq;

        function get_table_listing(table, data)
        {
            if(!table.hasClass('processing'))
            {
                url = table.find('.loader').attr('data-url');
                page = table.find('.loader').attr('data-page');
                if(page != "")
                {
                    table.find('.loader').removeClass('hidden');
                    table.addClass('processing');
                    next_page = (page*1+1);
                    
                    search = table.parents('.listing-block').find('.listing-search').val();
                    sort = table.find('thead i.active').length ? table.find('thead i.active').attr('data-field') : '';
                    direction = table.find('thead i.active').length ? table.find('thead i.active').attr('data-sort') : '';

                    filters = $('#filters-form').length ? $('#filters-form').serialize() : '';

                    data = 'page=' + next_page + '&sort=' + sort + '&direction=' + direction + '&search=' + search + (filters ? '&' + filters : '');
                    if(tableReq && tableReq.readyState != 4)
                    {
                        tableReq.abort();
                    }
                    tableReq = $.ajax({
                        url: url,
                        data: data,
                        success: function(resp){
                            table.find('tbody').append(resp.html);
                            table.find('.loader').attr('data-page', resp.page);
                            table.find('.loader').attr('data-counter', resp.counter);
                            table.find('.loader').attr('data-total', resp.count);
                            if(resp.pagination_counter >= resp.count)
                            {
                                table.find('.loader').addClass('hidden');
                                table.find('.loader').attr('data-page', '');
                            }
                            table.removeClass('processing');
                        }
                    });
                }
            }
        }


        $(window).scroll(function(){
           if( $(this).scrollTop() > ($(document).height()-$(window).height() - 50) )
           {
            table = $('.listing-table');
            get_table_listing(table);
        }
    });
    }

    //Sorting Tables
    $('.listing-table thead th:not(:first-child)').on('click', function(){
        field = $(this).find('i').attr('data-field');
        sort = $(this).find('i').attr('data-sort');
        direction =  sort && sort == 'asc' ? 'desc' : 'asc';
        icon = sort && sort == 'asc' ? 'fa-sort-up' : 'fa-sort-down';

        if($('.listing-table .ajaxPaginationEnabled').length > 0 && $(this).find('i').length > 0)
        {
            // ajax pagination table  case
            $(this).parents('thead').find('i').removeClass('active').removeClass('fa-sort').removeClass('fa-sort-up').removeClass('fa-sort-down');
            $(this).parents('thead').find('i').addClass('fa-sort');
            $(this).find('i').attr('data-sort', direction);
            $(this).find('i').addClass('active');
            $(this).find('i').addClass(icon);
            loader = $(this).parents('table').find('tfoot .loader')
            loader.attr('data-page', 0);
            loader.removeClass('hidden');

            table = $(this).parents('table');
            table.find('tbody').html('');
            get_table_listing(table);
        }
        else if($(this).find('i').length > 0)
        {
            // refresh pagination table  case
            search = $(this).parents('.listing-block').find('.listing-search').val();
            window.location.href = current_url + "?search=" + search + '&sort=' + field + '&direction=' + direction;
        }
    });

    $('body').on('keyup', '.listing-search', function(event){
        if($('.listing-table .ajaxPaginationEnabled').length > 0)
        {
            table = $(this).parents('.listing-block').find('.listing-table');
            loader = table.find('tfoot .loader')
            loader.attr('data-page', 0);
            loader.removeClass('hidden');
            table.removeClass('processing');
            table.find('tbody').html('');
            get_table_listing(table);
        }
        else if(event.which === 13) 
        {
            // refresh pagination table  case
            search = $(this).val();
            window.location.href = current_url + "?search=" + search;
        }
    });

    $('body').on('click', '.add_category_inline, .cancel_category_inline', function(){
        selectbox = $(this).parents('.category-group');
        toggleCategoryInline(selectbox);
    });

    // __add_edit_items functions
    $('body').on('submit', '#add_product .add_product_form', function(){
        button = $(this).find('.modal-footer button');
        button.prop('disabled', true);
        $.ajax({
            url: site_url + 'products/add',
            type: 'post',
            data: $(this).serialize(),
            success: function(resp){
                button.prop('disabled', false);
                if($('#item_select').length)
                {
                    $('.add_item_dropdown').html(resp.products_view);
                    $('#item_select').selectpicker();
                    make_add_edit_item_entry(resp.insert_id)
                }
            }
        });
        return false;
    });

    $('body').on('changed.bs.select', '#item_select', function(){
        make_add_edit_item_entry($(this).val());
    });
    $('body').on('click', '.input_n_dropdown .dropdown-menu li a', function(){
        value = $(this).attr('data-value');
        block = $(this).parents('.btn-group-vertical');
        block.find('.dropdown-toggle').html(value + ' <span class="caret"></span>');
        block.find('input').val(value).change();
    });
    $('body').on('keyup change', '._add_edit_items table tbody tr.item-row .amount, ._add_edit_items table tbody tr.item-row .unit, ._add_edit_items table tbody tr.item-row .price', function(){
        calculate_total();
    });
});

if($('#dropzone').length)
{
    Dropzone.autoDiscover = false;
    url = $('#dropzone').attr('data-url');
    var myDropzone = new Dropzone("div#dropzone", { url: url});
    console.log(myDropzone);
    myDropzone.on("complete", function(file) {
      myDropzone.removeFile(file);
  });
}

$('.delete_product_image').on('click', function(){
    if(confirm('Are you sure to delete this image ?'))
    {
        id = $(this).attr('data-id');
        that = $(this);
        $.ajax({
            url: site_url + 'products/delete_product_image/' + id,
            type: 'post',
            success: function(resp){
                if(resp.status == 'success')
                { 
                    that.closest('div').remove();
                    set_notification('success', resp.message);
                }
                else
                {
                    set_notification('error', resp.message);   
                }
            }
        });
    }
});

var stateAjax;
$('#state_id').on('change', function(){
    if(stateAjax && stateAjax.readyState != 4)
    {
        stateAjax.abort();
    }
    if($(this).val() && $('select#city_id').length)
    {
        stateAjax = $.ajax({
            url: site_url + 'admin/states/cities_dropdown/' + $(this).val(),
            success: function(resp) {
                $('select#city_id').html(resp);
                $('select#city_id').selectpicker('refresh');
            }
        });
    }
    else
    {
        $('select#city_id').html('');
    }
});

$('body').on('change','#profile_img', function (e) {

    if($(this).attr('data-id') && $(this).attr('data-url'))
    {
        $('#loading-image').show();
        var fd = new FormData();
        var files = $('#profile_img')[0].files[0];
        fd.append('file', files);
        fd.append('_token', csrf_token());
        fd.append('id', $(this).attr('data-id'));

        $.ajax({
            url: $(this).attr('data-url'),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function ( data ) {
                if(data.status == 'success' && data.picture != "")
                {
                    $('.prof_image_sidebar img').attr('src', data.picture);
                    set_notification('success', data.message);
                }
                else
                {
                    set_notification('error', data.message);
                }
            },
            complete: function(){           
                $("#loading-image").hide();
                $('#profile_img').val(' ');   
            }
        });
    }
});

/** Upload File Script **/
$('body').on('click', '.upload-image-section .button-ref button', function(){
    var that = $(this);
    var parent = that.parents('.upload-image-section');
    var uploadSection = parent.find('.upload-section');
    var textArea = parent.find('textarea');
    var showSection = parent.find('.show-section');
    var fixedEditSection = parent.find('.fixed-edit-section');
    var progerssBar = parent.find('.progress-bar');
    var isMultiple = parent.attr('data-multiple') == 'true' ? true : false;
    var fileType = parent.attr('data-type');
    var path = parent.attr('data-path');
    var resizeLarge = parent.attr('data-resize-large');
    resizeLarge = resizeLarge ? resizeLarge : "";
    var resizeMedium = parent.attr('data-resize-medium');
    resizeMedium = resizeMedium ? resizeMedium : "";
    var resizeSmall = parent.attr('data-resize-small');
    resizeSmall = resizeSmall ? resizeSmall : "";
    
    if(fileType && path)
    {
        parent.find('input[type=hidden]').val('');
        var form = $('#fileUploadForm');
        form.find('input[name=file_type]').val(fileType);
        form.find('input[name=path]').val(path);
        form.find('input[name=resize_large]').val(resizeLarge);
        form.find('input[name=resize_medium]').val(resizeMedium);
        form.find('input[name=resize_small]').val(resizeSmall);
    
        $('#fileUploadForm input[type=file]').val('');
        $('#fileUploadForm input').click();
        
        $('#fileUploadForm input').unbind('change');
        
        $('#fileUploadForm input').on('change', function() {
            $('#fileUploadForm').ajaxSubmit({
                beforeSend: function() {
                    progerssBar.parent().removeClass('d-none');
                    progerssBar.css('width', '0');
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    progerssBar.css('width', percentComplete + '%');
                },
                success: function(response) {
                    if(response.status == 'success')
                    {
                        if(!isMultiple)
                        {
                            if(fileType == 'image')
                                showSection.html('<div class="single-image"><a href="javascript:;" class="fileRemover single-cross image" data-path="'+response.path+'"><i class="fas fa-times"></i></a><img src="'+response.url+'"></div>');
                            else
                                showSection.html('<div class="single-file"><a href="'+site_url + response.path +'" target="_blank"><i class="fas fa-file"></i>'+response.name+'</a><a href="javascript:; file" class="fileRemover single-cross file" data-path="'+response.path+'"><i class="fas fa-times-circle"></i></a></div>');
                            uploadSection.addClass('d-none');
                            fixedEditSection.addClass('d-none');
                        }
                        else
                        {
                            if(fileType == 'image')
                                showSection.prepend('<div class="single-image"><a href="javascript:;" class="fileRemover single-cross image" data-path="'+response.path+'"><i class="fas fa-times"></i></a><img src="'+response.url+'"></div>');
                            else
                                showSection.prepend('<div class="single-file"><a href="'+site_url + response.path +'" target="_blank"><i class="fas fa-file"></i>'+response.name+'</a><a href="javascript:; file" class="fileRemover single-cross file" data-path="'+response.path+'"><i class="fas fa-times-circle"></i></a></div>');
                        }
                        showSection.removeClass('d-none');
                        updateFileValues(textArea, fileType, isMultiple);
                    }
                    else
                    {
                        set_notification('error', response.message);
                    }
                },
                complete: function() {
                    progerssBar.css('width', '100%');
                }
            });
        });
    }
    else
    {
        set_notification('error', 'File path and type are missing.');
    }
});

$('body').on('click', '.upload-image-section .fileRemover', function() {
    var parent = $(this).parents('.upload-image-section');
    var uploadSection = parent.find('.upload-section');
    var showSection = parent.find('.show-section');
    var progerssBar = parent.find('.progress-bar');
    if(confirm("Are you sure to delete this file ?"))
    {
        var relation = $(this).attr('data-relation') ? $(this).attr('data-relation') : null;
        var id = $(this).attr('data-id') ? $(this).attr('data-id') : null;
        var isMultiple = !$(this).hasClass('single-cross');
        var that = $(this);
        var path = $(this).attr('data-path');
        $.ajax({
            url: admin_url + '/actions/removeFile',
            type: "post",
            data: {
                "_token": csrf_token(),
                "file": path,
                "relation": relation,
                "id": id
            },
            success: function(resp) {
                that.parent().remove();
                if(!isMultiple)
                {
                    uploadSection.removeClass('d-none');
                    showSection.addClass('d-none');
                    progerssBar.css('width', 0);
                }
            }
        });
    }
});

function updateFileValues(textArea, fileType, isMultiple)
{
    if(isMultiple)
    {
        files = [];
        textArea.next('.show-section').find('.fileRemover').each(function() {
            var file = $(this).attr('data-path');
            files.push(file);
        });
        textArea.val(files.length > 0 ? JSON.stringify(files) : "");
    }
    else
    {
        textArea.val(textArea.next('.show-section').find('.fileRemover').attr('data-path'));
    }
}

/** Upload File Script **/

$('.edit-document').on('click', function() {
    $('#modal-edit-documents form').attr('action', $(this).attr('data-href'));
    $('#modal-edit-documents .modal-title').html($(this).attr('data-name'));
    $('#modal-edit-documents input[name="expiry_date"]').val($(this).attr('data-date'));
    $('#modal-edit-documents input[type="checkbox"]').prop('checked', $(this).attr('data-verify') == '1 ' ? true : false);
    $('#modal-edit-documents').modal('show');
});

$('#brands').on('change', function() {
    var id = $(this).val();
    var modelId = $(this).attr('data-model');
    modelId = modelId ? modelId : null;
    if(id)
    {
        $.ajax({
            url: site_url + '/admin/vehicles/'+id+'/getModelsDropdown',
            success: function(resp)
            {
                if(resp.html)
                {
                    $('#models').html(resp.html);
                    $('#models').selectpicker('refresh');
                    if(modelId)
                    {
                        $('#models').selectpicker('val', modelId);
                    }
                }
            }
        });
    }
    else
    {
        $('#models').html('<option value="">Please select vehicle brand.</option>');
        $('#models').selectpicker('refresh');
    }
})

$('#modal-brands form').on('submit', function() {
    if($(this).valid())
    {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: $(this).serialize(),
            success: function(resp) {
                if(resp && resp.status == 'success' && resp.brand)
                {
                    $('#brands').append('<option value="'+resp.brand.id+'" selected>'+resp.brand.title+'</option>');
                    $('#brands').selectpicker('refresh');
                    $('#brands').selectpicker('val', resp.brand.id);
                    $('#modal-brands').modal('hide');
                }
                else
                {
                    set_notification('error', resp.message);
                }
            }
        });
    }
    return false;
});

$('#modal-models form').on('submit', function() {
    if($(this).valid())
    {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: $(this).serialize(),
            success: function(resp) {
                if(resp && resp.status == 'success' && resp.model)
                {
                    $('#brands').attr('data-model', resp.model.id);
                    $('#brands').selectpicker('val', resp.model.brand_id).change();
                    $('#modal-models').modal('hide');
                }
                else
                {
                    set_notification('error', resp.message);
                }
            }
        });
    }
    return false;
});

$('#modal-years form').on('submit', function() {
    if($(this).valid())
    {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: $(this).serialize(),
            success: function(resp) {
                if(resp && resp.status == 'success' && resp.year)
                {
                    $('#years').append('<option value="'+resp.year.year+'" selected>'+resp.year.year+'</option>');
                    $('#years').selectpicker('refresh');
                    $('#years').selectpicker('val', resp.year.year);
                    $('#modal-years').modal('hide');
                }
                else
                {
                    set_notification('error', resp.message);
                }
            }
        });
    }
    return false;
});

$('#modal-colors form').on('submit', function() {
    if($(this).valid())
    {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: $(this).serialize(),
            success: function(resp) {
                if(resp && resp.status == 'success' && resp.color)
                {
                    $('#colors').append('<option value="'+resp.color.id+'" selected>'+resp.color.title+'</option>');
                    $('#colors').selectpicker('refresh');
                    $('#colors').selectpicker('val', resp.color.id);
                    $('#modal-colors').modal('hide');
                }
                else
                {
                    set_notification('error', resp.message);
                }
            }
        });
    }
    return false;
});