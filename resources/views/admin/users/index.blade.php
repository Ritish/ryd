@extends('layouts.adminlayout')
@section('content')
	<div class="header bg-primary pb-6">
		<div class="container-fluid">
			<div class="header-body">
				<div class="row align-items-center py-4">
					<div class="col-lg-6 col-7">
						<h6 class="h2 text-white d-inline-block mb-0">Manage <?php echo isset($_GET['role']) && $_GET['role'] == 'driver' ? 'Drivers' : 'Customers' ?></h6>
					</div>
					<div class="col-lg-6 col-5 text-right">
						@include('admin.users.filters')
					</div>
				</div>
			</div> 
		</div>
	</div>
	<!-- Page content -->
	<div class="container-fluid mt--6">
		<div class="row">
			<div class="col">
<!--!!!!! DO NOT REMOVE listing-block CLASS. INCLUDE THIS IN PARENT DIV OF TABLE ON LISTING PAGES !!!!!-->
				<div class="card listing-block">
					<!--!! FLAST MESSAGES !!-->
					@include('admin.partials.flash_messages')
					<!-- Card header -->
					<div class="card-header border-0">
						<div class="heading">
							<?php if(isset($_GET['role']) && $_GET['role'] == 'driver'): ?>
								<div class="nav-wrapper p-0">
								    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
								        <li class="nav-item">
								            <a class="nav-link mb-sm-3 mb-md-0<?php echo !isset($_GET['verified']) || $_GET['verified'] === '' ? ' active' : '' ?>" href="<?php echo route('admin.users') . '?role=driver&verified='?>"><i class="fa fa-list mr-2"></i>All ({{ $counts['all'] }})</a>
								        </li>
								        <li class="nav-item">
								            <a class="nav-link mb-sm-3 mb-md-0<?php echo isset($_GET['verified']) && $_GET['verified'] === 'no' ? ' active' : '' ?>" href="<?php echo route('admin.users') . '?role=driver&verified=no'?>"><i class="fa fa-exclamation-triangle mr-2"></i>Pending ({{ $counts['pending'] }})</a>
								        </li>
								        <li class="nav-item">
								            <a class="nav-link mb-sm-3 mb-md-0<?php echo isset($_GET['verified']) && $_GET['verified'] === 'yes' ? ' active' : '' ?>"  href="<?php echo route('admin.users') . '?role=driver&verified=yes'?>"><i class="fa fa-check-square mr-2"></i>Verified ({{ $counts['verified'] }})</a>
								        </li>
								    </ul>
								</div>
							<?php endif; ?>
						</div>
						<div class="actions">
							<div class="input-group input-group-alternative input-group-merge">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-search"></i></span>
								</div>
								<input class="form-control listing-search" placeholder="Search" type="text" value="<?php echo (isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '') ?>">
							</div>
							<?php if(Permissions::hasPermission('users', 'update') || Permissions::hasPermission('users', 'delete')): ?>
							<div class="dropdown" data-toggle="tooltip" data-title="Bulk Actions">
								<a class="btn btn-sm btn-icon-only text-warning" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fas fa-ellipsis-v"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
									<?php if(Permissions::hasPermission('users', 'update')): ?>
										<a 
											class="dropdown-item" 
											href="javascript:;"
											onclick="bulk_actions('<?php echo route('admin.users.bulkActions', ['action' => 'active']) ?>', 'active');"
										>
											<span class="badge badge-dot mr-4">
												<i class="bg-success"></i>
												<span class="status">Active</span>
											</span>
										</a>
										<a 
											class="dropdown-item" 
											href="javascript:;"
											onclick="bulk_actions('<?php echo route('admin.users.bulkActions', ['action' => 'inactive']) ?>', 'inactive');"
										>
											<span class="badge badge-dot mr-4">
												<i class="bg-warning"></i>
												<span class="status">Inactive</span>
											</span>
										</a>
										<?php if(isset($_GET['role']) && $_GET['role'] == 'driver' && (!isset($_GET['verified']) || !$_GET['verified'] || $_GET['verified'] == 'no') ): ?>
											<a 
												class="dropdown-item" 
												href="javascript:;"
												onclick="bulk_actions('<?php echo route('admin.users.bulkActions', ['action' => 'verified']) ?>', '1');"
											>
												<i class="fa fa-check text-green"></i> 
												<span class="status">Verified</span>
											</a>	
										<?php endif; ?>
									<?php endif; ?>
									<?php if(Permissions::hasPermission('users', 'delete')): ?>
									<div class="dropdown-divider"></div>
		                            <a 
		                            	href="javascript:void(0);" 
		                            	class="waves-effect waves-block dropdown-item text-danger" 
		                            	onclick="bulk_actions('<?php echo route('admin.users.bulkActions', ['action' => 'delete']) ?>', 'delete');">
											<i class="fas fa-times text-danger"></i>
											<span class="status text-danger">Delete</span>
		                            </a>
		                            <?php endif; ?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="table-responsive">

<!--!!!!! DO NOT REMOVE listing-table, mark_all  CLASSES. INCLUDE THIS IN ALL TABLES LISTING PAGES !!!!!-->
						<table class="table align-items-center table-flush listing-table">
							<thead class="thead-light">
								<tr>
									<th class="checkbox-th" width="5%">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input mark_all" id="mark_all">
											<label class="custom-control-label" for="mark_all"></label>
										</div>
									</th>
									<th class="sort" width="5%">
										<!--- MAKE SURE TO USE PROPOER FIELD IN data-field AND PROPOER DIRECTION IN data-sort -->
										Id
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.id' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.id" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.id' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.id" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.id" data-sort="asc"></i>
										<?php endif; ?>
									</th>
									<th class="sort" width="15%">
										Name
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.name"></i>
										<?php endif; ?>
									</th>
									<th class="sort" width="15%">
										Email
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.email' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.email" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.email' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.email" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.email"></i>
										<?php endif; ?>
									</th>
									<th class="sort" width="15%">
										Phone Number
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.phonenumber' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.phonenumber" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.phonenumber' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.phonenumber" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.phonenumber"></i>
										<?php endif; ?>
									</th>
									<?php if(isset($_GET['role']) && $_GET['role'] == 'driver'): ?>
									<th class="sort">
										Verified
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.is_verified' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.is_verified" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.is_verified' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.is_verified" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.is_verified"></i>
										<?php endif; ?>
									</th>
									<?php else: ?>
									<th class="sort">
										Last Login
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.last_login' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.last_login" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.last_login' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.last_login" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.last_login"></i>
										<?php endif; ?>
									</th>
									<?php endif; ?>
									<th class="sort">
										Status
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.status' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.status" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.status' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.status" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.status"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Created ON
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.created' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.created" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.created' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.created" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.created"></i>
										<?php endif; ?>
									</th>
									<th>
										Actions
									</th>
								</tr>
							</thead>
							<tbody class="list">
								<?php if(!empty($listing->items())): ?>
									@include('admin.users.listingLoop')
								<?php else: ?>
									<td align="left" colspan="7">
		                            	No records found!
		                            </td>
								<?php endif; ?>
							</tbody>
							<tfoot>
		                        <tr>
		                            <th align="left" colspan="20">
		                            	@include('admin.partials.pagination', ["pagination" => $listing])
		                            </th>
		                        </tr>
		                    </tfoot>
						</table>
					</div>
					<!-- Card footer -->
				</div>
			</div>
		</div>
	</div>
@endsection