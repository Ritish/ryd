<div class="card">
	<div class="card-header border-0">
		<div class="row align-items-center">
			<div class="col">
				<h3 class="mb-0">Documents</h3>
			</div>
			<div class="col text-right">
				<a href="#!" data-toggle="modal" data-target="#modal-documents" class="btn btn-sm py-2 px-3 btn-primary"><i class="fa fa-plus"></i> Add Document</a>
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<!-- Projects table -->
		<table class="table align-items-center table-flush listing-table">
			<thead class="thead-light">
				<tr>
					<th class="checkbox-th" width="15%">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input mark_all" id="mark_all">
							<label class="custom-control-label" for="mark_all"></label>
						</div>
					</th>
					<th scope="col">Title</th>
					<th scope="col">Expire On</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$allVerified = 0;
				$documents = $user->documents();
				if($documents->count() > 0): ?>
					<?php 
					foreach($user->documents as $k => $d): 
						if($d->verified) 
							$allVerified++;
					?>
						<tr>
							<td>
								<?php if($d->verified): ?>
									<div 
										class="custom-control custom-checkbox"
										data-toggle="tooltip"
										data-title="Verified"
									>
										<i class="fa fa-check text-success"></i>
									</div>
								<?php else: ?>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input listing_check" id="listing_check<?php echo $k ?>" value="<?php echo $d->id ?>">
										<label class="custom-control-label" for="listing_check<?php echo $k ?>"></label>
									</div>
								<?php endif; ?>
							</td>
							<th scope="row">
								<?php echo $d->title ?><br>
								<?php 
									$doc = "";
									if($d->file && file_exists(public_path($d->file))):
										$doc = '<a href="'. url($d->file). '" target="_blank"><i class="fa fa-download"></i> View Document</a>';
									else:
										$doc =  '<small class="text-danger">Document unavailable.</small>';
									endif; 
								?>
							</th>
							<td>
								<?php echo $d->expiry_date ? _d($d->expiry_date) : null; ?>
							</td>
							<th scope="row">
								<a
									href="javascript:;"
									data-href="<?php echo route('admin.users.editDocument', ['id' => $d->id]) ?>"
									class="edit-document"
									data-id="<?php echo $d->id ?>"
									data-name="<?php echo str_replace([' - Front', ' - Back'], '', $d->title) ?>"	
									data-date="<?php echo $d->expiry_date ?>"
									data-verify="<?php echo $d->verified ?>"
									data-toggle="tooltip"
									data-title="Edit"
								>
									<i class="fas fa-edit"></i>
								</a>
								<a 
									class="_delete" 
									href="javascript:;"
									data-link="<?php echo route('admin.users.documentsDelete', ['id' => $d->id]) ?>"
									data-toggle="tooltip"
									data-title="Delete"
								>
									<i class="fas fa-trash-alt text-danger"></i>
								</a>
							</th>
						</tr>
					<?php endforeach; ?>
					<?php if($allVerified < $documents->count()): ?>
					<tr><td colspan="4"><a href="#" onclick="bulk_actions('<?php echo route('admin.users.bulkActions', ['action' => 'documents_verification']) ?>', '1');" class="btn btn-sm py-2 px-3 btn-success" style="float:right"><i class="fa fa-check"></i> Verify Documents</a></td></tr>
					<?php endif; ?>
				<?php else: ?>
					<tr><td colspan="4">No documents available.</td></tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-documents" tabindex="-1" role="dialog" aria-labelledby="modal-documents" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="<?php echo route('admin.users.documents', ['id' => $user->id]) ?>" class="form-validation">
			{{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-documents">Upload Documents</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
            	<div class="form">
					<div class="form-group">
						<label class="form-control-label" for="input-first-name">Title</label>
						<select class="form-control" name="title" required>
							<option value="">Select</option>	
							<option value="driving-license-front">Driver's License - Front</option>
							<option value="driving-license-back">Driver's License - Back</option>
							<option value="vehicle-registration">Vehicle Registration</option>
							<option value="insurance-proof">Proof of Insurance</option>
						</select>
					</div>
					<div class="form-group">
						<div 
							class="upload-image-section"
							data-type="file"
							data-multiple="false"
							data-path="documents"
						>
							<div class="upload-section">
								<div class="button-ref mb-3">
									<button class="btn btn-icon btn-primary btn-lg" type="button">
						                <span class="btn-inner--icon"><i class="fas fa-upload"></i></span>
						                <span class="btn-inner--text">Upload File</span>
					              	</button>
					            </div>
					            <!-- PROGRESS BAR -->
								<div class="progress d-none">
				                  <div class="progress-bar bg-default" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
				                </div>
				            </div>
			                <!-- INPUT WITH FILE URL -->
			                <textarea class="d-none" required name="file"><?php echo old('file') ?></textarea>
			                <div class="show-section <?php echo !old('file') ? 'd-none' : "" ?>">
			                </div>
						</div>
					</div>
				</div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-edit-documents" tabindex="-1" role="dialog" aria-labelledby="modal-documents" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="" class="form-validation">
			{{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-documents">Edit Document</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
            	<div class="form">
					<div class="form-group">
						<label class="form-control-label" for="input-first-name">Expiry Date</label>
						<input type="date" class="form-control" name="expiry_date" required placeholder="Expiry Date">
						@error('expiry_date')
						    <small class="text-danger">{{ $message }}</small>
						@enderror
					</div>
					<div class="form-group">
						<label class="form-control-label" for="input-first-name">Verified</label>
						<div class="custom-control">
							<label class="custom-toggle">
								<input type="hidden" name="verified" value="0">
								<input type="checkbox" name="verified" value="1">
								<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
						</div>
						@error('verified')
						    <small class="text-danger">{{ $message }}</small>
						@enderror
					</div>
				</div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>