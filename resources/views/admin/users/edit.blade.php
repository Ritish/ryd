@extends('layouts.adminlayout')
@section('content')
<?php 
use App\Models\Admin\Services; 
use App\Models\Admin\Users; 
use App\Models\Admin\UserServices; 
use App\Models\Admin\Languages; 
use App\Models\Admin\UserLanguages; 
?>
<div class="header bg-primary pb-6">
	<div class="container-fluid">
		<div class="header-body">
			<div class="row align-items-center py-4">
				<div class="col-lg-6 col-7">
					<h6 class="h2 text-white d-inline-block mb-0">Manage <?php echo $user->driver ? 'Drivers' : 'Customers' ?></h6>
				</div>
				<div class="col-lg-6 col-5 text-right">
					<a href="<?php echo route('admin.users') . '?role=' . ($user->driver ? 'driver' : 'customer') ?>" class="btn btn-neutral"><i class="ni ni-bold-left"></i> Back</a>
					<?php if($user->driver): ?>
						<?php if($user->is_verified): ?>
							<a class="btn btn-success text-white"><i class="fa fa-check"></i> Verified</a>
						<?php else: ?>
							<a
								href="<?php // echo route('admin.users.verify', ['id' => $user->id]) ?>javascript:;" 
								class="btn btn-danger  text-white"
							><i class="fa fa-ban"></i> Pending Verification</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
	
		<div class="row">
			<div class="col-xl-6 order-xl-1">
				<div class="card">
					<!--!! FLAST MESSAGES !!-->
					@include('admin.partials.flash_messages')
					<form method="post" action="<?php echo route('admin.users.edit', ['id' => $user->id]) ?>" id="user-information-form">
						<!--!! CSRF FIELD !!-->
						{{ @csrf_field() }}
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col-8">
									<h3 class="mb-0">Update <?php echo $user->driver ? 'Drivers' : 'Customers' ?> Details Here.</h3>
								</div>
							</div>
						</div>
						<div class="card-body">		
							<h6 class="heading-small text-muted mb-4">User information</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-first-name">First name</label>
											<input type="text" class="form-control" name="first_name" required placeholder="First name" value="<?php echo $user->first_name ?>">
											@error('first_name')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>

									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-last-name">Last name</label>
											<input type="text" id="input-last-name" required class="form-control" placeholder="Last name" name="last_name" value="<?php echo $user->last_name ?>">
											@error('last_name')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-control-label" for="input-username">Email Address</label>
									<input type="email" id="input-username" class="form-control" placeholder="info@example.com" name="email"  value="<?php echo $user->email ?>" required>
									@error('email')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-username">Country code</label>
											<input type="number" id="input-country_code" class="form-control" placeholder="1" name="country_code"  value="<?php echo $user->country_code ?>" required>
											@error('country_code')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>	
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-email">Phone Number</label>
											<input type="text" id="input-email" class="form-control" placeholder="9988774455" name="phonenumber"  value="<?php echo $user->phonenumber ?>" required>
											@error('phonenumber')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-username">Gender</label>
											<select 
												id="gender"
												class="form-control" 
												name="gender" 
												required
												onChange="handleGenderRelations($(this).val())"
											>
												<option value="male" <?php echo $user->gender == 'male' ? 'selected' : '' ?>>Male</option>
												<option value="female" <?php echo $user->gender == 'female' ? 'selected' : '' ?>>Female</option>
											</select>
											@error('gender')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
									</div>
									<?php if($user->driver): ?>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="input-email">Date of Birth</label>
											<input type="date" id="input-email" class="form-control" name="dob"  value="{{ $user->dob }}" max="<?php echo date('Y-m-d') ?>" required>
											@error('dob')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
									</div>
									<?php endif; ?>
								</div>
							</div>
							
							<?php if($user->driver): ?>
								<hr class="my-4" />
								<h6 class="heading-small text-muted mb-4">Address Information</h6>
								<?php $address = $user->addreses()->orderBy('id', 'asc')->first(); ?>
								<div class="pl-lg-4">
									<div class="form-group">
										<label class="form-control-label" for="input-address">Mailing Address</label>
										<input type="text" class="form-control" name="address[mailing_address]" required placeholder="Mailing Address" value="<?php echo !empty($address) ? $address->mailing_address : "" ?>">
										@error('mailing_address')
										    <small class="text-danger">{{ $message }}</small>
										@enderror
									</div>
									<div class="form-group">
										<label class="form-control-label" for="input-address">Address Line</label>
										<input type="text" class="form-control" name="address[address_line]" placeholder="Address Line" value="<?php echo !empty($address) ? $address->address_line : "" ?>">
										@error('address_line')
										    <small class="text-danger">{{ $message }}</small>
										@enderror
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label" for="input-address">City</label>
												<input type="text" class="form-control" name="address[city]" required placeholder="Address Line" value="<?php echo !empty($address) ? $address->city : "" ?>">
												@error('city')
												    <small class="text-danger">{{ $message }}</small>
												@enderror
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label" for="input-address">State</label>
												<select name="address[state]" class="form-control" required>
													<option value=""></option>
													<?php foreach($states as $s): ?>
													<option  <?php echo ( !empty($address) && $address->state == $s->state_code ? 'selected' : ( !empty($address) && !$address->state && $s->state_code == 'GA' ? 'selected' : '' ) )  ?> value="<?php echo $s->state_code ?>"><?php echo $s->name . ' (' . $s->state_code . ')' ?></option>
													<?php endforeach; ?>
												</select>
												@error('state')
												    <small class="text-danger">{{ $message }}</small>
												@enderror
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label" for="input-address">Zip</label>
												<input type="number" class="form-control" name="address[zip]" required placeholder="Address Line" value="<?php echo !empty($address) ? $address->zip : "" ?>">
												@error('zip')
												    <small class="text-danger">{{ $message }}</small>
												@enderror
											</div>
										</div>
									</div>
								</div>
							<?php endif ?>
							<hr class="my-4" />
							<h6 class="heading-small text-muted mb-4">Service Information</h6>
							<div class="pl-lg-4">
								<div class="form-group">
									<label class="form-control-label" for="input-address">Female Driver</label>
									<div class="">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="female_driver_only1" name="female_driver_only" value="1" <?php echo (isset($user->female_driver_only) && $user->female_driver_only ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="female_driver_only1">Yes</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="female_driver_only0" name="female_driver_only" value="0" <?php echo (isset($user->female_driver_only) && !$user->female_driver_only ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="female_driver_only0">No</label>
										</div>
									</div>
									@error('female_driver_only')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<div class="form-group">
									<label class="form-control-label" for="input-address">Driver Speaking Spanish</label>
									<div class="">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="spanish_speaking_only1" name="spanish_speaking_only" value="1" <?php echo (isset($user->spanish_speaking_only) && $user->spanish_speaking_only ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="spanish_speaking_only1">Yes</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="spanish_speaking_only0" name="spanish_speaking_only" value="0" <?php echo (isset($user->spanish_speaking_only) && !$user->spanish_speaking_only ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="spanish_speaking_only0">No</label>
										</div>
									</div>
									@error('spanish_speaking_only')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<div class="form-group">
									<label class="form-control-label" for="input-address">Flag</label>
									<div class="">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="flag1" name="flag" value="1" <?php echo (isset($user->flag) && $user->flag ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="flag1">Yes</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="flag0" name="flag" value="0" <?php echo (isset($user->flag) && !$user->flag ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="flag0">No</label>
										</div>
									</div>
									@error('flag')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<?php if(!$user->driver): ?>
								<div class="form-group">
									<label class="form-control-label" for="input-address">Wheel Chair</label>
									<div class="">
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="wheel_chair1" name="wheel_chair" value="1" <?php echo (isset($user->wheel_chair) && $user->wheel_chair ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="wheel_chair1">Yes</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" id="wheel_chair0" name="wheel_chair" value="0" <?php echo (isset($user->wheel_chair) && !$user->wheel_chair ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="wheel_chair0">No</label>
										</div>
									</div>
									@error('flag')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<?php endif; ?>
								<div class="form-group">
									<label class="form-control-label" for="input-address">Service Type</label>
									<div class="row">
										<?php 
										$userServices = UserServices::where('user_id', $user->id)->pluck('service_id')->toArray();
										$services = Services::orderBy('id', 'asc')->where('allowed_gender', 'both')->get();
										?>
										<div class="col-md-4 both-services">
											<?php foreach($services as $k => $s): ?>
											<div class="custom-control custom-checkbox custom-control-inline" style="min-width: 30%;">
												<input type="checkbox" id="services<?php echo $s->id ?>" name="services[]" value="<?php echo $s->id ?>" <?php echo (in_array($s->id, $userServices) ? 'checked' : '') ?> class="custom-control-input">
												<label class="custom-control-label" for="services<?php echo $s->id ?>"><?php echo $s->title ?></label>
											</div>
											<?php endforeach; ?>
										</div>
										<?php $services = Services::orderBy('id', 'asc')->where('allowed_gender', 'male')->get(); ?>
										<div class="col-md-4 male-services">
											<?php foreach($services as $k => $s): ?>
											<div class="custom-control custom-checkbox custom-control-inline" style="min-width: 30%;">
												<input type="checkbox" id="services<?php echo $s->id ?>" name="services[]" value="<?php echo $s->id ?>" <?php echo (in_array($s->id, $userServices) ? 'checked' : '') ?> class="custom-control-input">
												<label class="custom-control-label" for="services<?php echo $s->id ?>"><?php echo $s->title ?></label>
											</div>
											<?php endforeach; ?>
										</div>
										<?php $services = Services::orderBy('id', 'asc')->where('allowed_gender', 'female')->get(); ?>
										<div class="col-md-4 female-services">
											<?php foreach($services as $k => $s): ?>
											<div class="custom-control custom-checkbox custom-control-inline" style="min-width: 30%;">
												<input type="checkbox" id="services<?php echo $s->id ?>" name="services[]" value="<?php echo $s->id ?>" <?php echo (in_array($s->id, $userServices) ? 'checked' : '') ?> class="custom-control-input">
												<label class="custom-control-label" for="services<?php echo $s->id ?>"><?php echo $s->title ?></label>
											</div>
											<?php endforeach; ?>
										</div>
									</div>
									@error('services')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>
								<div class="form-group">
									<label class="form-control-label" for="input-address">I Speak</label>
									<div class="">
										<?php 
										$userLanguages = UserLanguages::where('user_id', $user->id)->pluck('language_id')->toArray();
										$languages = Languages::orderBy('id', 'asc')->get()
										?>
										<?php foreach($languages as $k => $s): ?>
										<div class="custom-control custom-checkbox custom-control-inline" style="min-width: 30%;">
											<input type="checkbox" id="languages<?php echo $s->id ?>" name="languages[]" value="<?php echo $s->id ?>" <?php echo (in_array($s->id, $userLanguages) ? 'checked' : '') ?> class="custom-control-input">
											<label class="custom-control-label" for="languages<?php echo $s->id ?>"><?php echo $s->title ?></label>
										</div>
										<?php echo ($k+1)%3 <= 0 ? '<br>' : '' ?>
										<?php endforeach; ?>
									</div>
									@error('languages')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>

							</div>
							<?php if($user->driver): ?>
									<hr class="my-4" />
									<h6 class="heading-small text-muted mb-4">Trip Information</h6>
									<div class="pl-lg-4">
										<div class="form-group">
											<label class="form-control-label" for="input-address">Long Trips Range</label>
											<div class="row">
												<div class="col-md-6">
													<div class="input-group">
													  <div class="input-group-prepend">
													    <button type="button" class="btn btn-outline-secondary btn-minus">
													      <i class="fa fa-minus"></i>
													    </button>
													  </div>
													  <input class="form-control" min="1" name="long_trips_min" id="long_trips_min" value="<?php echo $user->long_trips_min > 0 ? $user->long_trips_min : '' ?>" type="number" placeholder="Min">
													  <div class="input-group-append">
													    <button  type="button" class="btn btn-outline-secondary btn-plus">
													      <i class="fa fa-plus"></i>
													    </button>
													  </div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="input-group">
													  <div class="input-group-prepend">
													    <button type="button" class="btn btn-outline-secondary btn-minus">
													      <i class="fa fa-minus"></i>
													    </button>
													  </div>
													  <input class="form-control" min="1" name="long_trips_max" id="long_trips_max" type="number" value="<?php echo $user->long_trips_max > 0 ? $user->long_trips_max : '' ?>">
													  <div class="input-group-append">
													    <button  type="button" class="btn btn-outline-secondary btn-plus">
													      <i class="fa fa-plus"></i>
													    </button>
													  </div>
													</div>
												</div>
											</div>
											@error('flag')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
										<div class="form-group">
											<label class="form-control-label" for="input-address">Airport Trips</label>
											<div class="">
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" id="airport_trips" name="airport_trips" value="1" <?php echo (isset($user->airport_trips) && $user->airport_trips ? 'checked' : '') ?> class="custom-control-input">
													<label class="custom-control-label" for="airport_trips">Yes</label>
												</div>
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" id="airport_trips0" name="airport_trips" value="0" <?php echo (isset($user->airport_trips) && !$user->airport_trips ? 'checked' : '') ?> class="custom-control-input">
													<label class="custom-control-label" for="airport_trips0">No</label>
												</div>
											</div>
											@error('flag')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div>
										<!-- <div class="form-group">
											<label class="form-control-label" for="input-address">Tag Number</label>
											<input type="text" class="form-control" name="tag_number" required placeholder="Tag Number" value="<?php //echo !empty($user) && $user->tag_number ? $user->tag_number : "" ?>">
											@error('tag_number')
											    <small class="text-danger">{{ $message }}</small>
											@enderror
										</div> -->
									</div>
								<?php endif; ?>
							

							<?php /*
							<hr class="my-4" />
							<h6 class="heading-small text-muted mb-4">Reset Password</h6>
							<div class="pl-lg-4">
								<div class="form-group">
									<div class="custom-control">
										<label class="custom-toggle">
											<input type="hidden" name="send_password_email" value="0">
											<input type="checkbox" name="send_password_email" value="1" id="sendPasswordEmail">
											<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
										</label>
										<label class="custom-control-label">Send new password on email ?</label>
									</div>
									@error('send_password_email')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
								</div>

								<div class="form-group passwordGroup">
									<div class="input-group">
										<input type="password" class="form-control" placeholder="****" aria-label="" aria-describedby="button-addon4" name="password">
										<div class="input-group-append" id="button-addon4">
											<button class="btn btn-outline-primary viewPassword" type="button"><i class="fas fa-eye"></i></button>
											<button class="btn btn-outline-primary regeneratePassword" type="button"><i class="fas fa-redo-alt"></i></button>
										</div>
									</div>
									@error('password')
									    <small class="text-danger">{{ $message }}</small>
									@enderror
									<div class="form-group">
										<small class="text-info">Password must be minimum 8 characters long.<br></small>
										<small class="text-info">Password should contain at least one capital letter (A-Z), one small letter (a-z), one number (0-9) and one special character (!@#$%^&amp;*).</small>
									</div>
								</div>
							</div>
							*/ ?>
							<hr class="my-4" />
							<button href="#" class="btn btn-sm py-2 px-3 btn-primary float-right">
								<i class="fa fa-save"></i> Submit
							</button>
						</div>
					</form>
				</div>
				
			</div>
			<div class="col-xl-6 order-xl-1">
				@include("admin.users.profile", ["id" => $user->id])
				<?php if($user->driver): ?>
				@include("admin.users.documents", ["user" => $user])
				@include("admin.users.vehicles", ["user" => $user, 'vehicle' => $vehicle, 'brands' => $brands, 'years' => $years, 'colors' => $colors, 'models' => $models])
				<?php endif ?>
			</div>
		</div>
	
</div>
@endsection