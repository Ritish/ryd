<?php use App\Models\Admin\Settings; ?>
<?php $symbol = Settings::get('currency_symbol'); ?>
<?php foreach($listing->items() as $k => $row):?>
<tr>
	<td>
		<!-- MAKE SURE THIS HAS ID CORRECT AND VALUES CORRENCT. THIS WILL EFFECT ON BULK CRUTIAL ACTIONS -->
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input listing_check" id="listing_check<?php echo $row->id ?>" value="<?php echo $row->id ?>">
			<label class="custom-control-label" for="listing_check<?php echo $row->id ?>"></label>
		</div>
	</td>
	<td>
		<span class="badge badge-dot mr-4">
			<i class="bg-warning"></i>
			<span class="status"><?php echo $row->id ?></span>
		</span>
	</td>
	<td>
		<?php if($row->user_first_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->user_id]) ?>"><?php echo $row->user_first_name . ' ' . $row->user_last_name ?></a>
		<?php else: ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->user_id]) ?>"><?php echo $row->user_phonenumber ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php if($row->driver_first_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->driver_id]) ?>"><?php echo $row->driver_first_name . ' ' . $row->driver_last_name ?></a>
		<?php else: ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->driver_id]) ?>"><?php echo $row->driver_phonenumber ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php echo $row->service->title ?>
	</td>
	<td>
		<?php echo $row->vehicle->number ?>
	</td>
	<td>
		<?php echo $row->distance . ' miles' ?>
	</td>
	<td>
		<?php echo $row->duration ?>
	</td>
	<td>
		<?php echo $symbol . ($row->driver_total > 0 ? $row->driver_total : '0.00') ?>
	</td>
	<td>
		<?php echo '<span class="badge badge-warning">'.ucfirst('pending').'</span>'; ?>
	</td>
	<td>
		<?php echo _dt($row->created) ?>
	</td>
	<td class="text-right">
		<a class="btn btn-sm btn-primary" href="<?php echo route('admin.trips.view', ['id' => $row->id]) ?>"><i class="fa fa-eye"></i> View Trip</a>
	</td>
</tr>
<?php endforeach; ?>