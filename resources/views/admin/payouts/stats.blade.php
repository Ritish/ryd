<?php use App\Models\Admin\Settings; ?>
<?php $symbol = Settings::get('currency_symbol'); ?>
<div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Pending Payouts</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['pendingAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['pending'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Completed Payouts</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['tranferedAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="fa fa-check"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['transfered'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Payouts</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['tranferedAmount'] + $counts['pendingAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="fa fa-money-bill-wave"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['pending'] + $counts['transfered'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Sales</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['totalSale'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <?php $profit = $counts['totalSale'] - $counts['tranferedAmount'] - $counts['pendingAmount'] ?> 
                    <?php if($profit > 0): ?>
                      <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo $symbol . round($profit, 2) ?></span>
                    <?php else: ?>
                      <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> <?php echo $symbol . round($profit, 2) ?></span>
                    <?php endif; ?>
                    <span class="text-nowrap">Profit</span>
                  </p>
                </div>
              </div>
            </div>
          </div>