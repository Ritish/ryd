@extends('layouts.adminlayout')
@section('content')
	<div class="header bg-primary pb-6">
		<div class="container-fluid">
			<div class="header-body">
				<div class="row align-items-center py-4">
					<div class="col-lg-6 col-7">
						<h6 class="h2 text-white d-inline-block mb-0">Driver Payouts</h6>
					</div>
					<div class="col-lg-6 col-5 text-right">
						@include('admin.payouts.filters')
					</div>
				</div>
				@include('admin.payouts.stats')
			</div>
		</div>
	</div>
	<!-- Page content -->
	<div class="container-fluid mt--6">
		<div class="row">
			<div class="col">
<!--!!!!! DO NOT REMOVE listing-block CLASS. INCLUDE THIS IN PARENT DIV OF TABLE ON LISTING PAGES !!!!!-->
				<div class="card listing-block">
					<!--!! FLAST MESSAGES !!-->
					@include('admin.partials.flash_messages')
					<!-- Card header -->
					<div class="card-header border-0">
						<div class="heading">
							<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
							        <li class="nav-item">
							            <a class="nav-link mb-sm-3 mb-md-0<?php echo strpos(request()->route()->getAction()['as'], 'admin.transactions.pendingPayouts') > -1 ? ' active' : '' ?>" href="<?php echo route('admin.transactions.pendingPayouts')?>"><i class="fa fa-list mr-2"></i>Pending ({{ $counts['pending'] }})</a>
							        </li>
							        <li class="nav-item">
							            <a class="nav-link mb-sm-3 mb-md-0<?php echo strpos(request()->route()->getAction()['as'], 'admin.transactions.payouts') > -1 ? ' active' : '' ?>" href="<?php echo route('admin.transactions.payouts')?>"><i class="fa fa-exclamation-triangle mr-2"></i>Transfered ({{ $counts['transfered'] }})</a>
							        </li>
							    </ul>
						</div>
						<div class="actions">
							<div class="input-group input-group-alternative input-group-merge">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-search"></i></span>
								</div>
								<input class="form-control listing-search" placeholder="Search" type="text" value="<?php echo (isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '') ?>">
							</div>
							<?php /*if(Permissions::hasPermission('trips', 'delete')): ?>
							<div class="dropdown" data-toggle="tooltip" data-title="Bulk Actions">
								<a class="btn btn-sm btn-icon-only text-warning" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fas fa-ellipsis-v"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
		                            <a 
		                            	href="javascript:void(0);" 
		                            	class="waves-effect waves-block dropdown-item text-danger" 
		                            	onclick="bulk_actions('<?php echo route('admin.transactions.bulkActions', ['action' => 'delete']) ?>', 'delete');"
		                            >
											<i class="fas fa-times text-danger"></i>
											<span class="status text-danger">Delete</span>
		                            </a>
								</div>
							</div>
							<?php endif; */?>
						</div>
					</div>
					<div class="table-responsive">
<!--!!!!! DO NOT REMOVE listing-table, mark_all  CLASSES. INCLUDE THIS IN ALL TABLES LISTING PAGES !!!!!-->
						<table class="table align-items-center table-flush listing-table">
							<thead class="thead-light">
								<tr>
									<th class="checkbox-th">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input mark_all" id="mark_all">
											<label class="custom-control-label" for="mark_all"></label>
										</div>
									</th>
									<th class="sort">
										<!--- MAKE SURE TO USE PROPOER FIELD IN data-field AND PROPOER DIRECTION IN data-sort -->
										Id
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.id' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.id" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.id' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.id" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.id" data-sort="asc"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Transaction ID
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.stripe_charge_id' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.stripe_charge_id" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.stripe_charge_id' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.stripe_charge_id" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.stripe_charge_id"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Customer
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.customer_name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.customer_name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.customer_name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.customer_name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.customer_name"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Driver
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.driver_name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.driver_name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.driver_name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.driver_name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.driver_name"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Service
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.service_name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.service_name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.service_name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.service_name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.service_name"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Vehicle
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.vehicle_number' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.vehicle_number" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.vehicle_number' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.vehicle_number" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.vehicle_number"></i>
										<?php endif; ?>
									</th>
									
									<th class="sort">
										Amount
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.amount' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.amount" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.amount' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.amount" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.amount"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Status
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.status' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.status" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.status' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.status" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.status"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Created ON
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'transactions.created' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="transactions.created" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'transactions.created' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="transactions.created" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="transactions.created"></i>
										<?php endif; ?>
									</th>
									<th>
										Actions
									</th>
								</tr>
							</thead>
							<tbody class="list">
								<?php if(!empty($listing->items())): ?>
									@include('admin.payouts.listingLoop')
								<?php else: ?>
									<td align="left" colspan="7">
		                            	No records found!
		                            </td>
								<?php endif; ?>
							</tbody>
							<tfoot>
		                        <tr>
		                            <th align="left" colspan="20">
		                            	@include('admin.partials.pagination', ["pagination" => $listing])
		                            </th>
		                        </tr>
		                    </tfoot>
						</table>
					</div>
					<!-- Card footer -->
				</div>
			</div>
		</div>
	</div>
@endsection