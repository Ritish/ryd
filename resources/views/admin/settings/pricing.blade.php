<?php 
use App\Models\Admin\Services;
use App\Libraries\DateTime;
?>
@extends('layouts.adminlayout')
@section('content')
<style>.form-group{width: 48%; display: inline-block; padding: 0 10px;}</style>
<div class="header bg-primary pb-6">
	<div class="container-fluid">
		<div class="header-body">
			<div class="row align-items-center py-4">
				<div class="col-lg-6 col-7">
					<h6 class="h2 text-white d-inline-block mb-0">Manage Pricing - <b><?php echo $service->title ?></b></h6>

				</div>
			</div>
			@include('admin.partials.flash_messages')
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
	<div class="row">
		<div class="col-xl-6 order-xl-1">
			<div class="card">
				<!--!! FLAST MESSAGES !!-->
				
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0"><i class="fas fa-user"></i> Customer Pricing</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo route('admin.settings.pricing', ['id' => $id]) ?>" class="form-validation">
						<!--!! CSRF FIELD !!-->
						{{ @csrf_field() }}
						<div class="pl-lg-4">
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Time before passenger Fee (per minute)</label>
								<input type="number" min="0" class="form-control" name="pickup_duration_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'pickup_duration_fee') }}">
								@error('pickup_duration_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Mileage before passenger Fee (per mile)</label>
								<input type="number" min="0" class="form-control" name="distance_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'distance_fee') }}">
								@error('distance_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Booking Fee</label>
								<input type="number" min="0" class="form-control" name="booking_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'booking_fee') }}">
								@error('booking_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Pickup Fee</label>
								<input type="number" min="0" class="form-control" name="pickup_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'pickup_fee') }}">
								@error('pickup_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Fee Per Mile</label>
								<input type="number" min="0" class="form-control" name="per_mile_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'per_mile_fee') }}">
								@error('per_mile_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Fee Per Minute (per minute)</label>
								<input type="number" min="0" class="form-control" name="per_minute_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'per_minute_fee') }}">
								@error('per_minute_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Wait Time Free</label>
								<?php $time = explode(':', DateTime::secondsToTime(Services::getPriceValue($id, 'free_wait_time')*60)); ?>
								<div class="time-inteval">
								  <input type="number" class="time form-control" name="free_wait_time[]" min="0" max="23" placeholder="23" value="<?php echo $time[0] ?>">:
								  <input type="number" class="time form-control" name="free_wait_time[]" min="0" max="59" placeholder="00" value="<?php echo $time[1] ?>">:
								  <input type="number" class="time form-control" name="free_wait_time[]" min="0" max="59" placeholder="00" value="<?php echo $time[2] ?>">
								</div>
								@error('free_wait_time')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Scheduled/Unscheduled Wait Fee</label>
								<input type="number" min="0" class="form-control" name="wait_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'wait_fee') }}">
								@error('wait_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Surge Charge</label>
								<input type="number" min="0" class="form-control" name="surge_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'surge_fee') }}">
								@error('surge_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>

							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Atlanta Origination Fee</label>
								<input type="number" min="0" class="form-control" name="atlanta_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'atlanta_fee') }}">
								@error('atlanta_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Em Fee</label>
								<input type="number" min="0" class="form-control" name="em_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'em_fee') }}">
								@error('em_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Et Fee</label>
								<input type="number" min="0" class="form-control" name="et_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'et_fee') }}">
								@error('et_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">GA Total Tax</label>
								<input type="number" min="0" class="form-control" name="ga_tax" required placeholder="$" value="{{ Services::getPriceValue($id, 'ga_tax') }}">
								@error('ga_tax')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
						</div>
						<button href="#" class="btn btn-sm py-2 px-3 btn-primary float-right">
							<i class="fa fa-save"></i> Submit
						</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-xl-6 order-xl-1">
			<div class="card">
				<!--!! FLAST MESSAGES !!-->
				
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0"><i class="fas fa-car"></i> Driver Pricing</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo route('admin.settings.pricing', ['id' => $id]) ?>" class="form-validation">
						<!--!! CSRF FIELD !!-->
						{{ @csrf_field() }}
						<div class="pl-lg-4">
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Time before passenger Fee (per minute)</label>
								<input type="number" min="0" class="form-control" name="driver_pickup_duration_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_pickup_duration_fee') }}">
								@error('driver_pickup_duration_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Mileage before passenger Fee (per mile)</label>
								<input type="number" min="0" class="form-control" name="driver_distance_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_distance_fee') }}">
								@error('driver_distance_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Pickup Fee</label>
								<input type="number" min="0" class="form-control" name="driver_pickup_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_pickup_fee') }}">
								@error('driver_pickup_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Fee Per Mile</label>
								<input type="number" min="0" class="form-control" name="driver_per_mile_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_per_mile_fee') }}">
								@error('driver_per_mile_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Fee Per Minute (per minute)</label>
								<input type="number" min="0" class="form-control" name="driver_per_minute_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_per_minute_fee') }}">
								@error('driver_per_minute_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Wait Time Free</label>
								<?php $time = explode(':', DateTime::secondsToTime(Services::getPriceValue($id, 'driver_free_wait_time')*60)); ?>
								<div class="time-inteval">
								  <input type="number" class="time form-control" name="driver_free_wait_time[]" min="0" max="23" placeholder="23" value="<?php echo $time[0] ?>">:
								  <input type="number" class="time form-control" name="driver_free_wait_time[]" min="0" max="59" placeholder="00" value="<?php echo $time[1] ?>">:
								  <input type="number" class="time form-control" name="driver_free_wait_time[]" min="0" max="59" placeholder="00" value="<?php echo $time[2] ?>">
								</div>
								@error('driver_free_wait_time')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Scheduled/Unscheduled Wait Fee</label>
								<input type="number" min="0" class="form-control" name="driver_wait_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_wait_fee') }}">
								@error('driver_wait_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Surge Charge</label>
								<input type="number" min="0" class="form-control" name="driver_surge_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_surge_fee') }}">
								@error('driver_surge_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>

							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Atlanta Origination Fee</label>
								<input type="number" min="0" class="form-control" name="driver_atlanta_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_atlanta_fee') }}">
								@error('driver_atlanta_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Em Fee</label>
								<input type="number" min="0" class="form-control" name="driver_em_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_em_fee') }}">
								@error('driver_em_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Et Fee</label>
								<input type="number" min="0" class="form-control" name="driver_et_fee" required placeholder="$" value="{{ Services::getPriceValue($id, 'driver_et_fee') }}">
								@error('driver_et_fee')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
						</div>
						<button href="#" class="btn btn-sm py-2 px-3 btn-primary float-right">
							<i class="fa fa-save"></i> Submit
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection