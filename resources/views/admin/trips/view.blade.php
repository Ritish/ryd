<?php use App\Models\Admin\Trips; ?>
<?php use App\Models\Admin\Settings; ?>
<?php $currency = Settings::get('currency_symbol'); ?>
@extends('layouts.adminlayout')
@section('content')
	<div class="header bg-primary pb-6">
		<div class="container-fluid">
			<div class="header-body">
				<div class="row align-items-center py-4">
					<div class="col-lg-6 col-7">
						<h6 class="h2 text-white d-inline-block mb-0">Trip #<?php echo $trip->id ?></h6>
					</div>
					<div class="col-lg-6 col-5 text-right">
						<a href="<?php echo route('admin.trips') ?>" class="btn text-white"><i class="fa fa-arrow-left"></i> Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Page content -->
	<div class="container-fluid mt--6">
		<div class="row">
			<div class="col-xl-7 order-xl-1">
				<div class="card">
					<!--!! FLAST MESSAGES !!-->
					@include('admin.partials.flash_messages')
					<div class="card-header">
						<div class="row align-items-center">
							<div class="col-8">
								<h3 class="mb-0">Trip Information</h3>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<!-- Projects table -->
						<table class="table align-items-center table-flush view-table">
							<tbody>
								<tr>
									<th>Id</th>
									<td><?php echo $trip->id ?></td>
								</tr>
								<tr>
									<th>Customer</th>
									<td>
										<?php if($trip->customer): ?>
										<a href="<?php echo route('admin.users.view', ['id' => $trip->user_id]) ?>"><?php echo $trip->customer->first_name . ' ' . $trip->customer->last_name . ($trip->customer->gender ? ' ('.ucfirst($trip->customer->gender).')' : '') ?></a>
										<?php endif; ?>
									</td>
								</tr>
								<tr>
									<th>Driver</th>
									<td>
										<?php if($trip->driver): ?>
											<a href="<?php echo route('admin.users.view', ['id' => $trip->driver_id]) ?>"><?php echo $trip->driver->first_name . ' ' . $trip->driver->last_name ?></a>
										<?php endif; ?>
									</td>
								</tr>
								<tr>
									<th>Service</th>
									<td>
										<?php if($trip->service): ?>
											<?php echo $trip->service->title; ?></a>
										<?php endif; ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Created On
									</th>
									<td>
										<?php echo _dt($trip->created) ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Status
									</th>
									<td>
										<?php echo Trips::getSatusLabel($trip->status)?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Customer Paid
									</th>
									<td>
										<?php echo $trip->customer_paid ? '<span class="badge badge-success">Paid</span>' : '<span class="badge badge-danger">Pending</span>' ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Transfered to Driver
									</th>
									<td>
										<?php echo $trip->driver_transfered ? '<span class="badge badge-success">Paid</span>' : '<span class="badge badge-danger">Pending</span>' ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
						<div class="row align-items-center">
							<div class="col">
								<h3 class="mb-0">Trip Route</h3>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<!-- Projects table -->
						<table class="table align-items-center table-flush view-table">
							<tbody>
								<tr>
									<th scope="row"  style="width: 30%;">
										Driver Location
									</th>
									<td>
										<?php echo $trip->from_address ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Pickup Location
									</th>
									<td>
										<?php echo $trip->pickup_address ?>
									</td>
								</tr>
								<?php foreach($trip->destinations as $k => $de): ?>
								<tr>
									<th scope="row">
										 <?php echo $k+1 == count($trip->destinations) ? 'End Destination' : ('Destination ' . ($k+1))?>
									</th>
									<td>
										<?php echo $de->address ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-6 order-xl-1">
					<div class="card">
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Trip Waitings</h3>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<!-- Projects table -->
							<table class="table align-items-center table-flush view-table">
								<thead>
									<tr>
										<th><b>Start Time</b></th>
										<th><b>End Time</b></th>
										<th><b>Minutes</b></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($trip->waitings) > 0): ?>
										<?php foreach($trip->waitings as $k => $de): ?>
										<tr>
											<td>
												<?php echo _dt(date('Y-m-d H:i:s', $de->start_time)) ?>
											</td>
											<td>
												<?php echo _dt(date('Y-m-d H:i:s', $de->start_time)) ?>
											</td>
											<td>
												<?php echo $de->end_time && round(($de->end_time - $de->start_time)/60, 1) ? round(($de->end_time - $de->start_time)/60, 1) . ' mins' : '0 mins' ?>
											</td>
										</tr>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="3">
												<p class="text-danger text-center ">No waiting available</p>
											</td>
										</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
					</div>
					</div>
					<div class="col-xl-6 order-xl-1">
					<div class="card">
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Messages</h3>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							@include('admin.trips.messages')
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="col-xl-5 order-xl-1">

				<div class="card">
					<div class="card-header">
						<div class="row align-items-center">
							<div class="col">
								<h3 class="mb-0">Time, Cost & Distance</h3>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<!-- Projects table -->
						<table class="table align-items-center table-flush view-table">
							<tbody>
								<tr>
									<th scope="row" style="width: 60%">
										Start Time (by driver)
									</th>
									<td colspan="2">
										<?php echo _dt($trip->start_trip_time) ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Pickup Time
									</th>
									<td colspan="2">
										<?php echo _dt($trip->pickup_trip_time) ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Time before driver arrives
									</th>
									<td colspan="2">
										<?php echo $trip->before_pickup_duration ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Mileage before driver arrives
									</th>
									<td colspan="2">
										<?php echo $trip->before_pickup_distance . ' miles' ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Duration
									</th>
									<td colspan="2">
										<?php echo $trip->duration_time ?>
									</td>
								</tr>
								
								<tr>
									<th scope="row">
										Mileage
									</th>
									<td colspan="2">
										<?php echo $trip->distance . ' miles' ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Waiting Time
									</th>
									<td colspan="2">
										<?php echo $trip->waiting_time ?>
									</td>
								</tr>

								<tr>
									<th style="background-color: #eee;">
										<small>Trip Charges</small>
									</th>
									<th style="background-color: #eee;">
										<small>Customer Charges</small>
									</th>
									<th style="background-color: #eee;">
										<small>Driver Pay</small>
									</th>
								</tr>
								<?php $charges = Trips::getCustomerTripCharges($trip); ?>
								<?php $dCharges = Trips::getDriverTripAmount($trip); ?>
								<tr>
									<th scope="row">
										Time before trip starts (<?php echo $trip->before_pickup_duration ?>)
									</th>
									<td>
										<?php echo $currency . $charges['timeBeforeTripStarts'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['timeBeforeTripStarts'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Mileage before trip starts (<?php echo $trip->before_pickup_distance . ' miles' ?>)
									</th>
									<td>
										<?php echo $currency . $charges['distanceBeforeTripStarts'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['distanceBeforeTripStarts'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Booking Fee
									</th>
									<td>
										<?php echo $currency . $charges['bookingFee'] ?>
									</td>
									<td>
										<?php echo $currency . '0.00' ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Pickup Fee
									</th>
									<td>
										<?php echo $currency . $charges['pickupFee'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['pickupFee'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Per Mile (<?php echo $trip->distance ?> miles)
									</th>
									<td>
										<?php echo $currency . $charges['perMile'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['perMile'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Per Minute (<?php echo $trip->before_pickup_duration ?>)
									</th>
									<td>
										<?php echo $currency . $charges['perMinutes'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['perMinutes'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Wait Time (Actual: <?php echo $trip->waiting_time?>, Free: <?php echo $trip->free_wait_time ?>)
										<br>
										Driver Free Time: <?php echo $trip->driver_free_wait_time ?>
									</th>
									<td>
										<?php echo $currency . $charges['waitTime'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['waitTime'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Surge charge
									</th>
									<td>
										<?php echo $currency . $charges['surgeCharge'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['surgeCharge'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Atlanta Origination Fee
									</th>
									<td>
										<?php echo $currency . $charges['atlantaFee'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['atlantaFee'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Em Fee
									</th>
									<td>
										<?php echo $currency . $charges['emFee'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['emFee'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Et Fee
									</th>
									<td>
										<?php echo $currency . $charges['etFee'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['etFee'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										GA Tax
									</th>
									<td>
										<?php echo $currency . $charges['gaTax'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['gaTax'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Tip
									</th>
									<td>
										<?php echo $currency . $charges['tip'] ?>
									</td>
									<td>
										<?php echo $currency . $dCharges['tip'] ?>
									</td>
								</tr>
								<tr>
									<th scope="row">
										Total Amount
									</th>
									<td>
										<b><?php echo $currency . $charges['totalAmount'] ?></b>
									</td>
									<td>
										<b><?php echo $currency . $dCharges['totalAmount'] ?></b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
@endsection