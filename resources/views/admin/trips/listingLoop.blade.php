<?php use App\Models\Admin\Trips; ?>
<?php use App\Models\Admin\Settings; ?>
<?php $symbol = Settings::get('currency_symbol'); ?>
<?php foreach($listing->items() as $k => $row):?>
<tr>
	<td>
		<!-- MAKE SURE THIS HAS ID CORRECT AND VALUES CORRENCT. THIS WILL EFFECT ON BULK CRUTIAL ACTIONS -->
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input listing_check" id="listing_check<?php echo $row->id ?>" value="<?php echo $row->id ?>">
			<label class="custom-control-label" for="listing_check<?php echo $row->id ?>"></label>
		</div>
	</td>
	<td>
		<span class="badge badge-dot mr-4">
			<i class="bg-warning"></i>
			<span class="status"><?php echo $row->id ?></span>
		</span>
	</td>
	<td>
		<?php if($row->user_first_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->user_id]) ?>"><?php echo $row->user_first_name . ' ' . $row->user_last_name ?></a>
		<?php else: ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->user_id]) ?>"><?php echo $row->user_phonenumber ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php if($row->driver_first_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->driver_id]) ?>"><?php echo $row->driver_first_name . ' ' . $row->driver_last_name ?></a>
		<?php else: ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->driver_id]) ?>"><?php echo $row->driver_phonenumber ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php echo $row->service_title ?>
	</td>
	<td>
		<?php echo $row->vehicle_number ?>
	</td>
	
	<td>
		<?php echo $row->distance ?>
	</td>
	<td>
		<?php echo $row->duration ?>
	</td>
	<td>
		<?php echo $symbol .$row->customer_total ?>
	</td>
	<td>
		<?php echo _dt($row->created) ?>
	</td>
	<td class="text-right">
		
			
		
		<div class="dropdown">
			<a class="btn btn-sm btn-icon-only text-warning" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-ellipsis-v"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
				<a class="dropdown-item" href="<?php echo route('admin.trips.view', ['id' => $row->id]) ?>">
					<i class="fas fa-eye text-info"></i>
					<span class="status">View</span>
				</a>
				<?php if(Permissions::hasPermission('trips', 'delete')): ?>
					<div class="dropdown-divider"></div>
					<a 
						class="dropdown-item _delete" 
						href="javascript:;"
						data-link="<?php echo route('admin.trips.delete', ['id' => $row->id]) ?>"
					>
						<i class="fas fa-times text-danger"></i>
						<span class="status text-danger">Delete</span>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</td>
</tr>
<tr class="markup">
	<td colspan="2" style="text-align: center;">
		<?php  echo Trips::getSatusLabel($row->status) ?>
	</td>
	<td colspan="4">
		<small><b>Pickup:</b> <?php echo $row->pickup_address ?></small>
	</td>
	<td colspan="5">
		<small><b>Destination</b> <?php echo $row->to_address ?></small>
	</td>
</tr>
<?php endforeach; ?>