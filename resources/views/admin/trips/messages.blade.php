<div class="col col-md-11 mx-auto my-auto px-1 py-3">
    <div id="messages_container" class="chat-log">
    	<?php if(count($messages) > 0): ?>
	    	<?php foreach($messages as $m): ?>
		    	<?php if($m->from_id == $trip->user_id): ?>
			        <div class="chat-log_item chat-log_item-own z-depth-0">
			            <div class="row justify-content-end mx-1 d-flex">
			                <div class="col-auto px-0">
			                    <span class="chat-log_author">
			                    	@<?php echo $trip->customer->first_name ? $trip->customer->first_name . ' ' . $trip->customer->last_name : 'Customer' ?>
			                    </span>
			                </div>
			                <div class="col-auto px-0">
			                </div>
			            </div>
			            <hr class="my-1 py-0 col-8" style="opacity: 0.5">
			            <div class="chat-log_message">
			                <p><?php echo nl2br($m->message) ?></p>
			            </div>
			            <div class="row chat-log_time m-0 p-0 justify-content-end">
			                <?php echo date('h:i a', strtotime($m->created)) ?> <small clas="text-muted">Customer</small>
			            </div>
			        </div>
		    	<?php else: ?>
			        <div class="chat-log_item chat-log_item z-depth-0">
			            <div class="row justify-content-end mx-1 d-flex">
			                <div class="col-auto px-0">
			                    <span class="chat-log_author">
			                    	@<?php echo $trip->driver->first_name ? $trip->driver->first_name . ' ' . $trip->driver->last_name : 'Driver' ?>
			                    </span>
			                </div>
			                <div class="col-auto px-0">
			                </div>
			            </div>
			            <hr class="my-1 py-0 col-8" style="opacity: 0.5">
			            <div class="chat-log_message">
			                <p><?php echo nl2br($m->message) ?></p>
			            </div>
			            <div class="row chat-log_time m-0 p-0 justify-content-end">
			                <?php echo date('h:i a', strtotime($m->created)) ?> <small clas="text-muted">Driver</small>
			            </div>
			        </div>
		    	<?php endif; ?>
	    	<?php endforeach; ?>
		<?php else: ?>
			<p class="text-center">No converstion has been made.</p>
		<?php endif; ?>
    </div>
</div>

