@extends('layouts.adminlayout')
@section('content')
	<div class="header bg-primary pb-6">
		<div class="container-fluid">
			<div class="header-body">
				<div class="row align-items-center py-4">
					<div class="col-lg-6 col-7">
						<h6 class="h2 text-white d-inline-block mb-0">Trips</h6>
					</div>
					<div class="col-lg-6 col-5 text-right">
						@include('admin.trips.filters')
					</div>
				</div>
				@include('admin.trips.stats')
			</div>
		</div>
	</div>
	<!-- Page content -->
	<div class="container-fluid mt--6">
		<div class="row">
			<div class="col">
<!--!!!!! DO NOT REMOVE listing-block CLASS. INCLUDE THIS IN PARENT DIV OF TABLE ON LISTING PAGES !!!!!-->
				<div class="card listing-block">
					<!--!! FLAST MESSAGES !!-->
					@include('admin.partials.flash_messages')
					<!-- Card header -->
					<div class="card-header border-0">
						<div class="heading">
							<h3 class="mb-0">Here Is Your Trips Listing!</h3>
						</div>
						<div class="actions">
							<div class="input-group input-group-alternative input-group-merge">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-search"></i></span>
								</div>
								<input class="form-control listing-search" placeholder="Search" type="text" value="<?php echo (isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '') ?>">
							</div>
							<?php if(Permissions::hasPermission('trips', 'delete')): ?>
							<div class="dropdown" data-toggle="tooltip" data-title="Bulk Actions">
								<a class="btn btn-sm btn-icon-only text-warning" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fas fa-ellipsis-v"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
									<?php if(Permissions::hasPermission('trips', 'delete')): ?>
		                            <a 
		                            	href="javascript:void(0);" 
		                            	class="waves-effect waves-block dropdown-item text-danger" 
		                            	onclick="bulk_actions('<?php echo route('admin.trips.bulkActions', ['action' => 'delete']) ?>', 'delete');"
		                            >
											<i class="fas fa-times text-danger"></i>
											<span class="status text-danger">Delete</span>
		                            </a>
		                        	<?php endif; ?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="table-responsive">
<!--!!!!! DO NOT REMOVE listing-table, mark_all  CLASSES. INCLUDE THIS IN ALL TABLES LISTING PAGES !!!!!-->
						<table class="table align-items-center table-flush listing-table">
							<thead class="thead-light">
								<tr>
									<th class="checkbox-th">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input mark_all" id="mark_all">
											<label class="custom-control-label" for="mark_all"></label>
										</div>
									</th>
									<th class="sort">
										<!--- MAKE SURE TO USE PROPOER FIELD IN data-field AND PROPOER DIRECTION IN data-sort -->
										Id
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'trips.id' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="trips.id" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'trips.id' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="trips.id" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="trips.id" data-sort="asc"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Customer
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'users.first_name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="users.first_name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'users.first_name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="users.first_name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="users.first_name"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Driver
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'driver.first_name' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="driver.first_name" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'driver.first_name' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="driver.first_name" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="driver.first_name"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Service
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'services.title' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="services.title" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'services.title' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="services.title" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="services.title"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Vehicle
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'vehicles.number' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="vehicles.number" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'vehicles.number' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="vehicles.number" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="vehicles.number"></i>
										<?php endif; ?>
									</th>
									<!-- <th class="sort">
										Pickup
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'trips.pickup_address' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="trips.pickup_address" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'trips.pickup_address' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="trips.pickup_address" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="trips.pickup_address"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Destination
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'trips.to_address' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="trips.to_address" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'trips.to_address' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="trips.to_address" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="trips.to_address"></i>
										<?php endif; ?>
									</th> -->
									<th class="sort">
										Distance
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'distance' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="distance" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'distance' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="distance" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="distance"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Duration
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'duration' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="duration" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'duration' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="duration" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="duration"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Charges
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'trips.customer_total' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="trips.customer_total" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'trips.customer_total' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="trips.customer_total" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="trips.customer_total"></i>
										<?php endif; ?>
									</th>
									<th class="sort">
										Created ON
										<?php if(isset($_GET['sort']) && $_GET['sort'] == 'trips.created' && isset($_GET['direction']) && $_GET['direction'] == 'asc'): ?>
										<i class="fas fa-sort-down active" data-field="trips.created" data-sort="asc"></i>
										<?php elseif(isset($_GET['sort']) && $_GET['sort'] == 'trips.created' && isset($_GET['direction']) && $_GET['direction'] == 'desc'): ?>
										<i class="fas fa-sort-up active" data-field="trips.created" data-sort="desc"></i>
										<?php else: ?>
										<i class="fas fa-sort" data-field="trips.created"></i>
										<?php endif; ?>
									</th>
									<th>
										Actions
									</th>
								</tr>
							</thead>
							<tbody class="list">
								<?php if(!empty($listing->items())): ?>
									@include('admin.trips.listingLoop')
								<?php else: ?>
									<td align="left" colspan="7">
		                            	No records found!
		                            </td>
								<?php endif; ?>
							</tbody>
							<tfoot>
		                        <tr>
		                            <th align="left" colspan="20">
		                            	@include('admin.partials.pagination', ["pagination" => $listing])
		                            </th>
		                        </tr>
		                    </tfoot>
						</table>
					</div>
					<!-- Card footer -->
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="export-trips" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Export Trips Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      		<p>Maximum of 3000 records can be export from all records at one time.</p>
	      		<form id="exportTrips">
	      			<!--!! CSRF FIELD !!-->
					{{ @csrf_field() }}
	      			<input type="hidden" class="filter-query" value="">
	      			<div class="form-group">
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="all" name="type" value="all" class="custom-control-input" checked="">
							<label class="custom-control-label" for="all">All Records</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="filtered" name="type" value="filtered" class="custom-control-input">
							<label class="custom-control-label" for="filtered">Filtered Records</label>
						</div>
					</div>
					<div class="form-group" id="daterangeFilter">
						<label class="form-control-label" for="input-first-name">Apply Date Range</label>
						<input type="text" class="form-control" id="datarangepicker" name="daterange" placeholder="MM/DD/YYYY - MM/DD/YYYY" value="">
					</div>
					<div class="form-group d-none" id="filteredMessage">
						<h3><mark><i class="fas fa-exclamation-circle"></i> Background applied filters will be applicable.</mark></h3>
					</div>
				</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="export-excel" data-url="<?php echo route('admin.trips.export') ?>" class="btn btn-primary"><i class="fas fa-file-export"></i> Export</button>
	      </div>
	    </div>
	  </div>
	</div>
@endsection