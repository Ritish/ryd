<?php use App\Models\Admin\Settings; ?>
<?php $symbol = Settings::get('currency_symbol'); ?>
<div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Active Trips</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $counts['active'] ?> <small>records</small></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="fas fa-car-side"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                      <span class="text-orange mr-2"><?php echo $counts['totalDistance'] ?></span>
                      <span class="text-nowrap">miles</span>
                    </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Completed Trips</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['completedAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="fa fa-check"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['completed'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Canceled Trips</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['canceledAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="fa fa-times"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['canceled'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Revenue</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $symbol . round($counts['completedAmount'] + $counts['canceledAmount'], 2) ?></span>
                    </div>
                    <div class="col-auto">
                      
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="fa fa-money-bill-wave"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-orange mr-2"><?php echo $counts['active'] + $counts['completed'] + $counts['canceled'] ?></span>
                    <span class="text-nowrap">records</span>
                  </p>
                </div>
              </div>
            </div>
          </div>