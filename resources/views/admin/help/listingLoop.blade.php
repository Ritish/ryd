<?php foreach($listing->items() as $k => $row):?>
<tr>
	<td>
		<!-- MAKE SURE THIS HAS ID CORRECT AND VALUES CORRENCT. THIS WILL EFFECT ON BULK CRUTIAL ACTIONS -->
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input listing_check" id="listing_check<?php echo $k ?>" value="<?php echo $row->id ?>">
			<label class="custom-control-label" for="listing_check<?php echo $k ?>"></label>
		</div>
	</td>
	<td>
		<span class="badge badge-dot mr-4">
			<i class="bg-warning"></i>
			<span class="status"><?php echo $row->id ?></span>
		</span>
	</td>
	<td>
		<a href="mailto:<?php echo $row->email ?>"><?php echo $row->email ?></a>
	</td>
	<td>
		<?php echo $row->status ? '<span class="badge badge-success">Resolved</span>' : '<span class="badge badge-danger">Open</span>' ?>
	</td>
	<td>
		<b><?php echo $row->help_category ?></b><br>
		<?php echo $row->issue ?>
	</td>
	
	<td>
		<?php echo _dt($row->created) ?>
	</td>
	<td class="text-right">
		<?php if(Permissions::hasPermission('help', 'update') || Permissions::hasPermission('help', 'delete')): ?>
		<div class="dropdown">
			<a class="btn btn-sm btn-icon-only text-warning" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-ellipsis-v"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
				<a class="dropdown-item" href="mailto:<?php echo $row->email ?>">
					<i class="fas fa-envelope text-info"></i>
					<span class="status">Send Email</span>
				</a>
				<div class="dropdown-divider"></div>
				<?php if(Permissions::hasPermission('help', 'update')): ?>
					<a class="dropdown-item" href="<?php echo route('admin.help.markAsResolved', ['id' => $row->id, 'status' => (int) !$row->status]) ?>">
						<i class="fas <?php echo (!$row->status ? 'fa-check text-success' : 'fa-ticket-alt text-danger') ?>"></i>
						<span class="status"><?php echo $row->status ? 'Mark as Open' : 'Mark as Resolved' ?></span>
					</a>
				<?php endif; ?>
				<?php if(Permissions::hasPermission('help', 'delete')): ?>
					<div class="dropdown-divider"></div>
					<a 
						class="dropdown-item _delete" 
						href="javascript:;"
						data-link="<?php echo route('admin.help.delete', ['id' => $row->id]) ?>"
					>
						<i class="fas fa-times text-danger"></i>
						<span class="status text-danger">Delete</span>
					</a>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>