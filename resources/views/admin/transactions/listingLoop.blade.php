<?php use App\Models\Admin\Trips; ?>
<?php foreach($listing->items() as $k => $row):?>
<tr>
	<td>
		<!-- MAKE SURE THIS HAS ID CORRECT AND VALUES CORRENCT. THIS WILL EFFECT ON BULK CRUTIAL ACTIONS -->
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input listing_check" id="listing_check<?php echo $row->id ?>" value="<?php echo $row->id ?>">
			<label class="custom-control-label" for="listing_check<?php echo $row->id ?>"></label>
		</div>
	</td>
	<td>
		<span class="badge badge-dot mr-4">
			<i class="bg-warning"></i>
			<span class="status"><?php echo $row->id ?></span>
		</span>
	</td>
	<td>
		<span class="badge badge-dot mr-4">
			<span class="status"><?php echo $row->stripe_charge_id ?></span>
		</span>
	</td>
	<td>
		<?php if($row->customer_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->user_id]) ?>"><?php echo $row->customer_name ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php if($row->driver_name): ?>
			<a href="<?php echo route('admin.users.edit', ['id' => $row->driver_id]) ?>"><?php echo $row->driver_name ?></a>
		<?php endif; ?>
	</td>
	<td>
		<?php echo $row->service_name ?>
	</td>
	<td>
		<?php echo $row->vehicle_number ?>
	</td>
	<td>
		<?php echo $row->currency_symbol . $row->amount ?>
	</td>
	<td>
		<?php echo $row->status ? '<span class="badge badge-success">'.ucfirst($row->status).'</span>' : '<span class="badge badge-warning">'.ucfirst($row->status).'</span>' ?>
	</td>
	<td>
		<?php echo _dt($row->created) ?>
	</td>
	<td class="text-right">
		<?php if($row->trip_id): ?>
		<a class="btn btn-sm py-2 px-3 btn-primary" href="<?php echo route('admin.trips.view', ['id' => $row->trip_id]) ?>"><i class="fa fa-eye"></i> View Trip</a>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>