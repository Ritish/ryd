<div class="dropdown filter-dropdown">
	<a class="btn btn-neutral dropdown-btn" href="#" <?php echo (isset($_GET) && !empty($_GET) ? 'data-title="Filters are active" data-toggle="tooltip"' : '') ?>>
		<?php if(isset($_GET) && !empty($_GET)): ?>
		<span class="filter-dot text-info"><i class="fas fa-circle"></i></span>
		<?php endif; ?>
		<i class="fas fa-filter"></i> Filters
	</a>
	<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
		<form action="<?php echo route('admin.transactions') ?>" id="filters-form">
			<a href="javascript:;" class="float-right px-2 closeit"><i class="fa fa-times-circle"></i></a>
			<div class="dropdown-item">
				<div class="row">
					<div class="col-md-12">
						<label class="form-control-label">Services</label>
						<select class="form-control" name="services[]" multiple>
					      	<?php foreach($services as $c): ?>
					      		<option value="<?php echo $c->id ?>" <?php echo isset($_GET['services']) && in_array($c->id, $_GET['services'])  ? 'selected' : '' ?>><?php echo $c->title ?></option>
					  		<?php endforeach; ?>
					    </select>
					</div>
					<div class="col-md-12">
						<label class="form-control-label">Status</label>
						<select class="form-control" name="status[]" multiple>
					      	<?php foreach($statuses as $k => $c): ?>
					      		<option 
					      			value="<?php echo $c ?>"
					      			<?php echo isset($_GET['status']) && in_array($c, $_GET['status'])  ? 'selected' : '' ?>
					      		>
					      			<?php echo ucwords($c) ?>
					      		</option>
					  		<?php endforeach; ?>
					    </select>
					</div>
					<div class="col-md-12">
						<label class="form-control-label">Customers</label>
						<select class="form-control" name="users[]" multiple>
					      	<?php foreach($users as $c): ?>
					      		<option data-subtext="<?php echo $c->status ? 'Active': 'Inactive'?>" value="<?php echo $c->id ?>" <?php echo isset($_GET['users']) && in_array($c->id, $_GET['users'])  ? 'selected' : '' ?>><?php echo $c->first_name . ' ' . $c->last_name ?></option>
					  		<?php endforeach; ?>
					    </select>
					</div>
					<div class="col-md-12">
						<label class="form-control-label">Drivers</label>
						<select class="form-control" name="drivers[]" multiple>
					      	<?php foreach($drivers as $c): ?>
					      		<option data-subtext="<?php echo $c->status ? 'Active': 'Inactive'?>" value="<?php echo $c->id ?>" <?php echo isset($_GET['drivers']) && in_array($c->id, $_GET['drivers'])  ? 'selected' : '' ?>><?php echo $c->first_name . ' ' . $c->last_name ?></option>
					  		<?php endforeach; ?>
					    </select>
					</div>
					<div class="col-md-12">
						<label class="form-control-label">Customer Charges</label>
						<div class="range-slider">
						  <input class="range-slider__range" name="customer_price" type="range" value="<?php echo (isset($_GET['customer_price']) && $_GET['customer_price'] ? $_GET['customer_price'] : ceil($maxCustomerTotal)) ?>" min="0" max="<?php echo ceil($maxCustomerTotal) ?>">
						  <span class="range-slider__value">0</span>
						</div>
					</div>
				</div>
			</div>
			<div class="dropdown-divider"></div>
			<div class="dropdown-item">
				<div class="row">
					<div class="col-md-6">
						<label class="form-control-label">Created On</label>
						<input class="form-control" type="date" name="created_on[0]" value="<?php echo (isset($_GET['created_on'][0]) && !empty($_GET['created_on'][0]) ? $_GET['created_on'][0] : '' ) ?>" placeholder="DD-MM-YYYY" >
					</div>
					<div class="col-md-6">
						<label class="form-control-label">&nbsp;</label>
						<input class="form-control" type="date" name="created_on[1]" value="<?php echo (isset($_GET['created_on'][1]) && !empty($_GET['created_on'][1]) ? $_GET['created_on'][1] : '' ) ?>" placeholder="DD-MM-YYYY">
					</div>
				</div>
			</div>
			<div class="dropdown-divider"></div>
			<a href="<?php echo route('admin.transactions') ?>" class="btn btn-sm py-2 px-3 float-left">
				Reset All
			</a>
			<button href="#" class="btn btn-sm py-2 px-3 btn-primary float-right">
				Submit
			</button>
		</form>
	</div>
</div>