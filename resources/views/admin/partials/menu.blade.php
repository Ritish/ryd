<?php 
use App\Models\Admin\Services; 
$services = Services::select(['id', 'title'])->orderBy('id', 'asc')->get();
?>
<div class="collapse navbar-collapse" id="sidenav-collapse-main">
    <ul class="navbar-nav">
        <li class="nav-item">
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.dashboard') > -1; ?>
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="<?php echo route('admin.dashboard') ?>">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
            </a>
        </li>
        
        <?php if(Permissions::hasPermission('users', 'listing')): ?>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.users') > -1 && (!isset($_GET['role']) || $_GET['role'] == 'customer') ; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.users') ?>">
                    <i class="fas fa-users text-warning"></i>
                    <span class="nav-link-text">Customers</span>
                </a>
            </li>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.users') > -1 && isset($_GET['role']) && $_GET['role'] == 'driver' ; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.users') . '?role=driver' ?>">
                    <i class="fas fa-steering-wheel  text-warning"></i>
                    <span class="nav-link-text">Drivers</span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(Permissions::hasPermission('vehicles', 'listing')): ?>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.vehicles') > -1; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.vehicles') ?>">
                    <i class="fas fa-car text-warning"></i>
                    <span class="nav-link-text">Vehicles</span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(Permissions::hasPermission('trips', 'listing')): ?>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.trips') > -1; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.trips') ?>">
                    <i class="fas fa-car-side text-warning"></i>
                    <span class="nav-link-text">Trips</span>
                </a>
            </li>
        <?php endif; ?>

        <?php if(Permissions::hasPermission('transactions', 'listing')): ?>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.transactions') > -1; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.transactions') ?>">
                    <i class="fas fa-dollar-sign text-warning"></i>
                    <span class="nav-link-text">Payments</span>
                </a>
            </li>
        <?php endif; ?>

        <?php if(Permissions::hasPermission('payouts', 'listing')): ?>
            <?php $active = strpos(request()->route()->getAction()['as'], 'admin.transactions.payouts') > -1; ?>
            <li class="nav-item">
                <a class="nav-link<?php echo ($active ? ' active' : '') ?>" href="<?php echo route('admin.transactions.payouts') ?>">
                    <i class="fas fa-money-bill-wave text-warning"></i>
                    <span class="nav-link-text">Payouts</span>
                </a>
            </li>
        <?php endif; ?>

        
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.help') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="#help" data-toggle="collapse">
                <i class="fa fa-life-ring  text-warning"></i>
                <span class="nav-link-text">Help</span>
            </a>
            <ul class="list-unstyled submenu collapse<?php echo ($active ? ' show' : '') ?>" id="help">
                <li class="nav-item">
                    <?php $active = strpos(request()->route()->getAction()['as'], 'admin.help.') > -1; ?>
                    <a class="nav-link <?php echo $active ?>" href="<?php echo route('admin.help') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Help Requests</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <?php $active = strpos(request()->route()->getAction()['as'], 'admin.helpCategories') > -1; ?>
                    <a class="nav-link <?php echo $active ?>" href="<?php echo route('admin.helpCategories') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Help Categories</span>
                        </span>
                    </a>
                </li>
            </ul>
            
        </li>
    </ul>
    <!-- Divider -->
    <hr class="my-3">
    <?php if(AdminAuth::isAdmin()): ?>
    <!-- Heading -->
    <h6 class="navbar-heading p-0 text-muted">
        <span class="docs-normal">Others</span>
    </h6>
    <?php endif; ?>
    <!-- Navigation -->
    <ul class="navbar-nav mb-md-3">
        <?php if(AdminAuth::isAdmin()): ?>
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.pages') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="#submenu_pages" data-toggle="collapse">
                <i class="ni ni-bullet-list-67"></i>
                <span class="nav-link-text">Pages</span>
            </a>
            <ul class="list-unstyled submenu collapse<?php echo ($active ? ' show' : '') ?>" id="submenu_pages">
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit' && $page->id == 18 ? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 18]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Customer Terms Conditions</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit' && $page->id == 20 ? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 20]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Customer Privacy Policy</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit' && $page->id == 22 ? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 22]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Customer Legal</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit' && $page->id == 19 ? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 19]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Driver Terms Conditions</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit'  && $page->id == 21? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 21]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Driver Privacy Policy</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.pages.edit' && $page->id == 23 ? ' active' : '') ?>" href="<?php echo route('admin.pages.edit', ['id' => 23]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Driver Legal</span>
                        </span>
                    </a>
                </li>
            </ul>
            
        </li>

        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.brands') > -1 || strpos(request()->route()->getAction()['as'], 'admin.colors') > -1 || strpos(request()->route()->getAction()['as'], 'admin.years') > -1 || strpos(request()->route()->getAction()['as'], 'admin.models') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="#submenu_data" data-toggle="collapse">
                <i class="fa fa-car"></i>
                <span class="nav-link-text">Vehicle Data</span>
            </a>
            <ul class="list-unstyled submenu collapse<?php echo ($active ? ' show' : '') ?>" id="submenu_data">
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.brands' ? ' active' : '') ?>" href="<?php echo route('admin.brands') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Manage Brands</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.models' ? ' active' : '') ?>" href="<?php echo route('admin.models') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Manage Models</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.years' ? ' active' : '') ?>" href="<?php echo route('admin.years') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Manage Years</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.colors' ? ' active' : '') ?>" href="<?php echo route('admin.colors') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Manage Colors</span>
                        </span>
                    </a>
                </li>
            </ul>
            
        </li>
        
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.settings.pricing') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="#pricing" data-toggle="collapse">
                <i class="fas fa-dollar-sign"></i>
                <span class="nav-link-text">Manage Pricing</span>
            </a>
            <ul class="list-unstyled submenu collapse<?php echo ($active ? ' show' : '') ?>" id="pricing">
                <?php foreach($services as $s): ?>
                <li class="nav-item">
                    <a class="nav-link<?php echo ($s->id == 1 ? ' active' : '') ?>" href="<?php echo route('admin.settings.pricing', ['id' => $s->id]) ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status"><?php echo $s->title ?></span>
                        </span>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
        </li>
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.admins') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="<?php echo route('admin.admins') ?>">
                <i class="fas fa-users"></i>
                <span class="nav-link-text">Manage Admins</span>
            </a>
        </li>
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.emailTemplates') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="<?php echo route('admin.emailTemplates') ?>">
                <i class="ni ni-email-83"></i>
                <span class="nav-link-text">Email Templates</span>
            </a>
        </li>
        
        <?php $active = strpos(request()->route()->getAction()['as'], 'admin.activities') > -1; ?>
        <li class="nav-item">
            <a class="nav-link<?php echo $active ? ' active' : '' ?>" href="#submenu_activites" data-toggle="collapse">
                <i class="ni ni-bullet-list-67"></i>
                <span class="nav-link-text">Activities</span>
            </a>
            <ul class="list-unstyled submenu collapse<?php echo ($active ? ' show' : '') ?>" id="submenu_activites">
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.activities.logs' ? ' active' : '') ?>" href="<?php echo route('admin.activities.logs') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Activity Logs</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link<?php echo (request()->route()->getAction()['as'] == 'admin.activities.emails' ? ' active' : '') ?>" href="<?php echo route('admin.activities.emails') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Email Logs</span>
                        </span>
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="<?php echo route('admin.activities.pages') ?>">
                        <span class="badge badge-dot mr-4">
                            <i class="bg-gray"></i>
                            <span class="status">Page Logs</span>
                        </span>
                    </a>
                </li> -->
            </ul>
            
        </li>
        <?php endif; ?>
    </ul>
</div>