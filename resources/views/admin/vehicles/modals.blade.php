<div class="modal fade" id="modal-brands" tabindex="-1" role="dialog" aria-labelledby="modal-brands" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="<?php echo route('admin.brands.add') ?>" class="form-validation">
			{{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-brands">Create New Brand</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
            	<div class="form">
					<div class="form-group">
						<label class="form-control-label" for="input-first-name">Title</label>
						<input type="text" class="form-control" name="title" required placeholder="Title" value="{{ old('title') }}">
					</div>
				</div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-models" tabindex="-1" role="dialog" aria-labelledby="modal-models" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="<?php echo route('admin.models.add') ?>" class="form-validation">
            {{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-models">Create New Model</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
                <div class="form">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Brand</label>
                        <select class="form-control" name="brand_id" required>
                            <option value=""></option>
                            <?php 
                                foreach($brands as $c): 
                            ?>
                                <option value="<?php echo $c->id ?>" <?php echo old('brand_id') && old('brand_id') == $c->id  ? 'selected' : '' ?>><?php echo $c->title ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Title</label>
                        <input type="text" class="form-control" name="title" required placeholder="Title" value="{{ old('title') }}">
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-colors" tabindex="-1" role="dialog" aria-labelledby="modal-colors" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="<?php echo route('admin.colors.add') ?>" class="form-validation">
            {{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-colors">Create New Color</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
                <div class="form">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Title</label>
                        <input type="text" class="form-control" name="title" required placeholder="Title" value="{{ old('title') }}">
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-years" tabindex="-1" role="dialog" aria-labelledby="modal-years" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
           <form method="post" action="<?php echo route('admin.years.add') ?>" class="form-validation">
            {{ @csrf_field() }}
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-years">Create New Year</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
                <div class="form">
                    <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Year</label>
                        <input type="text" class="form-control" name="year" required placeholder="Year" value="{{ old('year') }}">
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link mr-auto" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>