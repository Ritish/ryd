@extends('layouts.adminlayout')
@section('content')
<?php use App\Models\Admin\VehicleServices; ?>
<div class="header bg-primary pb-6">
	<div class="container-fluid">
		<div class="header-body">
			<div class="row align-items-center py-4">
				<div class="col-lg-6 col-7">
					<h6 class="h2 text-white d-inline-block mb-0">Manage Vehicles</h6>
				</div>
				<div class="col-lg-6 col-5 text-right">
					<a href="<?php echo route('admin.vehicles') ?>" class="btn btn-neutral"><i class="ni ni-bold-left"></i> Back</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
	<div class="row">
		<div class="col-xl-12 order-xl-1">
			<div class="card">
				<!--!! FLAST MESSAGES !!-->
				@include('admin.partials.flash_messages')
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0">Update Vehicle Here.</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="post" action="<?php echo route('admin.vehicles.edit', ['id' => $vehicle->id]) ?>" class="form-validation">
						<!--!! CSRF FIELD !!-->
						{{ @csrf_field() }}
						<h6 class="heading-small text-muted mb-4">Vehicle information</h6>
						<div class="pl-lg-4">
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Driver</label>
								<select class="form-control" name="user_id" required>
							      	<?php 
							      		foreach($drivers as $c): 
							      			$content = ($c->first_name ? $c->first_name . ' ' . $c->last_name : $c->country_code . '-' . $c->phonenumber ) . "<small class='badge badge-".($c->status ? "success" : "danger")."'>".($c->status ? "Active" : "Inactive")."</small>";
							      	?>
							      		<option 
							      			value="<?php echo $c->id ?>" 
							      			<?php echo isset($c->user_id) && $c->user_id == $vehicle->user_id  ? 'selected' : '' ?>
							      			data-content="<?php echo $content ?>"
							      		></option>
							  		<?php endforeach; ?>
							    </select>
								@error('user_id')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Vehicle Number</label>
								<input type="text" class="form-control" name="number" required placeholder="Vehicle Number" value="{{ $vehicle->number }}">
								@error('number')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Year</label>
								<select class="form-control" name="year" required>
									<option value=""></option>
							      	<?php 
							      		foreach($years as $c): 
							      	?>
							      		<option value="<?php echo $c->year ?>" <?php echo $vehicle->year && $vehicle->year == $c->year  ? 'selected' : '' ?>><?php echo $c->year ?></option>
							  		<?php endforeach; ?>
							    </select>
								@error('brand_id')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Brand</label>
								<select class="form-control" name="brand_id" required id="brands">
									<option value=""></option>
							      	<?php 
							      		foreach($brands as $c): 
							      	?>
							      		<option value="<?php echo $c->id ?>" <?php echo $vehicle->brand_id && $vehicle->brand_id == $c->id  ? 'selected' : '' ?>><?php echo $c->title ?></option>
							  		<?php endforeach; ?>
							    </select>
								@error('brand_id')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Model</label>
								<select class="form-control" name="model_id" required id="models">
									<option value=""></option>
							      	<?php 
							      		foreach($models as $c): 
							      	?>
							      		<option value="<?php echo $c->id ?>" <?php echo $vehicle->model_id && $vehicle->model_id == $c->id  ? 'selected' : '' ?>><?php echo $c->title ?></option>
							  		<?php endforeach; ?>
							    </select>
								@error('model_id')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Trim</label>
								<input type="text" class="form-control" name="trim" required placeholder="Trim" value="{{ $vehicle->trim }}">
								@error('trim')
								    <small class="text-danger">{{ $message }}</small>	
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Color</label>
								<select class="form-control" name="color_id" required>
									<option value=""></option>
							      	<?php 
							      		foreach($colors as $c): 
							      	?>
							      		<option value="<?php echo $c->id ?>" <?php echo $vehicle->color_id && $vehicle->color_id == $c->id  ? 'selected' : '' ?>><?php echo $c->title ?></option>
							  		<?php endforeach; ?>
							    </select>
								@error('brand_id')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-first-name">Seats</label>
								<div class="input-group">
									<input type="number" class="form-control" name="seats" value="{{ $vehicle->seats }}">
								</div>
								@error('seats')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
							<div class="form-group">
								<label class="form-control-label" for="input-address">Service Type</label>
								<div class="">
									<?php 
									$userServices = VehicleServices::where('vehicle_id', $vehicle->id)->pluck('service_id')->toArray();
									foreach($services as $k => $s): ?>
									<div class="custom-control custom-checkbox custom-control-inline" style="min-width: 30%;">
										<input type="checkbox" id="services<?php echo $s->id ?>" name="services[]" value="<?php echo $s->id ?>" <?php echo (in_array($s->id, $userServices) ? 'checked' : '') ?> class="custom-control-input">
										<label class="custom-control-label" for="services<?php echo $s->id ?>"><?php echo $s->title ?></label>
									</div>
									<?php echo ($k+1)%3 <= 0 ? '<br>' : '' ?>
									<?php endforeach; ?>
								</div>
								@error('services')
								    <small class="text-danger">{{ $message }}</small>
								@enderror
							</div>
						</div>
						<hr class="my-4" />
						<!-- Address -->
						<h6 class="heading-small text-muted mb-4">Publish Information</h6>
						<div class="pl-lg-4">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<!-- FILE OR IMAGE UPLOAD. FOLDER PATH SET HERE in data-path AND CHANGE THE data-multiple TO TRUE SEE MAGIC  -->
										<div 
											class="upload-image-section"
											data-type="image"
											data-multiple="true"
											data-path="vehicles"
											data-resize-large="920*640"
											data-resize-medium="400*250"
											data-resize-small="100*70"
										>
											<div class="upload-section">
												<div class="button-ref mb-3">
													<button class="btn btn-icon btn-primary btn-lg" type="button">
										                <span class="btn-inner--icon"><i class="fas fa-upload"></i></span>
										                <span class="btn-inner--text">Upload Image</span>
									              	</button>
									            </div>
									            <!-- PROGRESS BAR -->
												<div class="progress d-none">
								                  <div class="progress-bar bg-default" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
								                </div>
								            </div>
							                <!-- INPUT WITH FILE URL -->
							                <textarea class="d-none" name="image"><?php echo $vehicle->image ?></textarea>
							                <div class="show-section <?php echo !old('image') ? 'd-none' : "" ?>">
							                	@include('admin.partials.previewFileRender', ['file' => old('image') ])
							                </div>
							                <div class="fixed-edit-section">
							                	@include('admin.partials.previewFileRender', ['file' => $vehicle->image, 'relationType' => 'vehicles.image', 'relationId' => $vehicle->id ])
							                </div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<div class="custom-control">
											<label class="custom-toggle">
												<input type="hidden" name="status" value="0">
												<input type="checkbox" name="status" value="1" <?php echo ($vehicle->status != '0' ? 'checked' : '') ?>>
												<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
											</label>
											<label class="custom-control-label">Do you want to active this vehicle ?</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr class="my-4" />
						<button href="#" class="btn btn-sm py-2 px-3 btn-primary float-right">
							<i class="fa fa-save"></i> Submit
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection